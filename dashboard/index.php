<?php
require('important.php');
  $title = "Dashboard";
require('template/head.php');
require('features/stat.php');

$object = new Statistics();
?>

  <body class="h-100">
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
        <?php require('template/aside.php') ?>
        <!-- End Main Sidebar -->
        <main class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
          <?php require('template/navbar.php') ?>
          <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle"><?php echo $title; ?></span>
              </div>
            </div>

            <!-- End Page Header -->
            <!-- Small Stats Blocks -->
            
            <?php if(isset($_COOKIE['transfer_success'])):?>
            <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <?php echo $_COOKIE['transfer_success']; ?> </div>
<?php endif;?>
<?php if(isset($_COOKIE['transfer_error'])):?>
            <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <?php echo $_COOKIE['transfer_error']; ?> </div>
<?php endif;?>
<div class="row">
            <div class="col-lg col-md-4 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">Users</span>
                        <h6 class="stats-small__value count my-3"><?php echo $object->countUsers();?></h6>
                      </div>
                      
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg col-md-6 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">Transactions</span>
                        <h6 class="stats-small__value count my-3">182</h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg col-md-4 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">Successful Transactions</span>
                        <h6 class="stats-small__value count my-3">8,147</h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- End Small Stats Blocks -->
          
          </div>
          <?php require('template/footer.php') ?>
        </main>
      </div>
    </div>
   
    <?php require('template/script.php') ?>
    </body>
</html>