<?php

require('important.php');
    $title = "Change Password";
require('template/head.php');
require('features/stat.php');
$obj = new Statistics();
$users = $obj->getUsers();

   if(isset($_POST['submit'])){
       if($_POST['pass1'] === $_POST['pass2']){
           $obj->changePassword($_POST['pass1'],$_POST['user']);
       }
      
    }
?>

  <body class="h-100">
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
        <?php require('template/aside.php') ?>
        <!-- End Main Sidebar -->
        <main class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
          <?php require('template/navbar.php') ?>
          <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle"><?php echo $title; ?></span>
              </div>
            </div>
            <!-- End Page Header -->
            <div class="col-md-12">
            <?php if(isset($_COOKIE['password_change'])):?>
            <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <?php echo $_COOKIE['password_change']; ?> </div>
<?php endif;?>
            <form data-toggle="validator" novalidate="true" method="POST" enctype="multipart/form-data">
        <div class="form-group">
            <select name="user" class="form-control">
                <option value="">Select a User</option>
                <?php for($i = 0; $i < count($users); $i++): ?>
                <option value="<?php echo $users[$i]['id']; ?>" ><?php echo $users[$i]['firstname']." ".$users[$i]['lastname']; ?></option>
                <?php endfor; ?>
            </select>
        </div>
        <div class="form-group">
        <input data-toggle="validator" id="pass1" class="form-control"  data-minlength="8" type="password"  name="pass1" placeholder="New Password" required autocomplete/>
        <center><span class="help-block">Minimum of 8 characters</span></center> </div>
        <div class="form-group">
        <input name="pass2" type="password" id="pass2" class="form-control" id="password" data-match="#pass1" data-match-error="Whoops, these don't match" placeholder="Confirm Password" required autocomplete/>
        <center><span class="help-block with-errors"></span> </center>
        </div>
 
        <br>
        <div class="form-group">
        <input type="submit" name="submit" class="btn btn-primary col-md-offset-2" value="Submit">
        </div>
        
        
        </form>
            </div>
          
          </div>
          <?php require('template/footer.php') ?>
        </main>
      </div>
    </div>
   
    <?php require('template/script.php') ?>
    </body>
</html>