<?php

require('important.php');
require('../user/features/transaction.php');
$title = "Create User ";
require('template/head.php');
require('features/stat.php');
$obj = new Statistics();
$accounts = $obj->getUserAccount();

$routing_digit = isset($_POST['routing_digit']) ? $_POST['routing_digit']: "";
$account_id = isset($_POST['account_id']) ? $_POST['account_id']: "";
$rec_account_name = isset($_POST['rec_account_name']) ? $_POST['rec_account_name']: "";
$rec_account_number = isset($_POST['rec_account_number']) ? $_POST['rec_account_number']: "";
$bank_name = isset($_POST['bank']) ? $_POST['bank']: "";
$swift_code = isset($_POST['swift_code']) ? $_POST['swift_code']: "";
$amount = isset($_POST['amount']) ? $_POST['amount']: "";
$description = isset($_POST['description']) ? $_POST['description']: "";
$cred_deb = isset($_POST['cred_deb']) ? $_POST['cred_deb']: "";
$verification_code = isset($_POST['verification_code']) ? $_POST['verification_code']: "";

if(isset($_POST['submit'])){
    $user = $obj->getUserId($account_id);
    $obj2 = new Transfer($user,$account_id,$rec_account_name,$rec_account_number,$bank_name,$amount,$routing_digit,$swift_code,$description,$verification_code,$cred_deb);
    if($obj2->checkPin($verification_code) == false){
        $error = "Pin is Invalid";
    }else{
        $obj2->addTransfer2($_POST['date_of_transaction']);
    }
}
?>

  <body class="h-100">
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
        <?php require('template/aside.php') ?>
        <!-- End Main Sidebar -->
        <main class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
          <?php require('template/navbar.php') ?>
          <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle"><?php echo $title; ?></span>
              </div>
            </div>
            <!-- End Page Header -->
            <div class="col-md-12">
            <?php if(isset($_COOKIE['transaction_success'])):?>
            <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <?php echo $_COOKIE['transaction_success']; ?> </div>
<?php endif;?>

            
            <form data-toggle="validator" novalidate="true" method="POST" autocomplete="off">
        <fieldset>
        <h2 class="fs-title">Transaction Details</h2>
        <div class="form-group">
        <label >Account Number</label>
        <select name="account_id" class="form-control" required>
        <option value="">Select your Account</option>
        <?php foreach($accounts as $account):?>
            <option value ="<?php echo $account['id'];?>"><?php echo $account['account_number']." (".$account['account_type'].") - $".$account['credit'];?></option>
        <?php endforeach;?>
        </select>
        </div>

        <div class="form-group">
        <label >Recipient Account Name</label>
        <input class="form-control" type="text" name="rec_account_name" value="<?php echo $rec_account_name;?>" required/><span class="help-block with-errors"></span>
        </div>

        <div class="form-group">
        <label >Bank Name</label>
        <input class="form-control" required name="bank" type="text"/>
</div>
        <div class="form-group">
        <label >Recipient Account Number</label>
        <input class="form-control" type="text" name="rec_account_number" data-minlength="10" maxlength="10" value="<?php echo $rec_account_number;?>" required/><span class="help-block with-errors"></span>
        </div>

        <div class="form-group">
        <label >Amount</label>
        <input class="form-control" type="text" maxlength="10" step="1.00" pattern="^\d+(?:\.\d{1,2})?$" name="amount" value="<?php echo $amount;?>" required/>
        </div>

        <div class="form-group">
        <label>Routing Number</label>
        <input class="form-control" type="text"  data-minlength="9" maxlength="10" name="routing_digit" value="<?php echo $routing_digit;?>" required/><span class="help-block with-errors"></span>
        </div>

        <div class="form-group">
        <label >Swift Code</label>
        <input class="form-control" name="swift_code" type="text" data-minlength="8" maxlength="11" value="<?php echo $swift_code;?>" required/>
        <span class="help-block with-errors"></span>
        </div>
        <div class="form-group">
        <label >Description</label>
        <textarea class="form-control" columns="5" name="description" required><?php echo $description; ?></textarea>
        <span class="help-block with-errors"></span>
        </div>
        <div class="form-group">
        <label>Credit/Debit</label>
        <select name="cred_deb" class="form-control">
            <option value ="0">Debit</option>
            <option value ="1">Credit</option>
        </select>
        </div>

        <div class="form-group">
        <label >DATE</label>
        <input class="form-control" name="date_of_transaction" type="date" required/>
        </div>

        <div class="form-group">
        <label >Verification Code</label>
        <input class="form-control" name="verification_code" type="text" maxlength="6" value="<?php echo $verification_code;?>" required/>
        <center style="color:red;"><?php echo isset($error) ? $error : ""; ?></center>
        </div>
       <div class="form-group">
           <button type="submit" name="submit" class="form-control btn btn-primary btn-sm">Add Transaction</button>
        </fieldset>
        </form>
    </div>
          </div>
          <?php require('template/footer.php') ?>
        </main>
      </div>
    </div>
   
    <?php require('template/script.php') ?>
    </body>
</html>