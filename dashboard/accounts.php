<?php 
    $title = "Allocate Accounts";

require('important.php');
require('template/head.php');
require('features/stat.php');
$obj = new Statistics();
$users = $obj->getUsers();

$user = isset($_POST['user_id']) ? $_POST['user_id']: "";
$account_number = isset($_POST['account_number']) ? $_POST['account_number']: "";
$account_type = isset($_POST['account_type']) ? $_POST['account_type']: "";
$balance = isset($_POST['balance']) ? $_POST['balance']: "";
$credit = isset($_POST['credit']) ? $_POST['credit']: "";

    if(isset($_POST['submit'])){
        $obj2 = new Account($user,$account_number,$account_type,$balance,$credit);
        
            if($obj2->checkAccountNumber() == true){
                $error = "This Account Number is already allocated to another account";
            }else{
                $obj2->allocateAccount();
            }
    }
?>

  <body class="h-100">
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
        <?php require('template/aside.php') ?>
        <!-- End Main Sidebar -->
        <main class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
          <?php require('template/navbar.php') ?>
          <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle"><?php echo $title; ?></span>
              </div>
            </div>
            <!-- End Page Header -->
            <div class="col-md-12">
            <?php if(isset($_COOKIE['create_success'])):?>
            <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <?php echo $_COOKIE['create_success']; ?> </div>
<?php endif;?>
          <form class="form" method="POST">
              <div class="form-row">
                  <div class="form-group col-md-6 col-sm-12">
                  <label>User</label>
              <select name="user_id" class="form-control" required>
                  <?php for($i = 0; $i < count($users); $i++):?>
                  <option value ="<?php echo $users[$i]['id']?>"><?php echo $users[$i]['firstname']." ".$users[$i]['lastname']."(".$users[$i]['email'].")";?></option>
                <?php endfor;?>
              </select>
</div>
            <div class="form-group col-md-6 col-sm-12">
            <label>Account Number</label>
              <input class="form-control" name="account_number" type="text" maxlength="10" required value="<?php echo $account_number ?>"/>
              <?php if(isset($error)): ?>
            <center><span class="help-block"><?php echo $error;?></span></center>
            <?php endif;?></div>

            <div class="form-group col-md-6 col-sm-12">
            <label>Account Type</label>
              <select name="account_type" class="form-control" required>
                  <option>Investment</option>
                  <option>Business</option>
                  <option>Current</option>
                  <option>Deposit</option>
                  <option>Checking</option>
                <option>Savings</option>
              </select></div>
              
              <div class="form-group col-md-6 col-sm-12">
                <label>Balance</label>
              <input class="form-control" name="balance" type="text" maxlength="10" step=".01" pattern="^\d+(?:\.\d{1,2})?$" required value="<?php echo $balance ?>">
              </div>

              <div class="form-group col-md-6 col-sm-12">
                <label>Credit</label>
              <input class="form-control" name="credit" type="text" maxlength="10" step=".01" pattern="^\d+(?:\.\d{1,2})?$" required value="<?php echo $credit ?>">
              </div>
</div>
<div class="form-group">
    <button type="submit" class="btn btn-primary" name="submit">Submit</button>
</div>
          </form>  </div>
          
          </div>
          <?php require('template/footer.php') ?>
        </main>
      </div>
    </div>
   
    <?php require('template/script.php') ?>
    </body>
</html>
              