<?php
    class Security{
        private $answer;
        private $answer2;
        private $answer3;
        private $pin;
        private $ip_address;
        private $user_id;
        public $db_conn;

        public function __construct($answer,$answer2,$answer3,$pin,$ip_address,$user_id){
            $this->answer = $answer;
            $this->answer2 = $answer2;
            $this->answer3 = $answer3;
            $this->pin = $pin;
            $this->ip_address = $ip_address;
            $this->user_id = $user_id;
            $this->db_conn = new PDO('mysql:host=localhost;dbname=mcenggnf_mcentrale','root','project');

        }

        public function checkPin(){
            $sql = "SELECT COUNT(*) AS num FROM pins WHERE pin = ? AND status = ?";
				
			$stmt = $this->db_conn->prepare($sql);
			$stmt->execute(array($this->pin,'0'));
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
                if($row['num'] > 0){
                    return true;
                }else{
                    return false;
                }
        }

        public function finalLogin(){
            if($this->checkPin() && $this->answerToQuestion1() && $this->answerToQuestion2() && $this->answerToQuestion3() ){
                    $this->updatePin();
                    $this->addIpAdress();
                    header("Location: user/");
            }
        }

        public function answerToQuestion1(){
            if(strtolower($this->answer) == 'olivia'){
                $response = true;
            }else{
                $response = false;
            }
            return $response;
        }

        public function answerToQuestion2(){
            if(strtolower($this->answer2) == 'chelsea'){
                $response = true;
            }else{
                $response = false;
            }
            return $response;
        }

        public function answerToQuestion3(){
            if(strtolower($this->answer3) == 'patricia'){
                $response = true;
            }else{
                $response = false;
            }
            return $response;
        }

        public function updatePin(){
            $sql = "UPDATE pins SET status = ? WHERE pin = ? ";
			$stmt = $this->db_conn->prepare($sql);
			$stmt->execute(array('1',$this->pin));
        }

        public function addIpAdress(){
            $sql = "INSERT INTO recognised_ips(user_id,ip_address) VALUES(?,?) ";
			$stmt = $this->db_conn->prepare($sql);
			$stmt->execute(array($this->user_id,$this->ip_address));
        }
    }
?>