<?php

	class Login{
		private $email;
		private $password;
		public $db_conn;

		public function __construct($email, $password){
			$this->email = $email;
			$this->password = $password;
			$this->db_conn = new PDO('mysql:host=localhost;dbname=mcenggnf_mcentrale','mcenggnf_user','project');
			
		}

		public function checkEmail(){
			$sql = "SELECT COUNT(email) AS num FROM users WHERE email = :email";
			    $stmt = $this->db_conn->prepare($sql);
			    
			    $stmt->bindValue(':email', $this->email);
			    $stmt->execute();
			    
			    $row = $stmt->fetch(PDO::FETCH_ASSOC);
  				
   				 if($row['num'] == 0){
    			    return false;
  				  }else{
  				  	return true;
  				  }
		}

		public function getEmail(){
			return $this->email;
		}


		public function getPassword(){
			return $this->password;
		}

		public function get_hashed_password(){
			$stmt = $this->db_conn->prepare('SELECT password FROM users WHERE email = ?');
        	$stmt->execute(array($this->getEmail()));
        	$row = $stmt->fetch(PDO::FETCH_ASSOC);
        	return $row['password'];
		}

		public function getUserId(){
			$stmt = $this->db_conn->prepare('SELECT id FROM users WHERE email = ?');
        	$stmt->execute(array($this->getEmail()));
        	$row = $stmt->fetch(PDO::FETCH_ASSOC);
        	return $row['id'];
		}
		
		public function getRole(){
			$stmt = $this->db_conn->prepare('SELECT role FROM users WHERE email = ?');
        	$stmt->execute(array($this->getEmail()));
        	$row = $stmt->fetch(PDO::FETCH_ASSOC);
        	return $row['role'];
		}

		public function setAll(){
			$stmt = $this->db_conn->prepare('SELECT * FROM users WHERE email = ?');
        	$stmt->execute(array($this->getEmail()));
        	$row = $stmt->fetch();
        	setcookie('nice_email',$row['email'], time() + (36000),'/');
        	setcookie('nice_firstname',$row['firstname'], time() + (36000),'/');
        	setcookie('nice_lastname',$row['lastname'], time() + (36000),'/');
			setcookie('mice_id',$row['id'], time() + (36000),'/');
        	setcookie('signin_message',"You're welcome ".$row['firstname'].' '.$row['lastname'], time() + (10));
        	setcookie('nice_role',$row['role'], time() + (36000),'/');
        	setcookie('nice_picture',$row['profile_picture'],time()+(36000),'/');
		}

		public function login(){

			if($this->checkEmail() === true){

			$email = $this->getEmail();
			$password = md5($this->getPassword());

			$hashed = $this->get_hashed_password();
				if($password === $hashed ){
					$this->setAll();
					return true;
					}else{
					return false;
				}

			}
	}

		public function totalLogin(){
			$login = $this->login();
			$role = $this->getRole();

			$ip_check = $this->checkIpAddress();
			
				if(($role == '1' || $role == '2') && ($login == true)){		
					header('Location: dashboard/');
				}
				else if($login ==  true){
					
					header('Location: user/');
					
		}
	}

		public function checkIpAddress(){
			$ip = $this->getRealIpAddr();
			$user_id = $this->getUserId();
			$sql = "SELECT COUNT(*) AS num FROM recognised_ips WHERE user_id = ? and ip_address = ?";
			    $stmt = $this->db_conn->prepare($sql);
			    $stmt->execute(array($user_id,$ip));
			    $row = $stmt->fetch(PDO::FETCH_ASSOC);
   				 if($row['num'] == 1){
    			    return true;
  				  }else{
  				  	return false;
  				  }
				
		}

		public function getRealIpAddr(){
			$ipaddress = '';
    if (!empty($_SERVER['HTTP_CLIENT_IP'])){
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		}else if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}else if(!empty($_SERVER['HTTP_X_FORWARDED'])){
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		}else if(!empty($_SERVER['HTTP_FORWARDED_FOR'])){
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		}else if(!empty($_SERVER['HTTP_FORWARDED'])){
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
		}else if(!empty($_SERVER['REMOTE_ADDR'])){
        $ipaddress = $_SERVER['REMOTE_ADDR'];
		}
 
    return $ipaddress;
		}

	}

?>