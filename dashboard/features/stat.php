<?php
    class Statistics{
        public $db_conn;

        public function __construct(){
            $this->db_conn = new PDO('mysql:host=localhost;dbname=mcenggnf_mcentrale','mcenggnf_user','project');

        }

        public function countUsers(){
            $sql = "SELECT COUNT(*) AS num FROM users";
			$stmt = $this->db_conn->prepare($sql);
			$stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            return $row['num'];
        }

        public function getUserId($account_id){
            $sql = "SELECT user_id FROM accounts WHERE id = ?";
			$stmt = $this->db_conn->prepare($sql);
			$stmt->execute([$account_id]);
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            return $row['user_id'];
        }

        public function getUsers(){
            if($_COOKIE['nice_role'] === '2'){
                $sql = "SELECT * FROM users WHERE role = '0'";
                $stmt = $this->db_conn->prepare($sql);
                $stmt->execute();
            }else{
                $sql = "SELECT * FROM users WHERE user_created_id = ? AND role = ? | role = ? ";
                $stmt = $this->db_conn->prepare($sql);
                $stmt->execute([$_COOKIE['mice_id'],'0','1']);
            }
            
            
            $row = $stmt->fetchAll();
            return $row;
        }

        public function getUserAccount(){
            $sql = "SELECT * FROM accounts WHERE admin_id = ?";
			$stmt = $this->db_conn->prepare($sql);
            $stmt->execute([$_COOKIE['mice_id']]);
            $row = $stmt->fetchAll();
            return $row;
        }

        public function deleteUser($id){
            $sql = "DELETE  FROM users WHERE id = ?";
			$stmt = $this->db_conn->prepare($sql);
			$stmt->execute(array($id));
           if($stmt->rowCount() == 1){
               setcookie('delete_success','User Deleted Successfully',time()+5,'/');
               header('Location: users.php');
           }
        }

        public function getAccount(){
            $sql = "SELECT * FROM accounts";
			$stmt = $this->db_conn->prepare($sql);
			$stmt->execute();
            $row = $stmt->fetchAll();
            return $row;
        }
        public function changePassword($pass,$id){
            $sql = "UPDATE users SET password = ? WHERE id = ?";
			$stmt = $this->db_conn->prepare($sql);
            $stmt->execute(array(md5($pass),$id));
            if($stmt->rowCount() > 0){
                setcookie('password_change','Password has been changed Successfully.',time() + (5),'/');
                    header('Location: change_password.php');
            }
        }
    }

    class Account{
        private $user_id;
        private $account_number;
        private $account_type;
        private $balance;
        private $credit;
        public $db_conn;

        public function __construct($user_id,$account_number,$account_type,$balance,$credit){
            $this->user_id = $user_id;
            $this->account_number = $account_number;
            $this->account_type = $account_type;
            $this->balance = $balance;
            $this->credit = $credit;
		$this->db_conn = new PDO('mysql:host=localhost;dbname=mcenggnf_mcentrale','mcenggnf_user','project');
              
        }

        public function getUser(){
            return $this->user_id;
        }

        public function getAccountNumber(){
            return $this->account_number;
        }

        public function getAccountType(){
            return $this->account_type;
        }

        public function getBalance(){
            return $this->balance;
        }

        public function getCredit(){
            return $this->credit;
        }

        public function allocateAccount(){
            $user = $this->getUser();
            $account = $this->getAccountNumber();
            $type = $this->getAccountType();
            $balance = $this->getBalance();
            $credit = $this->getCredit();

            $sql = "INSERT INTO accounts (user_id, account_number, account_type, balance, credit,admin_id)	VALUES ( ?,?,?,?,?,?)";
				
				$stmt = $this->db_conn->prepare($sql);
				$stmt->execute(array($user,$account,$type,$balance,$credit,$_COOKIE['mice_id']));
				if($stmt->rowCount() > 0){
                    setcookie('create_success','Account has been allocated Successfully.',time() + (10),'/');
						header('Location: accounts.php');
				}
        }

        public function checkAccountNumber(){
            $sql = "SELECT COUNT(*) AS num FROM accounts WHERE account_number = ?";
				
			$stmt = $this->db_conn->prepare($sql);
			$stmt->execute(array($this->account_number));
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
                if($row['num'] > 0){
                    return true;
                }else{
                    return false;
                }		
        }
    }

    class PinGenerator{
        public $db_conn;

        public function __construct(){
            $this->db_conn = new PDO('mysql:host=localhost;dbname=mcenggnf_mcentrale','mcenggnf_user','project');

        }

        public function generate(){
            $pin = $this->pin();
            $sql = "INSERT INTO pins (pin, status,user_id)	VALUES ( ?,?,?)";
				
				$stmt = $this->db_conn->prepare($sql);
				$stmt->execute(array($pin,'0',$_COOKIE['mice_id']));
				if($stmt->rowCount() > 0){
                    setcookie('create_success','New Pin Generated: '.$pin,time() + (10),'/');
						header('Location: generate_pin.php');
				}
        }

        public function showPins(){
            $sql = "SELECT * FROM pins WHERE user_id = ?";	
			$stmt = $this->db_conn->prepare($sql);
			$stmt->execute([$_COOKIE['mice_id']]);
            $row = $stmt->fetchAll();
            return $row;
        }

        public function pin(){
        $permitted_chars = '0123456789';
        return substr(str_shuffle($permitted_chars), 0, 6);
        }
    }

    class AccountUpdate{
        private $account_id;
        private $balance;
        private $credit;
        public $db_conn;

        public function __construct($account_id,$balance,$credit){
            $this->account_id = $account_id;
            $this->balance = $balance;
            $this->credit = $credit;
            $this->db_conn = new PDO('mysql:host=localhost;dbname=eastwest','mcenggnf_user','project');
        }

        public function updateAccount(){
            $sql = "UPDATE accounts SET balance = ?, credit = ? WHERE id = ?";
			$stmt = $this->db_conn->prepare($sql);
            $stmt->execute(array($this->balance,$this->credit,$this->account_id));
            if($stmt->rowCount() > 0){
                setcookie('update_success','Account Balance and Credit has been Updated Successfully',time() + (10),'/');
                    header('Location: edit_balance.php');
            }
        }
    }
class TransactionStatus{
    private $user_id;
    private $code;
    public $db_conn;

    public function __construct($user_id,$code){
        $this->user_id = $user_id;
        $this->code = $code;
       	$this->db_conn = new PDO('mysql:host=localhost;dbname=mcenggnf_mcentrale','mcenggnf_user','project');
    }

    public function updateTransactionStatus(){
        $sql = "UPDATE users SET transaction_status = ? WHERE id = ?";
			$stmt = $this->db_conn->prepare($sql);
            $stmt->execute(array($this->code,$this->user_id));
            if($stmt->rowCount() > 0){
                setcookie('update_success','Transaction Status Changed',time() + (5),'/');
                    header('Location: transaction_status.php');
            }
    }

}
?>