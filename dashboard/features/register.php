<?php
class RegistrationClass{
    private $fname;
    private $lname;
    private $email;
    private $password;
    private $role;
    private $user_id;
    private $profile_pic;
    public $db_conn;
    

    public function __construct($fname,$lname,$email,$password,$role,$user_id,$profile_pic){
        $this->fname = $fname;
        $this->lname = $lname;
        $this->email = $email;
        $this->password = $password;
        $this->role = $role;
        $this->user_id = $user_id;
        $this->profile_pic = $profile_pic;
		$this->db_conn = new PDO('mysql:host=localhost;dbname=mcenggnf_mcentrale','mcenggnf_user','project');

    }

    public function getFirstName(){
        return $this->fname;
    }

    public function getLastName(){
        return $this->lname;
    }

    public function getEmail(){
        return $this->email;
    }

    public function getPassword(){
        return $this->password;
    }

    public function getRole(){
        return $this->role;
    }

		public function setEmail(){
			$sql = "SELECT COUNT(email) AS num FROM users WHERE email = :email";
			    $stmt = $this->db_conn->prepare($sql);
			    
			    $stmt->bindValue(':email', $this->email);
			    $stmt->execute();
			    
			    $row = $stmt->fetch(PDO::FETCH_ASSOC);
                
   				 if($row['num'] > 0){
    			    $result = true;
  				  }else{
                    $result = false;
                    }
                    return $result;
        }
        
        public function registerUser(){
				$fname = ucwords(strtolower($this->getFirstName()));
				$lname = ucwords(strtolower($this->getLastName()));
				$mail = $this->getEmail();
                $password  = md5($this->getPassword());
                $role = $this->getRole();
				
				$sql = "INSERT INTO users (firstname, lastname, email, password,role,user_created_id,transaction_status,profile_picture)	VALUES ( ?,?,?,?,?,?,?,?)";
				
				$stmt = $this->db_conn->prepare($sql);
				$stmt->execute(array($fname,$lname,$mail,$password,$role,$this->user_id,'1',$this->profile_pic));
				if($stmt->rowCount() > 0){
                    setcookie('register_success','Success',time() + (10),'/');
						header('Location: create_user.php');
				}
        }
        
}

?>