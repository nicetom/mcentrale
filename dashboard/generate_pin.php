<?php

require('important.php');
    $title = "Generate Pin";
    require('template/head.php');
    require('features/stat.php');
    $obj = new PinGenerator();
    $pins = $obj->showPins();
    
        if(isset($_POST['submit'])){
            $obj->generate();
        }
?>

  <body class="h-100">
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
        <?php require('template/aside.php') ?>
        <!-- End Main Sidebar -->
        <main class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
          <?php require('template/navbar.php') ?>
          <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle"><?php echo $title; ?></span>
              </div>
            </div>
            <!-- End Page Header -->
            <div class="col-md-12">
            <?php if(isset($_COOKIE['create_success'])):?>
            <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <?php echo $_COOKIE['create_success']; ?> </div>
<?php endif;?>
            <div class="card-body p-0 pb-3 text-center">
                <form method="POST">
                <button name="submit" class="btn btn-primary" type="submit"> Generate Pins</button>
                </form>
                <br>
                <div class="card-body p-0 pb-3 text-center">
            <table class='table mb-0'>
            <thead style="background:black;color:white;"> 
                <tr>
                    <th>Pin</th>
                    <th>Status</th>
                </tr>
</thead>
                <?php foreach($pins as $pin): ?>
                <tr>
                    <th><?php echo $pin['pin'];?></th>
                    <th><?php echo ($pin['status'] == 0) ?  "Valid" : "Expired";?></th>
</tr>
<?php endforeach;?>
            </table>
</div>
            </div>
</div>
          
          </div>
          <?php require('template/footer.php') ?>
        </main>
      </div>
    </div>
   
    <?php require('template/script.php') ?>
    </body>
</html>