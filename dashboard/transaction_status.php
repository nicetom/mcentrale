<?php 

require('important.php');
    $title = "Allocate Accounts";
require('template/head.php');
require('features/stat.php');
$obj = new Statistics();
$users = $obj->getUsers();

$user = isset($_POST['user_id']) ? $_POST['user_id']: "";
$message_code = isset($_POST['code']) ? $_POST['code']: "";

    if(isset($_POST['submit'])){
        $obj = new TransactionStatus($user,$message_code);
        $obj->updateTransactionStatus();
    }
?>
  <body class="h-100">
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
        <?php require('template/aside.php') ?>
        <!-- End Main Sidebar -->
        <main class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
          <?php require('template/navbar.php') ?>
          <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle"><?php echo $title; ?></span>
              </div>
            </div>
            <!-- End Page Header -->
            <div class="col-md-12">
            <?php if(isset($_COOKIE['update_success'])):?>
            <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <?php echo $_COOKIE['update_success']; ?> </div>
<?php endif;?>
          <form class="form" method="POST">
              <div class="form-row">
                  <div class="form-group col-md-6 col-sm-12">
                  <label>User</label>
              <select name="user_id" class="form-control" required>
                  <?php for($i = 0; $i < count($users); $i++):?>
                  <option value ="<?php echo $users[$i]['id']?>"><?php echo $users[$i]['firstname']." ".$users[$i]['lastname']."(".$users[$i]['email'].") - ".$users[$i]['transaction_status'];?></option>
                <?php endfor;?>
              </select>
</div>

            <div class="form-group col-md-6 col-sm-12">
            <label>Transaction Message Code</label>
              <select name="code" class="form-control" required>
              <option value="">Select A Message Code...</option>
                  <option>0</option>
                  <option>1</option>
                  <option>2</option>
              </select></div>

</div>
<div class="form-group">
    <button type="submit" class="btn btn-primary" name="submit">Submit</button>
</div>
          </form>
            </div>
<div class="card" style="padding:10px;">
            <table class="table table-hover">
            <h2>Note</h2>
            <tr>
            <th>Code</th>
            <th>Message</th>
            </tr>
            <tr>
            <td>0</td>
            <td>Success Message</td>
            </tr>
            <tr>
            <td>1</td>
            <td>Oops, you need to contact your financial advisor</td>
            </tr>
            <tr>
            <td>2</td>
            <td>Shows Form for further details</td>
            </tr>
            </table>
            </div>
          </div>
          <?php require('template/footer.php') ?>
        </main>
      </div>
    </div>
   
    <?php require('template/script.php') ?>
    </body>
</html>