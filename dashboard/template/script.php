<script src="scripts/jquery-3.3.1.min.js" ></script>
    <script src="scripts/popper.min.js"></script>
    <script src="styles/bootstrap-4.0.0/dist/js/bootstrap.min.js"></script>
    <script src="scripts/chart.min.js"></script>
    <script src="scripts/shards.min.js"></script>
    <script src="scripts/sharre.min.js"></script>
    <script src="scripts/extras.1.0.0.min.js"></script>
    <script src="scripts/shards-dashboards.1.0.0.min.js"></script>
    <script src="scripts/app/app-blog-overview.1.0.0.js"></script>
    <script src="scripts/validator.js"></script>
    <script src="dropify/dist/js/dropify.js"></script>
    <script>
        $(document).ready(function () {
            // Basic
            $('.dropify').dropify();
            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez'
                    , replace: 'Glissez-déposez un fichier ou cliquez pour remplacer'
                    , remove: 'Supprimer'
                    , error: 'Désolé, le fichier trop volumineux'
                }
            });
            // Used events
            var drEvent = $('#input-file-events').dropify();
            drEvent.on('dropify.beforeClear', function (event, element) {
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });
            drEvent.on('dropify.afterClear', function (event, element) {
                alert('File deleted');
            });
            drEvent.on('dropify.errors', function (event, element) {
                console.log('Has Errors');
            });
            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function (e) {
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                }
                else {
                    drDestroy.init();
                }
            })
        });

        function checkData(){
            var fname = document.getElementById('fname').value;
            var lname = document.getElementById('lname').value;
            var pass1 = document.getElementById('pass1').value;
            var pass2 = document.getElementById('pass2').value;
            if(fname.length > 0 && lname.length > 0 && pass1.length > 0 && pass2.length > 0){
                if(pass1 === pass2){
                    document.getElementById('em1').disabled = false;
                }else{
                    document.getElementById('em1').disabled = true;
                }
            }
        }
        
        function checkEmail(email){
            
            var rx = RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
            if(email.length > 0 && rx.test(email)){
                document.getElementById('em').disabled = false;
            }else{
                document.getElementById('em').disabled = true;
            }
        }
    </script>