<?php

require('important.php');
    $title = "Create User ";
require('template/head.php');
require('features/register.php');
$fname = isset($_POST['fname']) ? $_POST['fname']: "";
$lname = isset($_POST['lname']) ? $_POST['lname']: "";
$mail = isset($_POST['mail']) ? $_POST['mail']: "";
$pass1 = isset($_POST['pass1']) ? $_POST['pass1']: "";
$pass2 = isset($_POST['pass2']) ? $_POST['pass2']: "";
$role = isset($_POST['role']) ? $_POST['role']: "";

   if(isset($_POST['submit'])){
        if(isset($_FILES['profile_picture'])){
      $file_name = $_FILES['profile_picture']['name'];
      $file_size =$_FILES['profile_picture']['size'];
      $file_tmp =$_FILES['profile_picture']['tmp_name'];
      $file_type=$_FILES['profile_picture']['type'];
      $file_ext=strtolower(end(explode('.',$_FILES['profile_picture']['name'])));
      
      $extensions= array("jpeg","jpg","png");
      
      if(in_array($file_ext,$extensions)=== false){
         $upload_error = "Extension not allowed, please choose a JPEG or PNG file.";
      }
      
      if(empty($upload_error)==true){
          $new_location = "../user/uploads/".time().'.'.$file_ext;
         move_uploaded_file($file_tmp,$new_location);
         echo "Success";
      }else{
         var_dump($upload_error);
      }
            $obj = new RegistrationClass($fname,$lname,$mail,$pass1,$role,$_COOKIE['mice_id'],$new_location);
            if($obj->setEmail()){
                $error = 'Email already taken';
            }else{
                $obj->registerUser();
                
            }
        }
       
    }
?>

  <body class="h-100">
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
        <?php require('template/aside.php') ?>
        <!-- End Main Sidebar -->
        <main class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
          <?php require('template/navbar.php') ?>
          <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle"><?php echo $title; ?></span>
              </div>
            </div>
            <!-- End Page Header -->
            <div class="col-md-12">
            <?php if(isset($_COOKIE['register_success'])):?>
            <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> User has been created Successfully.. </div>
<?php endif;?>
            <form data-toggle="validator" novalidate="true" method="POST" enctype="multipart/form-data">
        <div class="form-group">
            <input type="email" class="form-control" id="mail" name="mail" placeholder="Email" required value="<?php echo $mail; ?>">
            <span class="help-block with-errors"></span>
            <?php if(isset($error)): ?>
            <center><span class="help-block"><?php echo $error;?></span></center>
            <?php endif;?>                           
        </div>
    
        <div class="form-group">
        <input type="text" data-minlength="3" class="form-control" name="fname" placeholder="First Name" required value="<?php echo $fname; ?>"/>
        <center><span class="help-block with-errors"></span></center>
        </div>
        <div class="form-group">
        <input type="text" data-minlength="3" class="form-control" name="lname" placeholder="Last Name" required value="<?php echo $lname; ?>"/>
        <center><span class="help-block with-errors"></span></center>
        </div>
        <div class="form-group">
        <input data-toggle="validator" id="pass1" class="form-control"  data-minlength="8" type="password"  name="pass1" placeholder="Password" required autocomplete/>
        <center><span class="help-block">Minimum of 8 characters</span></center> </div>
        <div class="form-group">
        <input name="pass2" type="password" id="pass2" class="form-control" id="password" data-match="#pass1" data-match-error="Whoops, these don't match" placeholder="Confirm Password" required autocomplete/>
        <center><span class="help-block with-errors"></span> </center>
        </div>
        <div class="form-group">
        <select name="role" class="form-control">
            <option value="0">Normal User</option>
            
   <?php if($_COOKIE['nice_role'] == 2):?>
   <option value="1">Admin</option>
   <option value="2">Super Admin</option>
<?php endif;?>
        </select>
        </div>
        <div class="form-group" >
            <input type="file" class="form-control" name="profile_picture" required >
        </div>
        <br>
        <div class="form-group">
        <input type="submit" name="submit" class="btn btn-primary col-md-offset-2" value="Submit">
        </div>
        
        
        </form>
            </div>
          
          </div>
          <?php require('template/footer.php') ?>
        </main>
      </div>
    </div>
   
    <?php require('template/script.php') ?>
    </body>
</html>