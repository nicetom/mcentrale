<?php 
require('important.php');
    $title = "View Users";
require('template/head.php');
require('features/stat.php');
$obj = new Statistics();
$users = $obj->getUsers();
if(isset($_POST['delete'])){
$obj->deleteUser($_POST['id']);
}
?>

  <body class="h-100">
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
        <?php require('template/aside.php') ?>
        <!-- End Main Sidebar -->
        <main class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
          <?php require('template/navbar.php') ?>
          <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle"><?php echo $title; ?></span>
              </div>
            </div>
            
            <!-- End Page Header -->
            <div class="card-body p-0 pb-3 text-center">
            <?php if(isset($_COOKIE['delete_success'])):?>
            <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <?php echo $_COOKIE['delete_success'];?>  </div>
<?php endif;?>
            <table class='table mb-0'>
            <thead style="background:black;color:white;"> 
                <tr>
                    <th>Firstname</th>
                    <th>Lastname</th>
                    <th>Email</th>
                    <th>Role</th>
                    <?php if($_COOKIE['nice_role'] == 2):?>
                    <th>Action</th>
<?php endif;?>
                </tr>
</thead>
                <?php for($i = 0; $i < count($users); $i++): ?>
                <tr>
                    <td><?php echo $users[$i]['firstname'];?></td>
                    <td><?php echo $users[$i]['lastname'];?></td>
                    <td><?php echo $users[$i]['email'];?></td>
                    <?php  if($users[$i]['role'] == '0'):?>
                    <td>Normal User</td>
                    <?php  elseif($users[$i]['role'] == '1'):?>
                    <td>Admin</td>
                    <?php  elseif($users[$i]['role'] == '2'):?>
                    <td>Super Admin</td>
<?php endif;?>
<?php if($_COOKIE['nice_role'] == 2):?>
<?php if(($users[$i]['role'] != 2)): ?>
                    <td>
                      <form method="POST">
                        <input type="hidden" value="<?php echo $users[$i]['id'];?>" name="id">
                        <button class="btn btn-primary" name="delete">Delete</button>
                      </form>
                    </td>
<?php endif; ?>
<?php endif;?>
                    
</tr>
<?php endfor;?>
            </table>
</div>
          
          </div>
          <?php require('template/footer.php') ?>
        </main>
      </div>
    </div>
   
    <?php require('template/script.php') ?>
    </body>
</html>