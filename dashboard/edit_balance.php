<?php 

require('important.php');
    $title = "Allocate Accounts";
require('template/head.php');
require('features/stat.php');
$obj = new Statistics();
$accounts = $obj->getAccount();

$account_id = isset($_POST['account_id']) ? $_POST['account_id']: "";
$balance = isset($_POST['balance']) ? $_POST['balance']: "";
$credit = isset($_POST['credit']) ? $_POST['credit']: "";

    if(isset($_POST['submit'])){
        $obj2 = new AccountUpdate($account_id,$balance,$credit);
        $obj2->updateAccount();
    }
?>
  <body class="h-100">
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
        <?php require('template/aside.php') ?>
        <!-- End Main Sidebar -->
        <main class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
          <?php require('template/navbar.php') ?>
          <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle"><?php echo $title; ?></span>
              </div>
            </div>
            <!-- End Page Header -->
            <div class="col-md-12">
            <?php if(isset($_COOKIE['update_success'])):?>
            <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <?php echo $_COOKIE['update_success']; ?> </div>
<?php endif;?>
          <form class="form" method="POST">
              <div class="form-row">
                  <div class="form-group col-md-6 col-sm-12">
                  <label>Account</label>
              <select name="account_id" class="form-control" required>
              <option value="">Select an Account</option>
                  <?php foreach($accounts as $account):?>
                  <option value ="<?php echo $account['id']?>"><?php echo $account['account_number']."- (".$account['account_type'].") $".$account['balance']." - $".$account['credit'];?></option>
<?php endforeach;?>
              </select>
</div>              
              <div class="form-group col-md-6 col-sm-12">
                <label>New Balance</label>
              <input class="form-control" name="balance" type="text" maxlength="10" step=".01" pattern="^\d+(?:\.\d{1,2})?$" required value="<?php echo $balance ?>">
              </div>

              <div class="form-group col-md-6 col-sm-12">
                <label>New Credit</label>
              <input class="form-control" name="credit" type="text" maxlength="10" step=".01" pattern="^\d+(?:\.\d{1,2})?$" required value="<?php echo $credit ?>">
              </div>
</div>
<div class="form-group">
    <button type="submit" class="btn btn-primary" name="submit">Submit</button>
</div>
          </form>  </div>
          
          </div>
          <?php require('template/footer.php') ?>
        </main>
      </div>
    </div>
   
    <?php require('template/script.php') ?>
    </body>
</html>