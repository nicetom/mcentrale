var vm = new Vue({
    el: '#wrapper',
    data: {
        email :"",
        fname : "",
        lname : "",
        pass1: "",
        pass2: "",
    },
    methods: {
        
    },
    watch:{
        email: function(){
            var rx = RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
            if(this.email.length > 0 && rx.test(this.email)){
                document.getElementById('em').disabled = false;
            }else{
                this.emailError = "Email field cannot be empty";
                document.getElementById('em').disabled = true;
            }
        }
    }
});