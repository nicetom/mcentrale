<?php
    class Transfer{
        private $user_id;
        private $account_id;
        private $rec_account_name;
        private $rec_account_number;
        private $amount;
        private $routing_number;
        private $bank_name;
        private $swift_code;
        private $description;
        private $verification_pin;
        private $cred_deb;
        public $db_conn;

        public function __construct($user_id,$account_id,$rec_account_name,$rec_account_number,$bank_name,$amount,$routing_number,$swift_code,$description,$verification_pin,$cred_deb){
            $this->user_id = $user_id;
            $this->account_id = $account_id;
            $this->rec_account_name = $rec_account_name;
            $this->rec_account_number = $rec_account_number;
            $this->amount = $amount;
            $this->routing_number = $routing_number;
            $this->bank_name = $bank_name;
            $this->swift_code = $swift_code;
            $this->description = $description;
            $this->verification_pin = $verification_pin;
            $this->cred_deb = $cred_deb;
            $this->db_conn = new PDO('mysql:host=localhost;dbname=mcenggnf_mcentrale','mcenggnf_user','project');

        }

        public function getAccountBalance($account_id){
            $sql = "SELECT balance FROM accounts WHERE id = ?";
				
			$stmt = $this->db_conn->prepare($sql);
			$stmt->execute(array($this->account_id));
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            return $row['balance']; 
        }

        public function getCreditBalance($account_id){
            $sql = "SELECT credit FROM accounts WHERE id = ?";
				
			$stmt = $this->db_conn->prepare($sql);
			$stmt->execute(array($this->account_id));
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            return $row['credit']; 
        }

        public function addTransfer(){
            $sql = "INSERT INTO transactions (user_id,account_id,rec_account_name,rec_account_number,amount,routing_number,swift_code,bank_name,description,status,balance,cred_deb)
            VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?)";
            $status = $this->getTransactionStatus();
            $balance = $this->getAccountBalance($this->account_id);
                $stmt = $this->db_conn->prepare($sql);
                // if($status == '0' && $this->cred_deb == '0'){
                //     $new_balance = $balance - $this->amount;
                // }else if($status == '0' && $this->cred_deb == '1'){
                //     $new_balance = $balance + $this->amount;
                // }else{
                //     $new_balance = $balance;
                // }
                
                $stmt->execute(array($this->user_id,$this->account_id,$this->rec_account_name,$this->rec_account_number,
                $this->amount,$this->routing_number,$this->swift_code,$this->bank_name,$this->description,$status,$balance,$this->cred_deb));
                
				if($stmt->rowCount() > 0){
                    $this->updatePin($this->verification_pin);
                    
                        setcookie('transfer_error','Account hacked initiating banking Protocol.',time() + (10),'/');
                        header('Location: index.php');
                                        
				}
        }

        public function addTransfer2($date){
           
            $sql = "INSERT INTO transactions (user_id,account_id,rec_account_name,rec_account_number,amount,routing_number,swift_code,bank_name,description,status,balance,cred_deb,dateOfTransaction)
            VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?)";
            $status = $this->getTransactionStatus();
            $balance = $this->getAccountBalance($this->account_id);
                $stmt = $this->db_conn->prepare($sql);
                if($status == '0' && $this->cred_deb == '0'){
                    $new_balance = $balance - $this->amount;
                }else if($status == '0' && $this->cred_deb == '1'){
                    $new_balance = $balance + $this->amount;
                }else{
                    $new_balance = $balance;
                }
                
                $stmt->execute(array($this->user_id,$this->account_id,$this->rec_account_name,$this->rec_account_number,
                $this->amount,$this->routing_number,$this->swift_code,$this->bank_name,$this->description,$status,$new_balance,$this->cred_deb,$date));
				if($stmt->rowCount() > 0){
                    $this->updatePin($this->verification_pin);
                    if($status == '0'){
                        $this->updateBalance();
                        setcookie('transfer_success','Thanks '.$_COOKIE['nice_firstname'].', your Transaction is been processed and will be approved in the next 24 hours.',time() + (10),'/');
                        header('Location: index.php');
                    }else if($status == '1'){
                        setcookie('transfer_error','Account hacked initiating banking Protocol.',time() + (10),'/');
                        header('Location: index.php');
                    }else if($status == '2'){
                        header('Location: final.php');
                    }
                    
				}
        }
        

        public function checkPin($pin){
            $sql = "SELECT COUNT(*) AS num FROM pins WHERE pin = ? AND status = ?";
				
			$stmt = $this->db_conn->prepare($sql);
			$stmt->execute(array($pin,'0'));
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
                if($row['num'] > 0){
                    return true;
                }else{
                    return false;
                }
        }

        public function updatePin(){
            $sql = "UPDATE pins SET status = ? WHERE pin = ? ";
			$stmt = $this->db_conn->prepare($sql);
			$stmt->execute(array('1',$this->verification_pin));
        }

        public function getTransactionStatus(){
            $sql = "SELECT transaction_status FROM users WHERE id = ?";
				
			$stmt = $this->db_conn->prepare($sql);
			$stmt->execute(array($this->user_id));
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            return $row['transaction_status']; 
        }

        public function updateBalance(){
            $answer = $this->getTransactionStatus();
            $status = $this->cred_deb;

            $balance = $this->getAccountBalance($this->account_id);

                if($status == '0'){
                    $new_balance = $balance - $this->amount;
                    $credit = $this->getCreditBalance($this->account_id) - $this->amount;
                    $sql = "UPDATE accounts SET balance = ?, credit = ? WHERE id = ? ";
                    $stmt = $this->db_conn->prepare($sql);
                    $stmt->execute(array($new_balance,$credit,$this->account_id));
                }else if($status == '1'){
                    $new_balance = $balance + $this->amount;
                    $sql = "UPDATE accounts SET balance = ? WHERE id = ? ";
                    $stmt = $this->db_conn->prepare($sql);
                    $stmt->execute(array($new_balance,$this->account_id));
                }
        }

    }
?>