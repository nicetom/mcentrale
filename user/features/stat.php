<?php
    class Statistics{
        public $db_conn;

        public function __construct(){
            // $this->db_conn = new PDO('mysql:host=localhost;dbname=mcenggnf_mcentrale','mcenggnf_user','project');
            $this->db_conn = new PDO('mysql:host=localhost;dbname=mcenggnf_mcentrale','mcenggnf_user','project');

        }

        public function countUsers(){
            $sql = "SELECT COUNT(*) AS num FROM users";
			$stmt = $this->db_conn->prepare($sql);
			$stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            return $row['num'];
        }

        public function getAvailableBalance(){
            $sql = "SELECT * FROM accounts WHERE user_id = ?";
			$stmt = $this->db_conn->prepare($sql);
			$stmt->execute(array($_COOKIE['mice_id']));
            $row = $stmt->fetchAll();
            $sum = 0;

                foreach($row as $credit){
                    $sum = $sum + $credit['credit'];
                }
            return $sum;
        }

        public function getTotalBalance(){
            $sql = "SELECT * FROM accounts WHERE user_id = ?";
			$stmt = $this->db_conn->prepare($sql);
			$stmt->execute(array($_COOKIE['mice_id']));
            $row = $stmt->fetchAll();
            $sum = 0;

                foreach($row as $credit){
                    $sum = $sum + $credit['balance'];
                }
            return $sum;
        }
    }

    class AccountData{
        private $user_id;
        public $db_conn;

        public function __construct($user_id){
            $this->user_id = $user_id;
            $this->db_conn = new PDO('mysql:host=localhost;dbname=mcenggnf_mcentrale','mcenggnf_user','project');

        }

        public function getUser(){
            return $this->user_id;
        }

        public function getUserAccount(){
            $sql = "SELECT * FROM accounts WHERE user_id = ?";
			$stmt = $this->db_conn->prepare($sql);
			$stmt->execute(array($this->getUser()));
            $row = $stmt->fetchAll();
            return $row;
        }

        public function getTransactionHistory(){
            $sql = "SELECT * FROM transactions WHERE user_id = ? ORDER BY dateOfTransaction DESC";
			$stmt = $this->db_conn->prepare($sql);
			$stmt->execute(array($this->getUser()));
            $row = $stmt->fetchAll();
            return $row;
        }

    }

?>