<?php
    class Security{
        private $answer;
        private $pin;
        private $ip_address;
        private $user_id;
        public $db_conn;

        public function __construct($answer,$pin,$ip_address,$user_id){
            $this->answer = $answer;
            $this->pin = $pin;
            $this->ip_address = $ip_address;
            $this->user_id = $user_id;
		$this->db_conn = new PDO('mysql:host=localhost;dbname=mcenggnf_mcentrale','mcenggnf_user','project');
                
        }

        public function checkPin(){
            $sql = "SELECT COUNT(*) AS num FROM pins WHERE pin = ? AND status = ?";
				
			$stmt = $this->db_conn->prepare($sql);
			$stmt->execute(array($this->pin,'0'));
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
                if($row['num'] > 0){
                    return true;
                }else{
                    return false;
                }
        }

        public function finalLogin(){
            if($this->checkPin() && $this->answerToQuestion() ){
                    $this->updatePin();
                    $this->addIpAdress();
                    header("Location: dashboard/");
            }
        }

        public function answerToQuestion(){
            if($this->answer == 'mary'){
                $response = true;
            }else{
                $response = false;
            }
            return $response;
        }

        public function updatePin(){
            $sql = "UPDATE pins SET status = ? WHERE pin = ? ";
			$stmt = $this->db_conn->prepare($sql);
			$stmt->execute(array('1',$this->pin));
        }

        public function addIpAdress(){
            $sql = "INSERT INTO recognised_ips(user_id,ip_address) VALUES(?,?) ";
			$stmt = $this->db_conn->prepare($sql);
			$stmt->execute(array($this->user_id,$this->ip_address));
        }
    }
?>