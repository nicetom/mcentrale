<?php
class RegistrationClass{
    private $fname;
    private $lname;
    private $email;
    private $password;
    public $db_conn;
    

    public function __construct($fname,$lname,$email,$password){
        $this->fname = $fname;
        $this->lname = $lname;
        $this->email = $email;
        $this->password = $password;
		$this->db_conn = new PDO('mysql:host=localhost;dbname=mcenggnf_mcentrale','mcenggnf_user','project');
           
    }

    public function getFirstName(){
        return $this->fname;
    }

    public function getLastName(){
        return $this->lname;
    }

    public function getEmail(){
        return $this->email;
    }

    public function getPassword(){
        return $this->password;
    }

		public function setEmail(){
			$sql = "SELECT COUNT(email) AS num FROM users WHERE email = :email";
			    $stmt = $this->db_conn->prepare($sql);
			    
			    $stmt->bindValue(':email', $this->email);
			    $stmt->execute();
			    
			    $row = $stmt->fetch(PDO::FETCH_ASSOC);
                
   				 if($row['num'] > 0){
    			    $result = true;
  				  }else{
                    $result = false;
                    }
                    return $result;
        }
        
        public function registerStudent(){
				$fname = ucfirst(strtolower($this->getFirstName()));
				$lname = ucfirst(strtolower($this->getLastName()));
				$mail = $this->getEmail();
				$password  = md5($this->getPassword());
				
				$sql = "INSERT INTO users (firstname, lastname, email, password)	VALUES ( ?,?,?,?)";
				
				$stmt = $this->db_conn->prepare($sql);
				$stmt->execute(array($fname,$lname,$mail,$password));
				if($stmt->rowCount() > 0){
                    setcookie('register_success','Success',time() + (10),'/');
						header('Location: create_user.php');
				}
        }
        
}

?>