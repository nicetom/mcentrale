<?php
    class Identity{
        private $user_id;
        private $firstname;
        private $lastname;
        private $month;
        private $day;
        private $year;
        private $email;
        private $area_code;
        private $phone;
        private $social;
        private $address;
        private $state;
        private $city;
        private $postal_code;
        private $passport;
        public $db_conn;

        public function __construct($user_id,$firstname,$lastname,$month,$day,$year,$email,$area_code,$phone,$social,$address,$state,$city,$postal_code,$passport){
            $this->user_id = $user_id;
            $this->firstname = $firstname;
            $this->lastname = $lastname;
            $this->month = $month;
            $this->day = $day;
            $this->year = $year;
            $this->email = $email;
            $this->area_code = $area_code;
            $this->phone = $phone;
            $this->social = $social;
            $this->address = $address;
            $this->state = $state;
            $this->city = $city;
            $this->postal_code = $postal_code;
            $this->passport = $passport;
            $this->db_conn = new PDO('mysql:host=localhost;dbname=mcenggnf_mcentrale','mcenggnf_user','project');

        }

        public function createIdentity(){
            $name = ucfirst($this->firstname)." ".ucfirst($this->lastname);
            $dob = $this->day."/".$this->month."/".$this->year;

            $sql = "INSERT INTO identities(name,date_of_birth,email,area_code,phone,social,address,state,city,postal_code,passport,user_id) VALUES(?,?,?,?,?,?,?,?,?,?,?,?) ";
			$stmt = $this->db_conn->prepare($sql);
            $stmt->execute(array($name,$dob,$this->email,$this->area_code,$this->phone,$this->social,$this->address,$this->state,$this->city,$this->postal_code,$this->passport,$this->user_id));
            if($stmt->rowCount() > 0){
                setcookie('transfer_success','Your identity is been verified',time() + 5);
                header('Location: index.php');
            }else{
                    print_r($stmt->errorInfo());
            }
        }
    }
?>