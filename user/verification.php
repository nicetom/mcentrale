<?php
require('important.php');
require('features/transaction.php');
if(!isset($_POST['routing_digit']) && !isset($_POST['account_id']) && !isset($_POST['rec_account_name']) && !isset($_POST['rec_account_number']) &&
 !isset($_POST['bank'])&& !isset($_POST['swift_code']) && !isset($_POST['amount'])){
     header('Location: transfer.php');
 }
$routing_digit = $_POST['routing_digit'];
$account_id = $_POST['account_id'];
$rec_account_name = $_POST['rec_account_name'];
$rec_account_number =$_POST['rec_account_number'];
$bank_name = $_POST['bank'];
$swift_code = $_POST['swift_code'];
$amount = $_POST['amount'];
$description =$_POST['description'];

$verification_code = isset($_POST['verification_code']) ? $_POST['verification_code']: "";
$cred_deb = isset($_POST['cred_deb']) ? $_POST['cred_deb']: "";

if(isset($_POST['submit2'])){
    $obj = new Transfer($_COOKIE['mice_id'],$account_id,$rec_account_name,$rec_account_number,$bank_name,$amount,$routing_digit,$swift_code,$description,$verification_code,$cred_deb);
    if($obj->checkPin($verification_code) == false){
        $error = "Pin is Invalid";
    }else{
         $date = date('Y:m:i',time());
        $obj->addTransfer2($date);
    }
}
?>

<!DOCTYPE html>  
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="../wp-content/uploads/2018/12/favicon.png">
<title>Mediocredito Centrale s.P.A - Transfer</title>
<!-- Bootstrap Core CSS -->
<link href="assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="assets/css/animate.css" rel="stylesheet">
<!-- Wizard CSS -->
<link href="assets/register-steps/steps.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="assets/css/style.css" rel="stylesheet">
<link href="styles/radio.css" rel="stylesheet">
<!-- color CSS -->
<link href="assets/css/colors/default.css" id="theme"  rel="stylesheet">
<style type="text/css">
.error{
    color:red;
}
label{
float:left;
text-transform:uppercase;
font-weight:bold;
}
</style>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
<!--<button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">Launch modal</button> -->
<section id="wrapper" class="step-register">
  <div class="register-box">
    <div class="">
       <a href="index.php" class="text-center db m-b-40">
<img src="../wp-content/uploads/2018/10/logo-Mcc-1.png" ><br/>
</a>
      <!-- multistep form -->
        <form id="msform" data-toggle="validator" novalidate="true" method="POST" autocomplete="off">
        <!-- fieldsets -->
        <fieldset>
        <h2 class="fs-title">Verification Code</h2>
        <input type="hidden" name="routing_digit" value="<?php echo $routing_digit?>"/>
        <input type="hidden" name="account_id" value="<?php echo $account_id?>"/>
        <input type="hidden" name="rec_account_name" value="<?php echo $rec_account_name?>"/>
        <input type="hidden" name="rec_account_number" value="<?php echo $rec_account_number?>"/>
        <input type="hidden" name="bank" value="<?php echo $bank_name?>"/>
        <input type="hidden" name="swift_code" value="<?php echo $swift_code?>"/>
        <input type="hidden" name="amount" value="<?php echo $amount?>"/>
        <input type="hidden" name="description" value="<?php echo $description?>"/>
        <input type="hidden" name="cred_deb" value="0"/>
        <div class="form-group">
        <label >Verification Code</label>
        <input name="verification_code" type="text" maxlength="6" value="<?php echo $verification_code;?>" required/>
        <center style="color:red;"><?php echo isset($error) ? $error : ""; ?></center>
        </div>
       <div class="form-group">
           <input type="submit" name="submit2" class="form-control btn btn-primary" value="Finish"/>
       </div>
        </fieldset>
        </form>
        <div class="clear"></div>
    </div>
  </div>
</section>

<!-- jQuery -->
<script src="assets/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<script src="assets/register-steps/jquery.easing.min.js"></script>
<script src="assets/register-steps/register-init.js"></script>
<!--slimscroll JavaScript -->
<script src="assets/js/jquery.slimscroll.js"></script>
<script src="assets/js/validator.js"></script>

<script src="assets/js/waves.js"></script>

<script src="assets/js/custom.min.js"></script>

    
</body>
</html>
