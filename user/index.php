<?php 
  $title = "Analytics Overview";
require('important.php');
require('template/head.php');
require('features/stat.php');

$object = new Statistics();
$object2 = new AccountData($_COOKIE['mice_id']);
$accounts = $object2->getUserAccount();
$records = $object2->getTransactionHistory();


?>
<style>
th{
  color: black;
  font-weight:bold;
  font-size: 18px;
  text-transform: uppercase;
}
</style>
  <body class="h-100">
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
        <?php require('template/aside.php') ?>
        <!-- End Main Sidebar -->
        <main class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
          <?php require('template/navbar.php') ?>
          <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle" style="font-weight:bold;color:black;font-size:15px;"><?php echo $_COOKIE['nice_firstname']." ".$_COOKIE['nice_lastname']; ?></span>
              </div>
            </div>
            
<?php if(isset($_COOKIE['transfer_error'])):?>
            <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <center><?php echo $_COOKIE['transfer_error']; ?> </center></div>
<?php endif;?>
<?php if(isset($_COOKIE['transfer_success'])):?>
            <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <center><?php echo $_COOKIE['transfer_success']; ?></center></div>
<?php endif;?>
<?php if(isset($_COOKIE['signin_message'])):?>
            <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <center><?php echo $_COOKIE['signin_message']; ?></center></div>
<?php endif;?>
            <div class="row">
            
            <div class="col-lg col-md-4 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">Available Balance</span>
                        <h6 class="stats-small__value count my-3">$<?php echo number_format($object->getAvailableBalance(), 2, '.', ',');?></h6>
                      </div>
                      
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg col-md-6 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">Total Balance</span>
                        <h6 class="stats-small__value count my-3">$<?php echo number_format($object->getTotalBalance(),2,'.',',');?></h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- End Small Stats Blocks -->
            <br>
            <div class="row">
              <div class="col-lg-6 col-sm-12 mb-4">
            <div class="card" style="padding:10px;">
          <div class="table-responsive">
          <h4>Linked Accounts</h4>
            <table class="table table-hover">
              <tr>
                <th>Account Number</th>
                <th>Account Type</th>
                <th>Total Balance</th>
              </tr>
              <?php foreach($accounts as $account):?>
              <tr>
              <td><?php echo '*****'.substr($account['account_number'],strlen($account['account_number']) - 6,4); ?></td>
              <td><?php echo $account['account_type'];?></td>
              <td>$<?php echo number_format($account['balance'],2,'.',',');?></td>
              
              </tr>
<?php endforeach; ?>
            </table>
          </div>
          </div>
</div>
</div>
            <div class="row">
            <div class="col-lg-12 mb-4">
              <div class="card" style="padding:10px;">
                  <div class="table-responsive">
                    <h4>Transaction History</h4>
                    <table class="table table-hover">
                      <tr>
                        <th>Date</th>
                        <th>Description</th>
                        <th>Debit/Credit</th>
                        <th>Balance</th>
                        <th>Status</th>
                      </tr>
                      <?php foreach($records as $record):?>
                      <tr>
                        <td><?php $ddd = explode(" ",$record['dateOfTransaction']); echo $ddd[0]?></td>
                        <td><?php echo $record['description'];?></td>
                        <?php if($record['cred_deb'] == 0): ?>
                        <td style="color:red;"><?php echo '- $'.number_format($record['amount'],2,'.',',');?></td>
                        <?php elseif($record['cred_deb'] == 1): ?>
                        <td style="color:green;"><?php echo '+ $'.$record['amount'];?></td>
<?php endif;?>
                        <td><?php echo '$'.number_format($record['balance'],2,'.',',');?></td>
                        <?php if($record['status'] == 0): ?>
              <td style='color:green;'>Success</td>
              <?php elseif($record['status'] == 1): ?>
              <td style='color:red;'>Failed</td>
              <?php elseif($record['status'] == 2): ?>
              <td style='color:yellow;'>Pending</td>
<?php endif; ?>
                      </tr>
<?php endforeach;?>
                    </table>
                  </div>
              </div>
            </div>
</div>
<?php if(isset($_COOKIE['transfer_success'])):?>
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel"><?php echo strtoupper($_COOKIE['transfer_success']);?></h4> </div>
                                        
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
<?php endif;?>
            
          </div>
          <?php require('template/footer.php') ?>
        </main>
      </div>
    </div>
   
    <?php require('template/script.php') ?>
    </body>
    
</html>