<?php
require('important.php');
require('features/stat.php');

$obj = new AccountData($_COOKIE['mice_id']);
$accounts = $obj->getUserAccount();

$routing_digit = isset($_POST['routing_digit']) ? $_POST['routing_digit']: "";
$account_id = isset($_POST['account_id']) ? $_POST['account_id']: "";
$rec_account_name = isset($_POST['rec_account_name']) ? $_POST['rec_account_name']: "";
$rec_account_number = isset($_POST['rec_account_number']) ? $_POST['rec_account_number']: "";
$bank_name = isset($_POST['bank']) ? $_POST['bank']: "";
$swift_code = isset($_POST['swift_code']) ? $_POST['swift_code']: "";
$amount = isset($_POST['amount']) ? $_POST['amount']: "";
$description = isset($_POST['description']) ? $_POST['description']: "";
?>

<!DOCTYPE html>  
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="../wp-content/uploads/2018/12/favicon.png">
<title>Centennial Bank | Transfer</title>
<!-- Bootstrap Core CSS -->
<link href="assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="assets/css/animate.css" rel="stylesheet">
<!-- Wizard CSS -->
<link href="assets/register-steps/steps.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="assets/css/style.css" rel="stylesheet">
<link href="styles/radio.css" rel="stylesheet">
<!-- color CSS -->
<link href="assets/css/colors/default.css" id="theme"  rel="stylesheet">
<style type="text/css">
.error{
    color:red;
}
label{
float:left;
text-transform:uppercase;
font-weight:bold;
}
</style>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
<!--<button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">Launch modal</button> -->
<section id="wrapper" class="step-register">
  <div class="register-box">
    <div class="">
       <a href="index.php" class="text-center db m-b-40">
<img src="../images/logo/cover.png" style="max-width: 100%;" ><br/>
</a>
      <!-- multistep form -->
        <form id="msform" action="verification.php" data-toggle="validator" novalidate="true" method="POST" autocomplete="off">
        <fieldset>
        <h2 class="fs-title">Transaction Details</h2>
        <div class="form-group">
        <label >Account Number</label>
        <select name="account_id" class="form-control" required>
        <option value="">Select your Account</option>
        <?php foreach($accounts as $account):?>
            <option value ="<?php echo $account['id'];?>"><?php echo '*****'.substr($account['account_number'],strlen($account['account_number']) - 6,4)." (".$account['account_type'].") - €".$account['credit'];?></option>
        <?php endforeach;?>
        </select>
        </div>

        

        <div class="form-group">
        <label >Reciepient Bank</label>
        <input class="form-control" required name="bank" type="text" />
</div>

        <div class="form-group">
        <label >Recipient Account Name</label>
        <input type="text" name="rec_account_name" value="<?php echo $rec_account_name;?>" required/><span class="help-block with-errors"></span>
        </div>

        <div class="form-group">
        <label >Recipient Account Number</label>
        <input type="text" name="rec_account_number" data-minlength="8" value="<?php echo $rec_account_number;?>" required/><span class="help-block with-errors"></span>
        </div>

        <div class="form-group">
        <label >Amount</label>
        <input type="text" maxlength="10" step="1.00" pattern="^\d+(?:\.\d{1,2})?$" name="amount" value="<?php echo $amount;?>" required/>
        </div>

        <div class="form-group">
        <label>Routing Number</label>
        <input type="text"  data-minlength="7" name="routing_digit" value="<?php echo $routing_digit;?>" required/><span class="help-block with-errors"></span>
        </div>

        <div class="form-group">
        <label >Swift Code</label>
        <input name="swift_code" type="text" data-minlength="8" maxlength="11" value="<?php echo $swift_code;?>" required/>
        <span class="help-block with-errors"></span>
        </div>
        <div class="form-group">
        <label >Description</label>
        <textarea columns="5" name="description" required><?php echo $description; ?></textarea>
        <span class="help-block with-errors"></span>
        </div>
       <div class="form-group">
           <button type="submit" name="submit" class="form-control btn btn-primary btn-lg">Next</button>
        </fieldset>
        </form>
        <div class="clear"></div>
    </div>
  </div>
</section>

<!-- jQuery -->
<script src="assets/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<script src="assets/register-steps/jquery.easing.min.js"></script>
<script src="assets/register-steps/register-init.js"></script>
<!--slimscroll JavaScript -->
<script src="assets/js/jquery.slimscroll.js"></script>
<script src="assets/js/validator.js"></script>

<script src="assets/js/waves.js"></script>

<script src="assets/js/custom.min.js"></script>

    
</body>
</html>
