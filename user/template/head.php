<!doctype html>
<html class="no-js h-100" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Centennial Bank |  <?php echo $title; ?></title>

    <link rel="shortcut icon" href="../wp-content/uploads/2018/12/favicon.png" />
    <meta name="description" content="A high-quality &amp; free Bootstrap admin dashboard template pack that comes with lots of templates and components.">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="styles/font-awesome.css" rel="stylesheet">
    <link href="styles/materials.css" rel="stylesheet">
    <link rel="stylesheet" href="styles/bootstrap-4.0.0/dist/css/bootstrap.min.css">
    <link rel="stylesheet" id="main-stylesheet" data-version="1.0.0" href="styles/shards-dashboards.1.0.0.min.css">
    <link rel="stylesheet" href="styles/extras.1.0.0.min.css">
    <script async defer src="scripts/buttons.min.js"></script>
    <link rel="stylesheet" href="dropify/dist/css/dropify.min.css">
    <style type="text/css">
    .help-block{
        color:red;
        text-align:center;
    }
    label{
    color:red;
    font-weight: bolder;
}
 .nav-second-level li,
 .nav-third-level li {
    border-bottom: none !important;
}

 .nav-second-level li a {
    padding: 14px 10px 14px 40px;
}
 .nav-third-level li a {
    padding-left: 60px;
}
    </style>
  </head>