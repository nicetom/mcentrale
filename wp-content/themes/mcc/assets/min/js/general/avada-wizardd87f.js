/* */
var aValue = ["0", "400000","800000","1200000","1600000","2000000","2400000","2800000","3200000","3600000","4000000","4400000","4800000","5200000","5600000","6000000"];
var aLabel = ["0","25.000","50.000","75.000","100.000","150.000","200.000","300.000","400.000","500.000","1.000.000","2.000.000","3.000.000","4.000.000","5.000.000","Oltre 5 milioni"];
var aRealValue = ["0","25000","50000","75000","100000","150000","200000","300000","400000","500000","1000000","2000000","3000000","4000000","5000000","6000000"];

jQuery(document).ready(function ($) {
   var ultimoStep       = localStorage.getItem('ultimoStep');//recupera l'ultimo step del wizard
   var wizardConcluso   =  localStorage.getItem('wizardConcluso');//verifichiamo se il wizard è concluso valore == 1
   if (ultimoStep == '' || ultimoStep == undefined || ultimoStep == null) {
           ajaxCallResumeTemplate('0');
   } else if (ultimoStep == '7') {
           //se 7 ricarica i risultati già ottenuti
           var passaggi = localStorage.getItem('passaggi');
           var passaggiParse = JSON.parse(passaggi);
           risultatoWizard(passaggiParse['7'], ultimoStep);
   } else {
           ajaxCallResumeTemplate(ultimoStep);
   }

   //permette di vedere gli step nella parte alta della pagina (i blocchi con matita)
    arrayStep();
   //hover sulla cartina
    jQuery(document).on('hover','path',function() {
        var path = jQuery(this);
        path.css('fill','#1dac80');
    });
    jQuery(document).on('mouseleave','path',function() {
        var path = jQuery(this);
        path.css('fill','#d3d3d3');
    });

    jQuery(document).on('click','path',function() {
        var path        = jQuery(this);
        var id          = path.attr('id');
        var nomeRegione = path.attr('data-regione');
        $('path').removeClass('selected');
        $('#' + id).addClass('selected');
        localStorage.setItem('regioneSelezionata', id);
        localStorage.setItem('nomeRegioneSelezionata', nomeRegione);
    });

    // click sui blocchi del primo step
    jQuery(document).on('click','.wizard-step',function() {
        var div = $(this);
        $('.wizard-step').removeClass('mcc-background-green');
        $('.mcc-wizard-step-hover').removeAttr("style");
        $('.mcc-wizard-step-visible').removeAttr("style");
        div.find('.mcc-wizard-step-visible').css('display', 'none');
        div.find('.mcc-wizard-step-hover').css('display', 'block');
        div.addClass('mcc-background-green');
    });


    //click sulla matita
    jQuery('.ajax-popup-link').magnificPopup({
        type: 'ajax',
        closeOnBgClick: false,
        callbacks: {
            parseAjax: function(mfpResponse) {
                // mfpResponse.data is a "data" object from ajax "success" callback
                // for simple HTML file, it will be just String
                // You may modify it to change contents of the popup
                // For example, to show just #some-element:
                // mfpResponse.data = $(mfpResponse.data).find('#some-element');
                //jQuery(".step-successivo").remove();
                // mfpResponse.data must be a String or a DOM (jQuery) element

                //console.log('Ajax content loaded:', mfpResponse);
            },
            ajaxContentAdded: function() {
                var magnificPopup   = $.magnificPopup.instance, cur = magnificPopup.st.el;
                var attributoName   = jQuery('.mfp-wrap form').find(':input').attr('name');//il form nella finestra modale
                var el              = cur.attr('data-localStorage');//si trova nella pagina wizard
                var step            = cur.attr('data-stepModifica');//si trova nella pagina wizard
                var valueStorage    = localStorage.getItem(el);
                modificaSelectWizard(step, valueStorage);
                jQuery('.mfp-wrap form').field(attributoName, valueStorage);//popola il form al modifica
                jQuery(".mfp-wrap .flusso").remove();
                jQuery(".mfp-wrap .mcc-step-modify-button").show();
            }
        }
    });
});

/*
chiamata ajax per andare avanti di uno step
 */
var ajaxCall = function (id, value, testo, testoLabelAppend) {
    mostraLoader();
    var lista = '';
    jQuery.ajax({
        method: "POST",
        url: localStorage.getItem('urlClass'),
        data: {id: id},
        async:true,
        cache: false,
        success: function (obj) {
            removeLoader();
            if(testo == '' || testo == undefined || testo == null) {
                jQuery('#step-'+id).css('display','flex').find('span').html(value +' '+testoLabelAppend);
                jQuery('#step-'+id).find('title').html(value);
            } else {
                jQuery('#step-'+id).css('display','flex').find('span').html(testo +' '+testoLabelAppend);
                jQuery('#step-'+id).find('title').html(testo);
            }
            passaggiWizard(id, value, testo, testoLabelAppend);

            jQuery("#wizard").empty();
            var result = JSON.parse(obj);
            if (result.hasOwnProperty('error')) {
                alert(result.error['message']);
                location.reload();
            } else {
                for (var idx in result) {
                    var template = result[idx].template;
                    if (result[idx].hasOwnProperty('servizio')) {
                        var select = result[idx].servizio;
                        for (i = 0; i < select.length; i++) {
                            lista += '<option value="'+ select[i].indice +'">' + select[i].descrizione + '</option>';
                        }
                        jQuery("#wizard").load(template, function () {
                            jQuery('#mcc-settore').append(lista);
                            popolaSettore();
                        })
                    } else {
                        jQuery("#wizard").load(template);
                    }
                }
                //memorizzo lo step corrente
                localStorage.setItem('ultimoStep',id);
                scrollTop();
            }
        },
        error: function (obj) {
            errorCallBack();
        }
    });
}

var stepSuccessivo = function (value, id, testo, testoLabelAppend) {
    /*
    per  anni(id=2) e volume d'affari(id=3) non bisogna validare lo 0
    perché il valore può essere anche 0
     */
    var div_error = jQuery('#div_error');
    if(id == '2' || id == '3') {
        if (value == undefined || value == '') {
            var errorOfset = div_error.css('display', 'table').offset().top;
            div_error.html('selezionare una voce');
            jQuery("html, body").animate({scrollTop: errorOfset}, "slow");
        } else {
            div_error.empty().hide();
            ajaxCall(id, value, testo, testoLabelAppend);
        }
    } else {
        if (value == undefined || value == '' || value == '0') {
            var errorOfset = div_error.css('display', 'table').offset().top;
            div_error.html('selezionare una voce');
            jQuery("html, body").animate({scrollTop: errorOfset}, "slow");
        } else {
            div_error.empty().hide();
            ajaxCall(id, value, testo, testoLabelAppend);
        }
    }
}


/*
andiamo a inserire tutti i passaggi nello storage
 */
var passaggiWizard = function (id, value, testo, testoLabelAppend) {
    // Get the existing data
    var existing = localStorage.getItem('passaggi');
    existing = existing ? JSON.parse(existing) : {};
    if(testo == undefined || testo == '') {
        existing[id] = value+' '+testoLabelAppend;
    // Add new data to localStorage Array

    } else {
        existing[id] = testo+' '+testoLabelAppend;
    }
    // Save back to localStorage
    localStorage.setItem('passaggi', JSON.stringify(existing));
}


/*
al caricamento della pagina carichiamo il template corrispondente allo step (id)
 */
var ajaxCallResumeTemplate = function (id) {
    var lista = '';
    jQuery.ajax({
        method: "POST",
        url: localStorage.getItem('urlClass'),
        async:false,
        cache: false,
        data: {id: id},
        success: function (obj) {
            jQuery("#wizard").empty();
            var result = JSON.parse(obj);
            if (result.hasOwnProperty('error')) {
                alert(result.error['message']);
                location.reload();
            } else {
                for (var idx in result) {
                    var template = result[idx].template;
                    if (result[idx].hasOwnProperty('servizio')) {
                        var select = result[idx].servizio;
                        for (i = 0; i < select.length; i++) {
                            lista += '<option value="'+ select[i].indice +'">' + select[i].descrizione + '</option>';
                        }
                        //carica il template
                        jQuery("#wizard").load(template, function () {
                            jQuery('#mcc-settore').append(lista);
                            popolaSettore();
                        })
                    } else {
                        jQuery("#wizard").load(template);
                    }
                }
            }
        },
        error: function (obj) {
            errorCallBack();
        }
    });
}

//crea le select grafiche
function popolaSettore() {
    var x, i, j, selElmnt, a, b, c;
    /*look for any elements with the class "custom-select":*/
    x = document.getElementsByClassName('mcc-custom-select');
    for (i = 0; i < x.length; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        /*for each element, create a new DIV that will act as the selected item:*/
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected");
        a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        x[i].appendChild(a);
        /*for each element, create a new DIV that will contain the option list:*/
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        for (j = 1; j < selElmnt.length; j++) {
            /*for each option in the original select element,
            create a new DIV that will act as an option item:*/
            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.addEventListener("click", function (e) {
                /*when an item is clicked, update the original select box,
                and the selected item:*/
                var y, i, k, s, h;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                h = this.parentNode.previousSibling;
                for (i = 0; i < s.length; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = this.innerHTML;
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        for (k = 0; k < y.length; k++) {
                            y[k].removeAttribute("class");
                        }
                        this.setAttribute("class", "same-as-selected");
                        break;
                    }
                }
                h.click();
            });
            b.appendChild(c);
        }
        x[i].appendChild(b);
        a.addEventListener("click", function (e) {
            /*when the select box is clicked, close any other select boxes,
            and open/close the current select box:*/
            e.stopPropagation();
            closeAllSelect(this);
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
        });
    }
}


//crea le select grafiche per le finestre modali
function popolaSettoreFinestreModali() {
    var x, i, j, selElmnt, a, b, c;
    /*look for any elements with the class "custom-select":*/
    x = jQuery('.mfp-wrap form .mcc-custom-select');
    for (i = 0; i < x.length; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        /*for each element, create a new DIV that will act as the selected item:*/
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected");
        a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        x[i].appendChild(a);
        /*for each element, create a new DIV that will contain the option list:*/
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        for (j = 1; j < selElmnt.length; j++) {
            /*for each option in the original select element,
            create a new DIV that will act as an option item:*/
            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.addEventListener("click", function (e) {
                /*when an item is clicked, update the original select box,
                and the selected item:*/
                var y, i, k, s, h;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                h = this.parentNode.previousSibling;
                for (i = 0; i < s.length; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = this.innerHTML;
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        for (k = 0; k < y.length; k++) {
                            y[k].removeAttribute("class");
                        }
                        this.setAttribute("class", "same-as-selected");
                        break;
                    }
                }
                h.click();
            });
            b.appendChild(c);
        }
        x[i].appendChild(b);
        a.addEventListener("click", function (e) {
            /*when the select box is clicked, close any other select boxes,
            and open/close the current select box:*/
            e.stopPropagation();
            closeAllSelect(this);
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
        });
    }
}

function closeAllSelect(elmnt) {
    /*a function that will close all select boxes in the document,
    except the current select box:*/
    var x, y, i, arrNo = [];
    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("select-selected");
    for (i = 0; i < y.length; i++) {
        if (elmnt == y[i]) {
            arrNo.push(i)
        } else {
            y[i].classList.remove("select-arrow-active");
        }
    }
    for (i = 0; i < x.length; i++) {
        if (arrNo.indexOf(i)) {
            x[i].classList.add("select-hide");
        }
    }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);


var risultatoWizard = function (value, id) {
    var div_error  = jQuery('#div_error');
    var testoLabelAppend = '€ Richiesti';
    var testo = '';
    if(value == undefined || value == '' || value == '0') {
        var errorOfset =  div_error.css('display','table').offset().top;
        div_error.html('selezionare un valore');
        jQuery("html, body").animate({ scrollTop: errorOfset }, "slow");
    } else {
        var datiSalvati = {
            bisogno :localStorage.getItem('bisogno'),
            anniAzienda:localStorage.getItem('anniAzienda'),
            volumeAffari: localStorage.getItem('volumeAffari'),
            settore: localStorage.getItem('settore'),
            regione:localStorage.getItem('regioneSelezionata'),
            finalita: localStorage.getItem('finalita'),
            fabbisogno: localStorage.getItem('fabbisogno')
        }

        div_error.empty().hide();
        mostraLoader();
        jQuery.ajax({
            method: "POST",
            async:true,
            cache: false,
            url: urlWizardResult,
            data: {dati:JSON.stringify(datiSalvati)},
            success: function (obj) {
                if (obj.hasOwnProperty('error')) {
                    alert(result.error['message']);
                    location.reload();
                } else {
                    jQuery('#step-' + id).css('display', 'flex').find('span').html(value + ' ' + testoLabelAppend);
                    passaggiWizard(id, value, testo, testoLabelAppend);
                    jQuery("#wizard").empty();
                    jQuery.post(url_ajax, {action: "mostra_risultati_wizard", 'codici': obj}, function (data) {
                        jQuery('#wizard').html(data);
                        messaggioFormWizard();
                        contaPost();
                        removeLoader();
                    });
                    localStorage.setItem('ultimoStep', id);
                    localStorage.setItem('wizardConcluso', 1);
                }
            },
            error: function (obj) {
                errorCallBack();
            }
        });
    }
}

/*
popoliamo in alto al wizard i vari step(matite)
 */
var arrayStep = function () {
    if(localStorage.getItem('passaggi') !== null) {
        var passaggi                = localStorage.getItem('passaggi');
        var parsePassaggi           = JSON.parse(passaggi);
        jQuery.each( parsePassaggi, function( key, value ) {
            jQuery('#step-'+ key).css('display', 'flex').find('span').html(value);
            jQuery('#step-'+ key).find('a').attr('title',value);
        });
    }
}

/*
update passaggi
 */
var updatePassaggi = function (value, id, testoLabelAppend) {
    /*
    per  anni(id=2) e volume d'affari(id=3) non bisogna validare lo 0
    perché il valore può essere anche 0
     */
    var div_error  = jQuery('#div_error');
    if(id == '2' || id == '3') {
        if (value == undefined || value == '') {
            var errorOfset = div_error.css('display', 'table').offset().top;
            div_error.html('selezionare una voce');
            jQuery("html, body").animate({scrollTop: errorOfset}, "slow");
        } else {
            var passaggi = localStorage.getItem('passaggi');
            var parsePassaggi = JSON.parse(passaggi);
            parsePassaggi[id] = value + ' ' + testoLabelAppend;
            localStorage.setItem('passaggi', JSON.stringify(parsePassaggi));
            ricaricaRisultatiAlModifica();
            setTimeout(function () {
                //eseguo la funzione dopo 500 perché va in conflitto con le altre funzioni al click su conferma nella finestra modale
                chiudi();
            }, 500);
        }
    } else {
        if (value == undefined || value == '' || value == '0') {
            var errorOfset = div_error.css('display', 'table').offset().top;
            div_error.html('selezionare una voce');
            jQuery("html, body").animate({scrollTop: errorOfset}, "slow");
        } else {
            var passaggi = localStorage.getItem('passaggi');
            var parsePassaggi = JSON.parse(passaggi);
            parsePassaggi[id] = value + ' ' + testoLabelAppend;
            localStorage.setItem('passaggi', JSON.stringify(parsePassaggi));
            ricaricaRisultatiAlModifica();
            setTimeout(function () {
                //eseguo la funzione dopo 500 perché va in conflitto con le altre funzioni al click su conferma nella finestra modale
                chiudi();
            }, 500);
        }
    }
}


/*
chiude finestra modale modifica passaggio
 */

var chiudi = function () {
    jQuery('.mfp-close').trigger('click');
}


//field function to get/set input values of any type of input
jQuery(function ($) {
    $.fn.field = function (inputName, value)
    {
        console.log('field called...');
        console.log($(this));

        console.log(typeof inputName);

        if (typeof inputName !== "string") return false;
        var $inputElement = $(this).find("[name=" + inputName + "]");
        // var $inputElement = $(this); //direct mapping with no form context

        console.log($inputElement);

        if (typeof value === "undefined" && $inputElement.length >= 1)
        {
            switch ($inputElement.attr("type"))
            {
                case "checkbox":
                    return $inputElement.is(":checked");
                    break;
                case "radio":
                    var result;
                    $inputElement.each(function (i, val) {
                        if ($(this).is(":checked")) result = $(this).val()
                    });
                    return result;
                    break;
                default:
                    return $inputElement.val();
                    break;
            }
        }
        else
        {
            switch ($inputElement.attr("type"))
            {
                case "checkbox":
                    $inputElement.attr({
                        checked: value
                    });
                    break;
                case "radio":
                    $inputElement.each(function (i) {
                        if ($(this).val() == value) $(this).attr({
                            checked: true
                        })
                    });
                    if($inputElement.selector == '.mfp-wrap form [name=primoStep]') {
                        jQuery('.mfp-wrap form [name=primoStep]:checked').parent().parent().addClass('mcc-background-green');
                        jQuery('.mfp-wrap form [name=primoStep]:checked').next().css('display', 'none');
                        jQuery('.mfp-wrap form [name=primoStep]:checked').next().next().css('display', 'block');
                       // jQuery('.page-template-wizard .mfp-container .mcc-wizard-first-step .mcc-wizard-grid form>div img').css('display', 'none');
                    }

                    break;
                case "range":
                	/* Procedo con la decodifica dell'importa con la corretta label - bella porcata */
        			for(var i = 0; i < aValue.length; i++){
        				if(value == aRealValue[i]){

        					if(value >= 2800000) {
        		                $('.rangeslider__fill').css('background', '#b4b4b4');
        		                $('.mcc-rangeslider-message').hide();
        		            } else {
        		                $('.rangeslider__fill').css('background', '#1bac80');
        		                $('.mcc-rangeslider-message').show();
        		            }
        					
        					$inputElement.prev().find('span').html(aLabel[i] +' \u20AC');
        					$inputElement.val(aValue[i]);
                            jQuery('input[type="range"]').rangeslider('update', true);
        				}
        			}
                	
                    /*
                    $inputElement.prev().find('span').html(value);
                    $inputElement.val(value);
                    jQuery('input[type="range"]').rangeslider('update', true);
                    */
                    break;
                case undefined:
                   // $(this).append('');
                    if($inputElement.selector == '.mfp-wrap form [name=mcc-settore]') {
                        jQuery("#mcc-settore option[value='" + value + "']").attr('selected', true);
                    }
                    break;
                default:
                    $inputElement.val(value);


                    break;
            }
            return $inputElement;
        }
    }
});

/*
quando apriamo la modale abbiamo selezionato il valore salvato precedentemente
 */
var modificaSelectWizard = function (id, value) {
    var lista = '';
    jQuery.ajax({
        method: "POST",
        url: localStorage.getItem('urlClass'),
        data: {id: id},
        async:true,
        cache: false,
        success: function (obj) {
            var result = JSON.parse(obj);
            if (result.hasOwnProperty('error')) {
                alert(result.error['message']);
                location.reload();
            } else {
                for (var idx in result) {
                    if (result[idx].hasOwnProperty('servizio')) {
                        var select = result[idx].servizio;
                        for (i = 0; i < select.length; i++) {
                            lista += '<option value="'+ select[i].indice +'">' + select[i].descrizione + '</option>';
                        }
                            jQuery('.mfp-wrap form #mcc-settore').append(lista);
                            jQuery(".mfp-wrap form #mcc-settore option[value='" + value + "']").attr('selected', true);
                            popolaSettoreFinestreModali();
                    }
                }
            }
        },
        error: function (obj) {
            errorCallBack();
        }
    });
}

/*
messaggio finale nel form d'invio messaggio
 */
var messaggioFormWizard = function () {
    var dati            = localStorage.getItem('passaggi');
    var datiParse       = JSON.parse(dati);
    var fatturato       = datiParse['3'].replace(new RegExp('€ Fatturato', 'g'),'');//elimino il testo
    if(datiParse['7'].indexOf('Oltre') > -1) {
        var finanziamento   = datiParse['7'].replace(new RegExp('€ Richiesti', 'g'), '');//elimino il testo
    } else {
        var finanziamento   = datiParse['7'].replace(new RegExp('€ Richiesti', 'g'), '');//elimino il testo
        var finanziamento   = finanziamento.replace(/ /g, '');//elimino gli spazi
    }

    var testo = 'Spettabile Mediocredito Centrale,\n' +
        '\n' +
        'sarei interessato a dei vostri prodotti finanziari mirati alla crescita della mia azienda con '+ datiParse['2'] +' in '+ datiParse['4'] +' nella regione ' + datiParse['5']+'.\n' +
        'Il mio fatturato è di '+ fatturato +' € annui e vorrei richiedere un finanziamento di ' + finanziamento + ' € per ' + datiParse['6'] + '.\n' +
        '\n' +
        'Cordiali saluti'
    jQuery('#campo-messaggio').val(testo);
}

/*
scriviamo in alto quanti prodotti sono stati trovati nel db
e mostriamo il form solo se ci sono risultati
 */
var contaPost = function () {
    var value           = jQuery('input[name=contaPost]').val();
    var convertNumber   = Number(value);
    var formContatti    = jQuery('#mcc-form-contatti-wizard');
    if(convertNumber > 1) {
        var testo = 'Abbiamo ' + value + ' risultati per te';
        formContatti.css('display','block');
    } else if(isNaN(convertNumber)) {
        var testo = 'Non abbiamo risultati per te';
        formContatti.css('display','none');
    } else {
        var testo = 'Abbiamo ' + value + ' risultato per te';
        formContatti.css('display','block');
    }
    jQuery('.rimuovi-intestazioni').hide();
    jQuery('#numero-risultati').html(testo);
}

/*
se il wizard è concluso al refresh della pagina avviamo la funzione che ricarica i risultati
 */
var ricaricaRisultatiAlModifica = function () {
    var step            = localStorage.getItem('wizardConcluso');
    var passaggi        = localStorage.getItem('passaggi');
    var passaggiParse   = JSON.parse(passaggi);
   if(step == '1') {
       risultatoWizard(passaggiParse['7'], localStorage.getItem('ultimoStep') );
   }
}

/*
converte il valore di un input da stringa a numero
 */
var stringToNumber = function (value) {
    var fromStringToNumber = Number(value);//trasformo il valore da stringa a numero
    var risultato =  fromStringToNumber.toLocaleString();
    return risultato;
}


var scrollTop = function () {
    var div = jQuery('#scrollTop').offset().top;
    jQuery("html, body").animate({ scrollTop: div }, "slow");

}

var errorCallBack = function () {
    alert('errore del servizio. Aggiorna la pagina');
}




