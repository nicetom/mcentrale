jQuery(window).on('load', function() {
    setTimeout("jQuery('.fraud-panel').toggle( 'slide' )", 10000);
});
jQuery(document).ready(function ($) {

    // nasconde il menu solo quando si clicca su lavora con noi nel menu top
    jQuery('.lavora-con-noi-hidden-menu').click(function () {
        localStorage.setItem('nascondiMenuLavoraConNoi', 'attivo');
    });
    //var schermo = jQuery(document).width();
    //alert(schermo);
    pulisciStorage();
    $('.fadeSearch').click(function () {
        $('#cerca-fullwidth').fadeIn(200);
    });

    $('#mcc-close-search').click(function () {
        $('#cerca-fullwidth').fadeOut(200);
    });

    // aggiunge e rimuove al  click la class mcc-selected nelle paginazioni
    $('.data-selezionata').click(function () {
        $('.data-selezionata').removeClass('mcc-selected');
        $(this).addClass('mcc-selected');
    });

    //chiamata ajax per finestre modali generiche
    jQuery('.ajax-modale').magnificPopup({
        type: 'ajax',
        closeOnBgClick: false,
        callbacks: {
            parseAjax: function (mfpResponse) {
                mfpResponse.data = $(mfpResponse.data).find('#visualizza-modale');
                // mfpResponse.data is a "data" object from ajax "success" callback
                // for simple HTML file, it will be just String
                // You may modify it to change contents of the popup
                // For example, to show just #some-element:
                // mfpResponse.data = $(mfpResponse.data).find('#some-element');
                // mfpResponse.data must be a String or a DOM (jQuery) element

                // console.log('Ajax content loaded:', mfpResponse);
            },
            ajaxContentAdded: function () {
                // console.log(this.content);
            }
        }


    });
    //fine
    //
  
    jQuery(".fraud-button").click(function () {
        $( ".fraud-panel" ).toggle( "slide", function() {
            setTimeout("jQuery('.fraud-panel').toggle( 'slide' )", 10000);
        } );
            // jQuery(".fraud-panel").animate({
            //     width: "toggle"
            // },function() {
            //     setTimeout("jQuery('.fraud-panel').animate({width: 'toggle'})", 10000);
            // });
    });
    //setTimeout("jQuery('.fraud-panel').animate({width: 'toggle'})", 10000);
   // setTimeout("jQuery('.fraud-panel').toggle( 'slide' )", 10000);

    //carosello anni
    $('.carousel-anni').owlCarousel({
        //afterInit : attachEvent(),
        loop: false,
        rewind: true,
        margin: 10,
        nav: false,
        slideBy: 2,
        dots: false,
        responsiveClass: true,
        autoHeight: false,
        autoplay: false,
        responsive: {
            0: {
                items: 1,
                nav: false,
                margin: 0,
                slideBy: 1
            },
            768: {
                items: 3,
                nav: false,
                margin: 0
            },
            1000: {
                items: 3,
                nav: false,
                loop: true,
                margin: 0
            }
        },
    });
    $('.mcc-customNextBtn').click(function () {
        $('.carousel-anni').trigger('next.owl.carousel', 500);
    });
    $('.mcc-customPreviousBtn').click(function () {
        $('.carousel-anni').trigger('prev.owl.carousel', 500);
    });


});

var mostraLoader = function () {
    jQuery('body').append('<div id="mcc-overlay-main"><div id="loaderMcc" class="tp-loader tp-demo spinner3">\n' +
        '<div class="dot1" style="background-color: rgb(29, 172, 128);"></div>\n' +
        '<div class="dot2" style="background-color: rgb(29, 172, 128);"></div>\n' +
        '<div class="bounce1" style="background-color: rgb(29, 172, 128);"></div>\n' +
        '<div class="bounce2" style="background-color: rgb(29, 172, 128);"></div>\n' +
        '<div class="bounce3" style="background-color: rgb(29, 172, 128);"></div>\n' +
        '</div></div>\n');
}

var removeLoader = function () {
    jQuery('#mcc-overlay-main').remove();
}

//dopo aver inviato il messaggio del wizard scompare il form e poi appare il messaggio di ringraziamento
document.addEventListener('wpcf7mailsent', function (event) {
    jQuery('#form-invio-wizard form').slideUp('slow', function () {
        jQuery('#mcc-wizard-send-ok').css('display', 'flex');
    });
},
    false
);

/*
al click su un link che sia nel footer o nel menu il localStorage viene resettato
 */
var pulisciStorage = function () {
    jQuery('.rev_slider .tp-caption,.fusion-footer-widget-area ul li a,.fusion-main-menu > ul > li > a,.fusion-logo a, .fusion-secondary-menu > ul > li > a, .widget .fusion-social-networks .fusion-social-network-icon').click(function ($) {
        localStorage.clear();
    });
}
