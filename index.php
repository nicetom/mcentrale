<!DOCTYPE html>
<html>

<head>
	<!-- End Google Tag Manager -->

	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<title>Centennial Bank  | The Bank for business</title>
	<link rel="shortcut icon" href="wp-content/uploads/2018/12/favicon.png" type="image/x-icon" />

	<script type="text/javascript">
		window._wpemojiSettings = {
			"baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/",
			"ext": ".png",
			"svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/",
			"svgExt": ".svg",
			"source": {
				"concatemoji": "https:\/\/www.mcc.it\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.9"
			}
		};
		! function(a, b, c) {
			function d(a, b) {
				var c = String.fromCharCode;
				l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, a), 0, 0);
				var d = k.toDataURL();
				l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, b), 0, 0);
				var e = k.toDataURL();
				return d === e
			}

			function e(a) {
				var b;
				if (!l || !l.fillText) return !1;
				switch (l.textBaseline = "top", l.font = "600 32px Arial", a) {
					case "flag":
						return !(b = d([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819])) && (b = d([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]), !b);
					case "emoji":
						return b = d([55358, 56760, 9792, 65039], [55358, 56760, 8203, 9792, 65039]), !b
				}
				return !1
			}

			function f(a) {
				var c = b.createElement("script");
				c.src = a, c.defer = c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)
			}
			var g, h, i, j, k = b.createElement("canvas"),
				l = k.getContext && k.getContext("2d");
			for (j = Array("flag", "emoji"), c.supports = {
					everything: !0,
					everythingExceptFlag: !0
				}, i = 0; i < j.length; i++) c.supports[j[i]] = e(j[i]), c.supports.everything = c.supports.everything && c.supports[j[i]], "flag" !== j[i] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[j[i]]);
			c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function() {
				c.DOMReady = !0
			}, c.supports.everything || (h = function() {
				c.readyCallback()
			}, b.addEventListener ? (b.addEventListener("DOMContentLoaded", h, !1), a.addEventListener("load", h, !1)) : (a.attachEvent("onload", h), b.attachEvent("onreadystatechange", function() {
				"complete" === b.readyState && c.readyCallback()
			})), g = c.source || {}, g.concatemoji ? f(g.concatemoji) : g.wpemoji && g.twemoji && (f(g.twemoji), f(g.wpemoji)))
		}(window, document, window._wpemojiSettings);
	</script>
	<style type="text/css">
		img.wp-smiley,
		img.emoji {
			display: inline !important;
			border: none !important;
			box-shadow: none !important;
			height: 1em !important;
			width: 1em !important;
			margin: 0 .07em !important;
			vertical-align: -0.1em !important;
			background: none !important;
			padding: 0 !important;
		}
	</style>
	<link rel='stylesheet' id='contact-form-7-css' href='wp-content/plugins/contact-form-7/includes/css/styles3c21.css?ver=5.1.1' type='text/css' media='all' />

	<link rel='stylesheet' id='avada-stylesheet-css' href='wp-content/themes/mcc/assets/css/style.min9f31.css?ver=5.7.2' type='text/css' media='all' />
	<!--[if lte IE 9]>
<link rel='stylesheet' id='avada-IE-fontawesome-css'  href='https://www.mcc.it/wp-content/themes/mcc/includes/lib/assets/fonts/fontawesome/font-awesome.min.css?ver=5.7.2' type='text/css' media='all' />
<![endif]-->
	<!--[if IE]>
<link rel='stylesheet' id='avada-IE-css'  href='https://www.mcc.it/wp-content/themes/mcc/assets/css/ie.min.css?ver=5.7.2' type='text/css' media='all' />
<![endif]-->
	<link rel='stylesheet' id='wpdreams-asp-basic-css' href='wp-content/plugins/ajax-search-pro/css/style.basice154.css?ver=A9LIR0' type='text/css' media='all' />
	<link rel='stylesheet' id='wpdreams-asp-chosen-css' href='wp-content/plugins/ajax-search-pro/css/chosen/chosene154.css?ver=A9LIR0' type='text/css' media='all' />
	<link rel='stylesheet' id='wpdreams-ajaxsearchpro-instances-css' href='wp-content/uploads/asp_upload/style.instancese154.css?ver=A9LIR0' type='text/css' media='all' />
	<link rel='stylesheet' id='fusion-dynamic-css-css' href='wp-content/uploads/fusion-styles/dda8281b94a9b34af31c3bc1ce1e4d96.mind87f.css?ver=4.9.9' type='text/css' media='all' />
	<link rel='stylesheet' id='mpc-massive-style-css' href='wp-content/plugins/mpc-massive/assets/css/mpc-styles9ab0.css?ver=2.4.3.2' type='text/css' media='all' />
	<link rel='stylesheet' id='style-css' href='wp-content/themes/mcc/assets/css/mcc-styled87f.css?ver=4.9.9' type='text/css' media='all' />
	<link rel='stylesheet' id='style-magnific-css' href='wp-content/plugins/mpc-massive/magnific-popupd87f.css?ver=4.9.9' type='text/css' media='all' />
	<link rel='stylesheet' id='carosello-css' href='wp-content/themes/mcc/assets/css/owl.carouseld87f.css?ver=4.9.9' type='text/css' media='all' />
	<link rel='stylesheet' id='carosello2-css' href='wp-content/themes/mcc/assets/css/owl.theme.default.mind87f.css?ver=4.9.9' type='text/css' media='all' />
	<script type='text/javascript' src='wp-includes/js/jquery/jqueryb8ff.js?ver=1.12.4'></script>
	<script type='text/javascript' src='wp-includes/js/jquery/jquery-migrate.min330a.js?ver=1.4.1'></script>
	<script type='text/javascript' src='wp-content/plugins/mpc-massive/magnific-popup.mind87f.js?ver=4.9.9'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/owl.carouseld87f.js?ver=4.9.9'></script>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300|Open+Sans:400|Open+Sans:700' rel='stylesheet' type='text/css'>
	<style type="text/css">
		.recentcomments a {
			display: inline !important;
			padding: 0 !important;
			margin: 0 !important;
		}
	</style>

	<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://www.mcc.it/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]-->
	<meta name="generator" content="Powered by Slider Revolution 5.4.8 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
	<style type="text/css">
		<!--
		@font-face {
			font-family: 'asppsicons2';
			src: url('wp-content/plugins/ajax-search-pro/css/fonts/icons/icons2.eot');
			src: url('wp-content/plugins/ajax-search-pro/css/fonts/icons/icons2d41d.eot?#iefix') format('embedded-opentype'),
				url('wp-content/plugins/ajax-search-pro/css/fonts/icons/icons2.html') format('woff2'),
				url('wp-content/plugins/ajax-search-pro/css/fonts/icons/icons2.woff') format('woff'),
				url('wp-content/plugins/ajax-search-pro/css/fonts/icons/icons2.ttf') format('truetype'),
				url('wp-content/plugins/ajax-search-pro/css/fonts/icons/icons2.svg#icons') format('svg');
			font-weight: normal;
			font-style: normal;
		}

		.asp_m {
			height: 0;
		}
		-->
	</style>
	<script type="text/javascript">
		if (typeof _ASP !== "undefined" && _ASP !== null && typeof _ASP.initialize !== "undefined")
			_ASP.initialize();
	</script>
	<script type="text/javascript">
		function setREVStartSize(e) {
			try {
				e.c = jQuery(e.c);
				var i = jQuery(window).width(),
					t = 9999,
					r = 0,
					n = 0,
					l = 0,
					f = 0,
					s = 0,
					h = 0;
				if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function(e, f) {
						f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
					}), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
					var u = (e.c.width(), jQuery(window).height());
					if (void 0 != e.fullScreenOffsetContainer) {
						var c = e.fullScreenOffsetContainer.split(",");
						if (c) jQuery.each(c, function(e, i) {
							u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
						}), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
					}
					f = u
				} else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
				e.c.closest(".rev_slider_wrapper").css({
					height: f
				})
			} catch (d) {
				console.log("Failure at Presize of Slider:" + d)
			}
		};
	</script>
	<noscript>
		<style type="text/css">
			.wpb_animate_when_almost_visible {
				opacity: 1;
			}
		</style>
	</noscript>

	<script type="text/javascript">
		var doc = document.documentElement;
		doc.setAttribute('data-useragent', navigator.userAgent);
	</script>

	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());
		gtag('config', 'UA-115795670-2', {
			'anonymize_ip': true
		});
	</script>
</head>

<body data-rsssl=1 class="home page-template page-template-home page-template-home-php page page-id-6 wpb-js-composer js-comp-ver-5.6 vc_responsive fusion-body ltr no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-mobile-totop fusion-disable-outline layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-flyout fusion-image-hovers fusion-show-pagination-text fusion-header-layout-v2 avada-responsive avada-footer-fx-none fusion-search-form-classic fusion-avatar-square">
	<!-- Google Tag Manager (noscript) -->

	<!-- End Google Tag Manager (noscript) -->

	<div class="hidden">
		<div class="wpb_text_column wpb_content_element ">
			<div class="wpb_wrapper">

			</div>
		</div>
	</div>
	<div id="cerca-fullwidth">
		<div id="mcc-close-search"><img src="wp-content/themes/mcc/assets/images/close.svg" /></div>
		<div class='asp_w asp_m asp_m_3 asp_m_3_1 wpdreams_asp_sc wpdreams_asp_sc-3 ajaxsearchpro asp_main_container  asp_non_compact' data-id="3" data-instance="1" id='ajaxsearchpro3_1'>
			<div class="probox">

				<div class='promagnifier'>
					<div class='asp_text_button hiddend'>
						Search </div>
					<div class='innericon'>
						<svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 512 512">
							<path d="M460.475 408.443L351.4 299.37c15.95-25.137 25.2-54.923 25.2-86.833C376.6 122.914 303.687 50 214.062 50 124.44 50 51.525 122.914 51.525 212.537s72.914 162.537 162.537 162.537c30.326 0 58.733-8.356 83.055-22.876L406.917 462l53.558-53.557zM112.117 212.537c0-56.213 45.732-101.946 101.945-101.946 56.213 0 101.947 45.734 101.947 101.947S270.275 314.482 214.06 314.482c-56.213 0-101.945-45.732-101.945-101.945z" /></svg> </div>
					<div class="asp_clear"></div>
				</div>



				<div class='prosettings' style='display:none;' data-opened=0>
					<div class='innericon'>
						<svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 512 512">
							<polygon transform="rotate(90 256 256)" points="142.332,104.886 197.48,50 402.5,256 197.48,462 142.332,407.113 292.727,256" /></svg> </div>
				</div>



				<div class='proinput'>
					<form action='#' autocomplete="off" aria-label="Search form 3">
						<input type='search' class='orig' placeholder='Cerca' name='phrase' value='' aria-label="Search input 3" autocomplete="off" />
						<input type='text' class='autocomplete' name='phrase' value='' aria-label="Search autocomplete, ignore please" aria-hidden="true" autocomplete="off" disabled />
						<input type='submit' aria-hidden="true" style='width:0; height: 0; visibility: hidden;'>
					</form>
				</div>



				<div class='proloading'>
					<div class="asp_loader">
						<div class="asp_loader-inner asp_ball-pulse">

							<div></div>

							<div></div>

							<div></div>
						</div>
					</div>
				</div>

				<div class='proclose'>
					<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512" enable-background="new 0 0 512 512" xml:space="preserve">
						<polygon id="x-mark-icon" points="438.393,374.595 319.757,255.977 438.378,137.348 374.595,73.607 255.995,192.225 137.375,73.622 73.607,137.352 192.246,255.983 73.622,374.625 137.352,438.393 256.002,319.734 374.652,438.378 " />
					</svg>
				</div>


			</div>
			<div id='ajaxsearchprores3_1' class='asp_w asp_r asp_r_3 asp_r_3_1 vertical ajaxsearchpro wpdreams_asp_sc wpdreams_asp_sc-3' data-id="3" data-instance="1">



				<div class="results">


					<div class="resdrg">
					</div>


				</div>





				<div class="asp_res_loader hiddend">
					<div class="asp_loader">
						<div class="asp_loader-inner asp_ball-pulse">

							<div></div>

							<div></div>

							<div></div>
						</div>
					</div>
				</div>
			</div>
			<div id='ajaxsearchprosettings3_1' class="asp_w asp_s asp_s_3 asp_s_3_1 wpdreams_asp_sc wpdreams_asp_sc-3 ajaxsearchpro searchsettings" data-id="3" data-instance="1">
				<form name='options' class="asp-fss-flex" autocomplete='off'>
					<input type="hidden" style="display:none;" name="current_page_id" value="6">
					<fieldset class="">
						<legend>Generic filters</legend>

						<div class="asp_option_inner hiddend">
							<input type='hidden' name='qtranslate_lang' value='0' />
						</div>




						<div class="asp_option">
							<div class="asp_option_inner">
								<input type="checkbox" value="exact" id="set_exact3_1" aria-label="Exact matches only" name="asp_gen[]" />
								<label aria-hidden="true" for="set_exact3_1"></label>
							</div>
							<div class="asp_option_label">
								Exact matches only </div>
						</div>
						<div class="asp_option hiddend" aria-hidden="true">
							<div class="asp_option_inner">
								<input type="checkbox" value="title" id="set_title3_1" name="asp_gen[]" checked="checked" />
								<label for="set_title3_1"></label>
							</div>
						</div>
						<div class="asp_option hiddend" aria-hidden="true">
							<div class="asp_option_inner">
								<input type="checkbox" value="content" id="set_content3_1" name="asp_gen[]" checked="checked" />
								<label for="set_content3_1"></label>
							</div>
						</div>
						<div class="asp_option hiddend" aria-hidden="true">
							<div class="asp_option_inner">
								<input type="checkbox" value="excerpt" id="set_excerpt3_1" name="asp_gen[]" />
								<label for="set_excerpt3_1"></label>
							</div>
						</div>
						<div class="asp_option hiddend" aria-hidden="true">
							<div class="asp_option_inner">
								<input type="checkbox" value="comments" id="set_comments3_1" name="asp_gen[]" />
								<label for="set_comments3_1"></label>
							</div>
						</div>
					</fieldset>
					<fieldset class="asp_sett_scroll hiddend asp_checkboxes_filter_box">
						<legend>Filter by Custom Post Type</legend>
						<div class="asp_option_inner hiddend" aria-hidden="true">
							<input type="checkbox" value="operazionisocietarie" id="3_1customset_3_11" name="customset[]" checked="checked" />
							<label for="3_1customset_3_11"></label>
						</div>
						<div class="asp_option_label hiddend"></div>
						<div class="asp_option_inner hiddend" aria-hidden="true">
							<input type="checkbox" value="sospensionerate" id="3_1customset_3_12" name="customset[]" checked="checked" />
							<label for="3_1customset_3_12"></label>
						</div>
						<div class="asp_option_label hiddend"></div>
						<div class="asp_option_inner hiddend" aria-hidden="true">
							<input type="checkbox" value="investimenti" id="3_1customset_3_13" name="customset[]" checked="checked" />
							<label for="3_1customset_3_13"></label>
						</div>
						<div class="asp_option_label hiddend"></div>
						<div class="asp_option_inner hiddend" aria-hidden="true">
							<input type="checkbox" value="post" id="3_1customset_3_14" name="customset[]" checked="checked" />
							<label for="3_1customset_3_14"></label>
						</div>
						<div class="asp_option_label hiddend"></div>
						<div class="asp_option_inner hiddend" aria-hidden="true">
							<input type="checkbox" value="missioni" id="3_1customset_3_15" name="customset[]" checked="checked" />
							<label for="3_1customset_3_15"></label>
						</div>
						<div class="asp_option_label hiddend"></div>
						<div class="asp_option_inner hiddend" aria-hidden="true">
							<input type="checkbox" value="primopiano" id="3_1customset_3_16" name="customset[]" checked="checked" />
							<label for="3_1customset_3_16"></label>
						</div>
						<div class="asp_option_label hiddend"></div>
						<div class="asp_option_inner hiddend" aria-hidden="true">
							<input type="checkbox" value="page" id="3_1customset_3_17" name="customset[]" checked="checked" />
							<label for="3_1customset_3_17"></label>
						</div>
						<div class="asp_option_label hiddend"></div>
						<div class="asp_option_inner hiddend" aria-hidden="true">
							<input type="checkbox" value="agevolazione" id="3_1customset_3_18" name="customset[]" checked="checked" />
							<label for="3_1customset_3_18"></label>
						</div>
						<div class="asp_option_label hiddend"></div>
						<div class="asp_option_inner hiddend" aria-hidden="true">
							<input type="checkbox" value="finanziamento" id="3_1customset_3_19" name="customset[]" checked="checked" />
							<label for="3_1customset_3_19"></label>
						</div>
						<div class="asp_option_label hiddend"></div>
					</fieldset>
					<div style="clear:both;"></div>
				</form>
			</div>

		</div>
		<div class='asp_hidden_data' id="asp_hidden_data_3_1" style="display:none;">

			<div class='asp_item_overlay'>
				<div class='asp_item_inner'>
					<svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 512 512">
						<path d="M448.225 394.243l-85.387-85.385c16.55-26.08 26.146-56.986 26.146-90.094 0-92.99-75.652-168.64-168.643-168.64-92.988 0-168.64 75.65-168.64 168.64s75.65 168.64 168.64 168.64c31.466 0 60.94-8.67 86.176-23.734l86.14 86.142c36.755 36.754 92.355-18.783 55.57-55.57zm-344.233-175.48c0-64.155 52.192-116.35 116.35-116.35s116.353 52.194 116.353 116.35S284.5 335.117 220.342 335.117s-116.35-52.196-116.35-116.352zm34.463-30.26c34.057-78.9 148.668-69.75 170.248 12.863-43.482-51.037-119.984-56.532-170.248-12.862z" /></svg> </div>
			</div>

		</div>
		<style type="text/css">
			/* User defined Ajax Search Pro Custom CSS */
			div.asp_m.ajaxsearchpro .probox .proclose svg {

				background: #333;
				border-radius: 50%;
				position: absolute;
				top: 50%;
				width: 50px;
				height: 50px;
				margin-top: -10px;
				left: 50%;
				margin-left: -10px;
				fill: #fefefe;
				padding: 4px;
				box-sizing: border-box;
				box-shadow: 0 0 0 2px rgba(255, 255, 255, .9);

			}

			#ajaxsearchpro3_1 .probox,
			#ajaxsearchpro3_2 .probox,
			div.asp_m.asp_m_3 .probox {
				margin: 40px;
				height: 77px;
				background-image: -moz-radial-gradient(center, ellipse cover, rgb(255, 255, 255), rgb(255, 255, 255));
				background-image: -webkit-gradient(radial, center center, 0px, center center, 100%, rgb(255, 255, 255), rgb(255, 255, 255));
				background-image: -webkit-radial-gradient(center, ellipse cover, #fff, #fff);
				background-image: -o-radial-gradient(center, ellipse cover, rgb(255, 255, 255), rgb(255, 255, 255));
				background-image: -ms-radial-gradient(center, ellipse cover, rgb(255, 255, 255), rgb(255, 255, 255));
				background-image: radial-gradient(ellipse at center, #fff, #fff);
				border-bottom: 1px solid #c5c5c5;
				r-radius: 0px 0px 0px 0px;
				box-shadow: 0px 0px 0px 0px rgb(181, 181, 181) inset;
				width: 83%;
				margin: 0 auto;
			}

			#ajaxsearchpro3_1,
			#ajaxsearchpro3_2,
			div.asp_m.asp_m_3 {
				width: 100%;
				height: auto;
				max-height: none;
				border-radius: 5px;
				background: #d1eaff;
				background-image: -moz-radial-gradient(center, ellipse cover, rgb(255, 255, 255), rgb(255, 255, 255));
				background-image: -webkit-gradient(radial, center center, 0px, center center, 100%, rgb(255, 255, 255), rgb(255, 255, 255));
				background-image: -webkit-radial-gradient(center, ellipse cover, #fff, #fff);
				background-image: -o-radial-gradient(center, ellipse cover, rgb(255, 255, 255), rgb(255, 255, 255));
				background-image: -ms-radial-gradient(center, ellipse cover, rgb(255, 255, 255), rgb(255, 255, 255));
				background-image: radial-gradient(ellipse at center, #fff, #fff);
				overflow: hidden;
				border: 1px none #e4e4e4;
				border-radius: 0px 0px 0px 0px;
				box-shadow: 0px 0px 0px 0px #e4e4e4;
				padding-top: 3em;
			}

			div.asp_w.asp_r .results {
				width: 83%;
				margin: 0 auto;
				background: #ffffff;
			}

			#ajaxsearchprores3_1.vertical,
			#ajaxsearchprores3_2.vertical,
			div.asp_r.asp_r_3.vertical {
				z-index: 99999;
				-webkit-box-shadow: 0px 17px 20px -12px rgba(148, 148, 148, 1);
				-moz-box-shadow: 0px 17px 20px -12px rgba(148, 148, 148, 1);
				box-shadow: 0px 17px 20px -12px rgba(148, 148, 148, 1);
			}
		</style>
		<div class="asp_init_data" style="display:none !important;" id="asp_init_id_3_1" data-aspdata="ew0KICAgICJob21ldXJsIjogImh0dHBzOi8vd3d3Lm1jYy5pdC8iLA0KICAgICJyZXN1bHRzdHlwZSI6ICJ2ZXJ0aWNhbCIsDQogICAgInJlc3VsdHNwb3NpdGlvbiI6ICJob3ZlciIsDQogICAgIml0ZW1zY291bnQiOiA0LA0KICAgICJpbWFnZXdpZHRoIjogNzAsDQogICAgImltYWdlaGVpZ2h0IjogNzAsDQogICAgInJlc3VsdGl0ZW1oZWlnaHQiOiAiYXV0byIsDQogICAgInNob3dhdXRob3IiOiAwLA0KICAgICJzaG93ZGF0ZSI6IDAsDQogICAgInNob3dkZXNjcmlwdGlvbiI6IDAsDQogICAgImNoYXJjb3VudCI6ICAwLA0KICAgICJkZWZhdWx0SW1hZ2UiOiAiaHR0cHM6Ly93d3cubWNjLml0L3dwLWNvbnRlbnQvcGx1Z2lucy9hamF4LXNlYXJjaC1wcm8vaW1nL2RlZmF1bHQuanBnIiwNCiAgICAiaGlnaGxpZ2h0IjogMCwNCiAgICAiaGlnaGxpZ2h0d2hvbGV3b3JkcyI6IDEsDQogICAgIm9wZW5Ub0JsYW5rIjogMCwNCiAgICAic2Nyb2xsVG9SZXN1bHRzIjogMCwNCiAgICAicmVzdWx0YXJlYWNsaWNrYWJsZSI6IDEsDQogICAgImF1dG9jb21wbGV0ZSI6IHsNCiAgICAgICAgImVuYWJsZWQiOiAxLA0KICAgICAgICAiZ29vZ2xlT25seSI6IDAsDQogICAgICAgICJsYW5nIjogIml0IiwNCiAgICAgICAgIm1vYmlsZSI6IDEgICAgfSwNCiAgICAidHJpZ2dlcm9udHlwZSI6IDEsDQogICAgInRyaWdnZXJfb25fY2xpY2siOiAwLA0KICAgICJ0cmlnZ2VyT25GYWNldENoYW5nZSI6IDEsDQogICAgInRyaWdnZXIiOiB7DQogICAgICAgICJkZWxheSI6IDMwMCwNCiAgICAgICAgImF1dG9jb21wbGV0ZV9kZWxheSI6IDMxMCAgICB9LA0KICAgICJvdmVycmlkZXdwZGVmYXVsdCI6IDEsDQogICAgIm92ZXJyaWRlX21ldGhvZCI6ICJnZXQiLA0KICAgICJyZWRpcmVjdG9uY2xpY2siOiAxLA0KICAgICJyZWRpcmVjdENsaWNrVG8iOiAicmVzdWx0c19wYWdlIiwNCiAgICAicmVkaXJlY3RDbGlja0xvYyI6ICJzYW1lIiwNCiAgICAicmVkaXJlY3Rfb25fZW50ZXIiOiAwLA0KICAgICJyZWRpcmVjdEVudGVyVG8iOiAiYWpheF9zZWFyY2giLA0KICAgICJyZWRpcmVjdEVudGVyTG9jIjogInNhbWUiLA0KICAgICJyZWRpcmVjdF91cmwiOiAiP3M9e3BocmFzZX0iLA0KICAgICJzZXR0aW5nc2ltYWdlcG9zIjogInJpZ2h0IiwNCiAgICAic2V0dGluZ3NWaXNpYmxlIjogMCwNCiAgICAic2V0dGluZ3NIaWRlT25SZXMiOiAwLA0KICAgICJocmVzdWx0aGlkZWRlc2MiOiAiMCIsDQogICAgInByZXNjb250YWluZXJoZWlnaHQiOiAiNDAwcHgiLA0KICAgICJwc2hvd3N1YnRpdGxlIjogIjAiLA0KICAgICJwc2hvd2Rlc2MiOiAiMSIsDQogICAgImNsb3NlT25Eb2NDbGljayI6IDEsDQogICAgImlpZk5vSW1hZ2UiOiAiZGVzY3JpcHRpb24iLA0KICAgICJpaVBhZ2luYXRpb24iOiAxLA0KICAgICJpaVJvd3MiOiAyLA0KICAgICJpaUd1dHRlciI6IDUsDQogICAgImlpdGVtc1dpZHRoIjogIjIwMHB4IiwNCiAgICAiaWl0ZW1zSGVpZ2h0IjogMjAwLA0KICAgICJpaXNob3dPdmVybGF5IjogMSwNCiAgICAiaWlibHVyT3ZlcmxheSI6IDEsDQogICAgImlpaGlkZUNvbnRlbnQiOiAxLA0KICAgICJsb2FkZXJMb2NhdGlvbiI6ICJhdXRvIiwNCiAgICAiYW5hbHl0aWNzIjogMCwNCiAgICAiYW5hbHl0aWNzU3RyaW5nIjogIj9hamF4X3NlYXJjaD17YXNwX3Rlcm19IiwNCiAgICAic2hvd19tb3JlIjogew0KICAgICAgICAiZW5hYmxlZCI6IDAsDQogICAgICAgICJ1cmwiOiAiP3M9e3BocmFzZX0iLA0KICAgICAgICAiYWN0aW9uIjogImFqYXgiLA0KICAgICAgICAibG9jYXRpb24iOiAic2FtZSIsDQogICAgICAgICJpbmZpbml0ZSI6IDEgICAgfSwNCiAgICAibW9iaWxlIjogew0KICAgICAgICAidHJpZ2dlcl9vbl90eXBlIjogMSwNCiAgICAgICAgImNsaWNrX2FjdGlvbiI6ICJyZXN1bHRzX3BhZ2UiLA0KICAgICAgICAicmV0dXJuX2FjdGlvbiI6ICJhamF4X3NlYXJjaCIsDQogICAgICAgICJjbGlja19hY3Rpb25fbG9jYXRpb24iOiAic2FtZSIsDQogICAgICAgICJyZXR1cm5fYWN0aW9uX2xvY2F0aW9uIjogInNhbWUiLA0KICAgICAgICAicmVkaXJlY3RfdXJsIjogIj9zPXtwaHJhc2V9IiwNCiAgICAgICAgImhpZGVfa2V5Ym9hcmQiOiAxLA0KICAgICAgICAiZm9yY2VfcmVzX2hvdmVyIjogMSwNCiAgICAgICAgImZvcmNlX3NldHRfaG92ZXIiOiAxLA0KICAgICAgICAiZm9yY2Vfc2V0dF9zdGF0ZSI6ICJjbG9zZWQiDQogICAgfSwNCiAgICAiY29tcGFjdCI6IHsNCiAgICAgICAgImVuYWJsZWQiOiAwLA0KICAgICAgICAid2lkdGgiOiAiMTAwJSIsDQogICAgICAgICJjbG9zZU9uTWFnbmlmaWVyIjogMSwNCiAgICAgICAgImNsb3NlT25Eb2N1bWVudCI6IDAsDQogICAgICAgICJwb3NpdGlvbiI6ICJzdGF0aWthousand0KICAgICAgICAib3ZlcmxheSI6IDAgICAgfSwNCiAgICAic2IiOiB7DQogICAgICAgICJyZWRpcmVjdF9hY3Rpb24iOiAicmVzdWx0c19wYWdlIiwNCiAgICAgICAgInJlZGlyZWN0X2xvY2F0aW9uIjogInNhbWUiLA0KICAgICAgICAicmVkaXJlY3RfdXJsIjogIj9zPXtwaHJhc2V9Ig0KICAgIH0sDQogICAgImFuaW1hdGlvbnMiOiB7DQogICAgICAgICJwYyI6IHsNCiAgICAgICAgICAgICJzZXR0aW5ncyI6IHsNCiAgICAgICAgICAgICAgICAiYW5pbSIgOiAiZmFkZWRyb3AiLA0KICAgICAgICAgICAgICAgICJkdXIiICA6IDMwMCAgICAgICAgICAgIH0sDQogICAgICAgICAgICAicmVzdWx0cyIgOiB7DQogICAgICAgICAgICAgICAgImFuaW0iIDogImZhZGVkcm9wIiwNCiAgICAgICAgICAgICAgICAiZHVyIiAgOiAzMDAgICAgICAgICAgICB9LA0KICAgICAgICAgICAgIml0ZW1zIiA6ICJmYWRlSW5Eb3duIg0KICAgICAgICB9LA0KICAgICAgICAibW9iIjogew0KICAgICAgICAgICAgInNldHRpbmdzIjogew0KICAgICAgICAgICAgICAgICJhbmltIiA6ICJmYWRlZHJvcCIsDQogICAgICAgICAgICAgICAgImR1ciIgIDogMzAwICAgICAgICAgICAgfSwNCiAgICAgICAgICAgICJyZXN1bHRzIiA6IHsNCiAgICAgICAgICAgICAgICAiYW5pbSIgOiAiZmFkZWRyb3AiLA0KICAgICAgICAgICAgICAgICJkdXIiICA6IDMwMCAgICAgICAgICAgIH0sDQogICAgICAgICAgICAiaXRlbXMiIDogInZvaWRhbmltIg0KICAgICAgICB9DQogICAgfSwNCiAgICAiY2hvc2VuIjogew0KICAgICAgICAibm9yZXMiOiAiTm8gcmVzdWx0cyBtYXRjaCINCiAgICB9LA0KICAgICJkZXRlY3RWaXNpYmlsaXR5IiA6IDAsDQogICAgImF1dG9wIjogew0KICAgICAgICAic3RhdGUiOiAiZGlzYWJsZWQiLA0KICAgICAgICAicGhyYXNlIjogIiIsDQogICAgICAgICJjb3VudCI6IDEwICAgIH0sDQogICAgInJlc1BhZ2UiOiB7DQogICAgICAgICJ1c2VBamF4IjogMCwNCiAgICAgICAgInNlbGVjdG9yIjogIiNtYWluIg0KICAgIH0sDQogICAgImZzc19sYXlvdXQiOiAiZmxleCIsDQogICAgInN0YXRpc3RpY3MiOiAwfQ0K"></div>
	</div>
	<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
	<div id="wrapper" class="">
		<div id="home" style="position:relative;top:-1px;"></div>

		<header class="fusion-header-wrapper">
			<div class="fusion-header-v2 fusion-logo-left fusion-sticky-menu- fusion-sticky-logo- fusion-mobile-logo-1  fusion-mobile-menu-design-flyout fusion-header-has-flyout-menu">

				<div class="fusion-secondary-header">
					<div class="fusion-row">
						<div class="fusion-alignright">
							<nav class="fusion-secondary-menu" role="navigation" aria-label="Secondary Menu">
								<ul id="menu-menu-top" class="menu">
									<li id="menu-item-88" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-88" data-item-id="88"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="Quick Links"><span class="menu-text">Quick links</span></a></li>
									<li id="menu-item-87" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-87" data-item-id="87"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="FAQ"><span class="menu-text">FAQ</span></a></li>
									<li id="menu-item-5088" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5088" data-item-id="5088"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="Work with us"><span class="menu-text">Work with us</span></a></li>
									<li id="menu-item-86" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-86" data-item-id="86"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="Where we are"><span class="menu-text">Where we Are</span></a></li>
									<li id="menu-item-85" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-85" data-item-id="85"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="Contacts"><span class="menu-text">Contacts</span></a></li>
									<li id="menu-item-89" class="fadeSearch menu-item menu-item-type-custom menu-item-object-custom menu-item-89" data-classes="fadeSearch" data-item-id="89"><a href="#" class="fusion-icon-only-link fusion-flex-link fusion-bar-highlight" role="menuitem" data-name="Search"><span class="fusion-megamenu-icon"><i class="glyphicon fa-search fas"></i></span><span class="menu-text"><span class="menu-title">Search</span></span></a></li>
								</ul>
							</nav>
							<nav class="fusion-mobile-nav-holder fusion-mobile-menu-text-align-left" aria-label="Secondary Mobile Menu"></nav>
						</div>
					</div>
				</div>
				<div class="fusion-header-sticky-height"></div>
				<div class="fusion-header">
					<div class="fusion-row">
						<div class="fusion-header-has-flyout-menu-content">
							<div class="fusion-logo" data-margin-top="31px" data-margin-bottom="31px" data-margin-left="0px" data-margin-right="0px">
								<a class="fusion-logo-link" href="index.php">

									<!-- standard logo -->
									<img src="images/logo/cover.png" alt="Centennial Bank Logo" />

									<!-- mobile logo -->
									<img src="images/logo/cover.png" width="180" height="113" style="max-height:113px;height:auto;" alt="Centennial Bank Logo" class="fusion-mobile-logo" />

								</a>
							</div>
							<nav class="fusion-main-menu" aria-label="Main Menu">
								<ul id="menu-main-menu" class="fusion-menu">
									<li id="menu-item-93" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-93 fusion-dropdown-menu" data-item-id="93">
										<a href="login.php" class="fusion-bar-highlight" role="menuitem" data-name="LOG IN"><span class="menu-text">LOG IN</span>
										</a></li>
									<li id="menu-item-93" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-93 fusion-dropdown-menu" data-item-id="93">
										<a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="WHO WE ARE"><span class="menu-text">WHO WE ARE</span>
										</a>
										<ul role="menu" class="sub-menu">
											<li id="menu-item-4806" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4806 fusion-dropdown-submenu">
												<a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="MISSION"><span>MISSION</span></a>
											</li>
											<li id="menu-item-4807" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4807 fusion-dropdown-submenu">
												<a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="GOVERNANCE"><span>GOVERNANCE</span></a>
											</li>
											<li id="menu-item-4808" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4808 fusion-dropdown-submenu">
												<a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="COMPANY INFO"><span>COMPANY INFO</span></a>
											</li>
											<li id="menu-item-4809" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4809 fusion-dropdown-submenu">
												<a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="INVEST IN US"><span>INVEST IN US</span></a>
											</li>
											<li id="menu-item-5087" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5087 fusion-dropdown-submenu"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="WORK WITH US"><span>WORK WITH US</span></a></li>
										</ul>
									</li>
									<li id="menu-item-3882" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3882 fusion-dropdown-menu" data-item-id="3882"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="WHAT WE DO"><span class="menu-text">WHAT WE DO</span></a>
										<ul role="menu" class="sub-menu">
											<li id="menu-item-4812" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4812 fusion-dropdown-submenu"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="FUNDING"><span>FINANZIAMENTI</span></a></li>
											<li id="menu-item-4811" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4811 fusion-dropdown-submenu"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="AGEVOLAZIONI"><span>AGEVOLAZIONI</span></a></li>
										</ul>
									</li>
									<li id="menu-item-2683" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2683 fusion-dropdown-menu" data-item-id="2683"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="IN THE SPOTLIGHT"><span class="menu-text">IN THE SPOTLIGHT</span></a>
										<ul role="menu" class="sub-menu">
											<li id="menu-item-4813" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4813 fusion-dropdown-submenu"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="NOTIZIE"><span>NOTIZIE</span></a></li>
											<li id="menu-item-4814" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4814 fusion-dropdown-submenu"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="Success Stories"><span>Success Stories</span></a></li>
										</ul>
									</li>
								</ul>
							</nav>
							<div class="fusion-mobile-navigation">
								<ul id="menu-mobile-menu" class="fusion-mobile-menu">
									<li id="menu-item-1656" class="title-sotto-menu-mobile menu-item menu-item-type-custom menu-item-object-custom menu-item-1656" data-classes="title-sotto-menu-mobile" data-item-id="1656"><a href="#" class="fusion-bar-highlight" role="menuitem" data-name="Menu"><span class="menu-text">Menu</span></a></li>
									<li id="menu-item-1657" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-6 current_page_item menu-item-1657" data-item-id="1657"><a href="login.php" class="fusion-bar-highlight" role="menuitem" data-name="HOME"><span class="menu-text">LOG IN</span></a></li>
									<li id="menu-item-4377" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4377" data-item-id="4377"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="WHO WE ARE"><span class="menu-text">WHO WE ARE</span></a></li>
									<li id="menu-item-1659" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1659" data-item-id="1659"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="WHAT WE DO"><span class="menu-text">WHAT WE DO</span></a></li>
									<li id="menu-item-3071" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3071" data-item-id="3071"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="Primo piano"><span class="menu-text">Primo piano</span></a></li>
									<li id="menu-item-1662" class="title-sotto-menu-mobile-2lv menu-item menu-item-type-custom menu-item-object-custom menu-item-1662" data-classes="title-sotto-menu-mobile-2lv" data-item-id="1662"><a href="#" class="fusion-bar-highlight" role="menuitem" data-name="Link utili"><span class="menu-text">Link utili</span></a></li>
									<li id="menu-item-1666" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1666" data-item-id="1666"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="LINK VELOCI"><span class="menu-text">LINK VELOCI</span></a></li>
									<li id="menu-item-1665" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1665" data-item-id="1665"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="FAQ"><span class="menu-text">FAQ</span></a></li>
									<li id="menu-item-5089" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5089" data-item-id="5089"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="LAVORA CON NOI"><span class="menu-text">LAVORA CON NOI</span></a></li>
									<li id="menu-item-1664" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1664" data-item-id="1664"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="DOVE SIAMO"><span class="menu-text">DOVE SIAMO</span></a></li>
									<li id="menu-item-1663" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1663" data-item-id="1663"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="CONTATTI"><span class="menu-text">CONTATTI</span></a></li>
								</ul>
							</div>
							<div class="fusion-flyout-menu-icons fusion-flyout-mobile-menu-icons">



								<div class="fusion-flyout-search-toggle">
									<div class="fusion-toggle-icon">
										<div class="fusion-toggle-icon-line"></div>
										<div class="fusion-toggle-icon-line"></div>
										<div class="fusion-toggle-icon-line"></div>
									</div>
									<a class="fusion-icon fusion-icon-search" aria-hidden="true" aria-label="Toggle Search" href="#"></a>
								</div>

								<a class="fusion-flyout-menu-toggle" aria-hidden="true" aria-label="Toggle Menu" href="#">
									<div class="fusion-toggle-icon-line"></div>
									<div class="fusion-toggle-icon-line"></div>
									<div class="fusion-toggle-icon-line"></div>
								</a>
							</div>

							<div class="fusion-flyout-search">
								<form role="search" class="searchform fusion-search-form" method="get" action="https://www.mcc.it/">
									<div class="fusion-search-form-content">
										<div class="fusion-search-field search-field">
											<label class="screen-reader-text" for="s">Search for:</label>
											<input type="text" value="" name="s" class="s" placeholder="Search ..." required aria-required="true" aria-label="Search ..." />
										</div>
										<div class="fusion-search-button search-button">
											<input type="submit" class="fusion-search-submit searchsubmit" value="&#xf002;" />
										</div>
									</div>
								</form>
							</div>

							<div class="fusion-flyout-menu-bg"></div>

							<nav class="fusion-mobile-nav-holder fusion-flyout-menu fusion-flyout-mobile-menu" aria-label="Main Menu Mobile"></nav>

						</div>
					</div>
				</div>
			</div>
			<div class="fusion-clearfix"></div>
		</header>


		<div id="sliders-container">
			<link href="https://fonts.googleapis.com/css?family=Barlow:500%2C300%2C400" rel="stylesheet" property="stylesheet" type="text/css" media="all">
			<div id="rev_slider_4_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" style="margin:0px auto;background:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;background-image:url(wp-content/uploads/2019/02/slide-background.jpg);background-repeat:no-repeat;background-size:cover;background-position:center center;">
				<!-- START REVOLUTION SLIDER 5.4.8 fullwidth mode -->
				<div id="rev_slider_4_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.8">
					<ul>
						<!-- SLIDE  -->
						<li data-index="rs-11" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-rotate="0" data-saveperformance="off" data-title="Slide-1" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
							<!-- MAIN IMAGE -->
							<img src="wp-content/uploads/2019/01/slide1.jpg" alt="Centennial Bank - Diamo credito all'Italia" title="Centennial Bank &#8211; Diamo credito all&#8217;Italia" width="1920" height="1051" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
							<!-- LAYERS -->

							<!-- LAYER NR. 1 -->
							<div class="tp-caption     rev_group" id="slide-11-layer-81" data-x="['left','left','left','left']" data-hoffset="['102','100','100','100']" data-y="['top','middle','middle','middle']" data-voffset="['296','0','0','0']" data-width="538" data-height="117" data-whitespace="nowrap" data-visibility="['on','off','off','off']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; min-width: 538px; max-width: 538px; max-width: 117px; max-width: 117px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 2 -->
								<h1 class="tp-caption   tp-resizeme" id="slide-11-layer-1" data-x="['left','left','left','center']" data-hoffset="['0','0','128','128']" data-y="['top','middle','top','middle']" data-voffset="['0','0','358','358']" data-fontsize="['50','44','44','50']" data-lineheight="['68','40','40','68']" data-letterspacing="['0','1','1','-2']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['on','off','off','off']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":"+770","speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeIn"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 50px; line-height: 68px; font-weight: 500; color: #ffffff; letter-spacing: 0px;font-family:Barlow;margin:0px;">Diamo credito all'Italia </h1>

								<!-- LAYER NR. 3 -->
								<h1 class="tp-caption   tp-resizeme" id="slide-11-layer-23" data-x="['left','left','left','center']" data-hoffset="['0','0','127','127']" data-y="['bottom','bottom','top','middle']" data-voffset="['11','0','405','405']" data-fontsize="['30','24','24','34']" data-lineheight="['38','30','30','68']" data-letterspacing="['1','1','1','-2']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['on','off','off','off']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":"+860","speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeIn"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; font-size: 30px; line-height: 38px; font-weight: 300; color: #ffffff; letter-spacing: 1px;font-family:Barlow;margin:0px;">Valorizziamo le Imprese </h1>
							</div>

							<!-- LAYER NR. 4 -->
							<div class="tp-caption     rev_group" id="slide-11-layer-76" data-x="['left','center','left','left']" data-hoffset="['767','150','774','775']" data-y="['bottom','bottom','top','top']" data-voffset="['76','0','304','304']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['on','off','off','off']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":1460,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 8; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 5 -->
								<div class="tp-caption rev-btn   mcc-slider-tondo" id="slide-11-layer-74" data-x="['left','left','left','left']" data-hoffset="['-1','-1','-1','-1']" data-y="['top','top','top','top']" data-voffset="['-1','-1','-1','-1']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['on','off','off','off']" data-type="button" data-actions='[{"event":"click","action":"simplelink","target":"_self","url":"https:\/\/www.mcc.it\/wizard","delay":""}]' data-responsive_offset="on" data-frames='[{"delay":"+10","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(14,125,87);bg:rgb(255,255,255);bs:solid;bw:0 0 0 0;"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['center','center','center','center']" data-paddingtop="[99,99,99,99]" data-paddingright="[130,130,130,130]" data-paddingbottom="[130,130,130,130]" data-paddingleft="[42,42,42,42]" style="z-index: 9; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 24px; line-height: 28px; font-weight: 400; color: rgba(255,255,255,1); letter-spacing: px;font-family:Barlow;background-color:rgba(3,164,115,0.7);border-color:rgba(0,0,0,1);border-radius:130px 130px 130px 130px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Trova la soluzione<br />per la tua Impresa </div>
							</div>

							<!-- LAYER NR. 6 -->
							<div class="tp-caption     rev_group" id="slide-11-layer-77" data-x="['left','center','left','left']" data-hoffset="['990','400','1008','1008']" data-y="['top','middle','top','top']" data-voffset="['144','0','128','128']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['on','off','off','off']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":1430,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 10; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 7 -->
								<a class="tp-caption rev-btn   mcc-slider-tondo-bianco" href="index.php" target="_self" id="slide-11-layer-79" data-x="['left','left','left','left']" data-hoffset="['-1','-1','-1','-1']" data-y="['top','top','top','top']" data-voffset="['-2','-2','-2','-2']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['on','off','off','off']" data-type="button" data-actions='' data-responsive_offset="on" data-frames='[{"delay":"+10","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgba(0,0,0,0.2);bs:solid;bw:0 0 0 0;"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['center','center','center','center']" data-paddingtop="[99,99,99,99]" data-paddingright="[42,42,42,42]" data-paddingbottom="[130,130,130,130]" data-paddingleft="[42,42,42,42]" style="z-index: 11; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 24px; line-height: 28px; font-weight: 400; color: #0e7d57; letter-spacing: px;font-family:Barlow;background-color:rgba(255,255,255,0.9);border-color:rgba(0,0,0,1);border-radius:130px 130px 130px 130px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">Scopri la nostra<br />offerta </a>
							</div>

							<!-- LAYER NR. 8 -->
							<div class="tp-caption     rev_group" id="slide-11-layer-118" data-x="['left','left','left','left']" data-hoffset="['107','78','78','78']" data-y="['top','middle','middle','middle']" data-voffset="['301','16','16','16']" data-width="538" data-height="117" data-whitespace="nowrap" data-visibility="['off','on','off','off']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 12; min-width: 538px; max-width: 538px; max-width: 117px; max-width: 117px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 9 -->
								<h1 class="tp-caption  " id="slide-11-layer-119" data-x="['left','left','left','center']" data-hoffset="['0','0','128','128']" data-y="['top','middle','top','middle']" data-voffset="['0','0','358','358']" data-fontsize="['50','44','44','50']" data-lineheight="['68','40','40','68']" data-letterspacing="['0','1','1','-2']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['off','on','off','off']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":"+770","speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeIn"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 13; white-space: nowrap; font-size: 50px; line-height: 68px; font-weight: 500; color: #ffffff; letter-spacing: 0px;font-family:Barlow;margin:0px;">Diamo credito all'Italia </h1>

								<!-- LAYER NR. 10 -->
								<h1 class="tp-caption  " id="slide-11-layer-120" data-x="['left','left','left','center']" data-hoffset="['0','0','127','127']" data-y="['bottom','bottom','top','middle']" data-voffset="['11','0','405','405']" data-fontsize="['30','24','24','34']" data-lineheight="['38','30','30','68']" data-letterspacing="['1','1','1','-2']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['off','on','off','off']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":"+860","speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeIn"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 14; white-space: nowrap; font-size: 30px; line-height: 38px; font-weight: 300; color: #ffffff; letter-spacing: 1px;font-family:Barlow;margin:0px;">Valorizziamo le Imprese </h1>
							</div>

							<!-- LAYER NR. 11 -->
							<div class="tp-caption     rev_group" id="slide-11-layer-112" data-x="['left','center','left','left']" data-hoffset="['779','130','774','775']" data-y="['top','bottom','top','top']" data-voffset="['308','75','304','304']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['off','on','off','off']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":1460,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 15; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 12 -->
								<div class="tp-caption rev-btn   mcc-slider-tondo" id="slide-11-layer-113" data-x="['left','left','left','left']" data-hoffset="['-1','-1','-1','-1']" data-y="['top','top','top','top']" data-voffset="['-1','-1','-1','-1']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['off','on','off','off']" data-type="button" data-actions='[{"event":"click","action":"simplelink","target":"_self","url":"https:\/\/www.mcc.it\/wizard","delay":""}]' data-responsive_offset="on" data-frames='[{"delay":"+10","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(14,125,87);bg:rgb(255,255,255);bs:solid;bw:0 0 0 0;"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['center','center','center','center']" data-paddingtop="[99,99,99,99]" data-paddingright="[130,130,130,130]" data-paddingbottom="[130,130,130,130]" data-paddingleft="[42,42,42,42]" style="z-index: 16; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 24px; line-height: 28px; font-weight: 400; color: rgba(255,255,255,1); letter-spacing: px;font-family:Barlow;background-color:rgba(3,164,115,0.7);border-color:rgba(0,0,0,1);border-radius:130px 130px 130px 130px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Trova la soluzione<br />per la tua Impresa </div>
							</div>

							<!-- LAYER NR. 13 -->
							<div class="tp-caption     rev_group" id="slide-11-layer-115" data-x="['left','center','left','left']" data-hoffset="['1013','391','1008','1008']" data-y="['top','middle','top','top']" data-voffset="['133','-20','128','128']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['off','on','off','off']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":1430,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 17; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 14 -->
								<a class="tp-caption rev-btn   mcc-slider-tondo-bianco" href="index.php" target="_self" id="slide-11-layer-116" data-x="['left','left','left','left']" data-hoffset="['-1','-1','-1','-1']" data-y="['top','top','top','top']" data-voffset="['-2','-2','-2','-2']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['off','on','off','off']" data-type="button" data-actions='' data-responsive_offset="on" data-frames='[{"delay":"+10","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgba(0,0,0,0.2);bs:solid;bw:0 0 0 0;"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['center','center','center','center']" data-paddingtop="[99,99,99,99]" data-paddingright="[42,42,42,42]" data-paddingbottom="[130,130,130,130]" data-paddingleft="[42,42,42,42]" style="z-index: 18; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 24px; line-height: 28px; font-weight: 400; color: #0e7d57; letter-spacing: px;font-family:Barlow;background-color:rgba(255,255,255,0.9);border-color:rgba(0,0,0,1);border-radius:130px 130px 130px 130px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">Scopri la nostra<br />offerta </a>
							</div>

							<!-- LAYER NR. 15 -->
							<div class="tp-caption     rev_group" id="slide-11-layer-82" data-x="['left','left','center','center']" data-hoffset="['219','219','0','0']" data-y="['top','top','middle','middle']" data-voffset="['152','152','-132','-132']" data-width="['538','538','455','455']" data-height="['117','117','92','92']" data-whitespace="nowrap" data-visibility="['off','off','on','off']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 19; min-width: 538px; max-width: 538px; max-width: 117px; max-width: 117px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 16 -->
								<h1 class="tp-caption   tp-resizeme" id="slide-11-layer-83" data-x="['left','left','center','center']" data-hoffset="['0','107','0','0']" data-y="['top','top','top','middle']" data-voffset="['0','265','0','-96']" data-fontsize="['50','44','44','50']" data-lineheight="['68','40','40','68']" data-letterspacing="['0','1','1','-2']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['off','off','on','off']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":"+770","speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeIn"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 20; white-space: nowrap; font-size: 50px; line-height: 68px; font-weight: 500; color: #ffffff; letter-spacing: 0px;font-family:Barlow;margin:0px;">Diamo credito all'Italia </h1>

								<!-- LAYER NR. 17 -->
								<h1 class="tp-caption   tp-resizeme" id="slide-11-layer-84" data-x="['left','left','left','center']" data-hoffset="['0','107','81','0']" data-y="['top','top','top','middle']" data-voffset="['68','309','50','-49']" data-fontsize="['30','24','24','34']" data-lineheight="['38','30','30','68']" data-letterspacing="['1','1','1','-2']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['off','off','on','off']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":"+860","speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeIn"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 21; white-space: nowrap; font-size: 30px; line-height: 38px; font-weight: 300; color: #ffffff; letter-spacing: 1px;font-family:Barlow;margin:0px;">Valorizziamo le Imprese </h1>
							</div>

							<!-- LAYER NR. 18 -->
							<div class="tp-caption     rev_group" id="slide-11-layer-85" data-x="['left','left','left','left']" data-hoffset="['774','774','117','117']" data-y="['top','top','top','top']" data-voffset="['304','304','456','456']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['off','off','on','off']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":1460,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 22; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 19 -->
								<div class="tp-caption rev-btn   mcc-slider-tondo" id="slide-11-layer-87" data-x="['left','left','center','center']" data-hoffset="['-1','-1','0','0']" data-y="['top','top','middle','middle']" data-voffset="['-1','-1','0','0']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['off','off','on','off']" data-type="button" data-actions='[{"event":"click","action":"simplelink","target":"_self","url":"https:\/\/www.mcc.it\/wizard","delay":""}]' data-responsive_offset="on" data-frames='[{"delay":"+10","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(14,125,87);bg:rgb(255,255,255);bs:solid;bw:0 0 0 0;"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['center','center','center','center']" data-paddingtop="[99,99,99,99]" data-paddingright="[130,130,130,130]" data-paddingbottom="[130,130,130,130]" data-paddingleft="[42,42,42,42]" style="z-index: 23; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 24px; line-height: 28px; font-weight: 400; color: rgba(255,255,255,1); letter-spacing: px;font-family:Barlow;background-color:rgba(3,164,115,0.7);border-color:rgba(0,0,0,1);border-radius:130px 130px 130px 130px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Trova la soluzione<br />per la tua Impresa </div>
							</div>

							<!-- LAYER NR. 20 -->
							<div class="tp-caption     rev_group" id="slide-11-layer-88" data-x="['left','left','left','left']" data-hoffset="['1008','1008','404','403']" data-y="['top','top','top','top']" data-voffset="['128','128','369','370']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['off','off','on','off']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":1430,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 24; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 21 -->
								<a class="tp-caption rev-btn   mcc-slider-tondo-bianco" href="index.php" target="_self" id="slide-11-layer-89" data-x="['left','left','left','left']" data-hoffset="['-1','-1','-1','-1']" data-y="['top','top','top','top']" data-voffset="['-2','-2','-2','-2']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['off','off','on','off']" data-type="button" data-actions='' data-responsive_offset="on" data-frames='[{"delay":"+10","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgba(0,0,0,0.2);bs:solid;bw:0 0 0 0;"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['center','center','center','center']" data-paddingtop="[99,99,99,99]" data-paddingright="[42,42,42,42]" data-paddingbottom="[130,130,130,130]" data-paddingleft="[42,42,42,42]" style="z-index: 25; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 24px; line-height: 28px; font-weight: 400; color: #0e7d57; letter-spacing: px;font-family:Barlow;background-color:rgba(255,255,255,0.9);border-color:rgba(0,0,0,1);border-radius:130px 130px 130px 130px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">Scopri la nostra<br />offerta </a>
							</div>

							<!-- LAYER NR. 22 -->
							<div class="tp-caption     rev_group" id="slide-11-layer-91" data-x="['left','left','center','center']" data-hoffset="['219','219','0','0']" data-y="['top','top','middle','middle']" data-voffset="['152','152','-56','-57']" data-width="['538','538','455','454']" data-height="['117','117','92','107']" data-whitespace="nowrap" data-visibility="['off','off','off','on']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 26; min-width: 538px; max-width: 538px; max-width: 117px; max-width: 117px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 23 -->
								<h1 class="tp-caption   tp-resizeme" id="slide-11-layer-92" data-x="['left','left','center','center']" data-hoffset="['0','107','0','-2']" data-y="['top','top','top','middle']" data-voffset="['0','265','0','-12']" data-fontsize="['50','44','44','50']" data-lineheight="['68','40','40','68']" data-letterspacing="['0','1','1','-1']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['off','off','off','on']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":"+770","speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeIn"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 27; white-space: nowrap; font-size: 50px; line-height: 68px; font-weight: 500; color: #ffffff; letter-spacing: 0px;font-family:Barlow;margin:0px;">Diamo credito all'Italia </h1>

								<!-- LAYER NR. 24 -->
								<h1 class="tp-caption   tp-resizeme" id="slide-11-layer-93" data-x="['left','left','center','center']" data-hoffset="['0','107','0','0']" data-y="['top','top','top','middle']" data-voffset="['68','309','50','34']" data-fontsize="['30','24','24','34']" data-lineheight="['38','30','30','68']" data-fontweight="['300','300','500','400']" data-letterspacing="['1','1','1','0']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['off','off','off','on']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":"+860","speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeIn"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 28; white-space: nowrap; font-size: 30px; line-height: 38px; font-weight: 300; color: #ffffff; letter-spacing: 1px;font-family:Barlow;margin:0px;">Valorizziamo le Imprese </h1>
							</div>

							<!-- LAYER NR. 25 -->
							<div class="tp-caption     rev_group" id="slide-11-layer-97" data-x="['left','left','left','left']" data-hoffset="['1008','1008','419','305']" data-y="['top','top','top','top']" data-voffset="['128','128','443','421']" data-width="['260','260','200','160']" data-height="['260','260','200','160']" data-whitespace="nowrap" data-visibility="['off','off','off','on']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":1430,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 29; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 26 -->
								<a class="tp-caption rev-btn   mcc-slider-tondo-bianco" href="index.php" target="_self" id="slide-11-layer-98" data-x="['left','left','center','left']" data-hoffset="['-1','-1','0','0']" data-y="['top','top','middle','top']" data-voffset="['-2','-2','0','0']" data-fontsize="['24','24','24','18']" data-lineheight="['28','28','28','20']" data-width="['260','260','200','160']" data-height="['260','260','200','160']" data-whitespace="nowrap" data-visibility="['off','off','off','on']" data-type="button" data-actions='' data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":"+10","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['center','center','center','center']" data-paddingtop="[99,99,67,60]" data-paddingright="[42,42,0,0]" data-paddingbottom="[130,130,80,70]" data-paddingleft="[42,42,0,0]" style="z-index: 30; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 24px; line-height: 28px; font-weight: 400; color: #0e7d57; letter-spacing: px;font-family:Barlow;background-color:rgba(255,255,255,0.9);border-radius:130px 130px 130px 130px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">Scopri la nostra<br />offerta </a>
							</div>

							<!-- LAYER NR. 27 -->
							<div class="tp-caption     rev_group" id="slide-11-layer-106" data-x="['left','left','left','left']" data-hoffset="['774','774','163','78']" data-y="['top','top','top','top']" data-voffset="['304','304','481','497']" data-width="['260','260','200','160']" data-height="['260','260','200','160']" data-whitespace="nowrap" data-visibility="['off','off','off','on']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":1460,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 31; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 28 -->
								<div class="tp-caption rev-btn   mcc-slider-tondo" id="slide-11-layer-108" data-x="['left','left','center','center']" data-hoffset="['-1','-1','0','0']" data-y="['top','top','middle','middle']" data-voffset="['-1','-1','0','0']" data-fontsize="['24','24','22','18']" data-lineheight="['28','28','26','20']" data-width="['260','260','200','160']" data-height="['260','260','200','160']" data-whitespace="nowrap" data-visibility="['off','off','off','on']" data-type="button" data-actions='[{"event":"click","action":"simplelink","target":"_self","url":"https:\/\/www.mcc.it\/wizard\/","delay":""}]' data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":"+10","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['center','center','center','center']" data-paddingtop="[99,99,64,55]" data-paddingright="[130,130,0,0]" data-paddingbottom="[130,130,88,88]" data-paddingleft="[42,42,0,0]" style="z-index: 32; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 24px; line-height: 28px; font-weight: 400; color: rgba(255,255,255,1); letter-spacing: px;font-family:Barlow;background-color:rgba(3,164,115,0.7);border-radius:130px 130px 130px 130px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Trova la soluzione<br />per la tua Impresa </div>
							</div>
						</li>
						<!-- SLIDE  -->
						<li data-index="rs-12" data-transition="slidehorizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-rotate="0" data-saveperformance="off" data-title="Slide-2" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
							<!-- MAIN IMAGE -->
							<img src="wp-content/uploads/2019/02/slide2.jpg" alt="Centennial Bank - La Banca per le Imprese" title="Centennial Bank &#8211; La Banca per le Imprese" width="1920" height="1282" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
							<!-- LAYERS -->

							<!-- LAYER NR. 29 -->
							<div class="tp-caption     rev_group" id="slide-12-layer-81" data-x="['left','left','left','left']" data-hoffset="['102','100','100','100']" data-y="['top','middle','middle','middle']" data-voffset="['296','0','0','0']" data-width="538" data-height="117" data-whitespace="nowrap" data-visibility="['on','off','off','off']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; min-width: 538px; max-width: 538px; max-width: 117px; max-width: 117px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 30 -->
								<h1 class="tp-caption   tp-resizeme" id="slide-12-layer-1" data-x="['left','left','left','center']" data-hoffset="['0','0','128','128']" data-y="['top','middle','top','middle']" data-voffset="['0','0','358','358']" data-fontsize="['50','44','44','50']" data-lineheight="['68','40','40','68']" data-letterspacing="['0','1','1','-2']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['on','off','off','off']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":"+770","speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeIn"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 50px; line-height: 68px; font-weight: 500; color: #ffffff; letter-spacing: 0px;font-family:Barlow;margin:0px;">La banca per le Imprese </h1>

								<!-- LAYER NR. 31 -->
								<h1 class="tp-caption   tp-resizeme" id="slide-12-layer-23" data-x="['left','left','left','center']" data-hoffset="['0','0','127','127']" data-y="['bottom','bottom','top','middle']" data-voffset="['11','0','405','405']" data-fontsize="['30','24','24','34']" data-lineheight="['38','30','30','68']" data-letterspacing="['1','1','1','-2']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['on','off','off','off']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":"+860","speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeIn"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; font-size: 30px; line-height: 38px; font-weight: 300; color: #ffffff; letter-spacing: 1px;font-family:Barlow;margin:0px;">Creare, Crescere, Innovare </h1>
							</div>

							<!-- LAYER NR. 32 -->
							<div class="tp-caption     rev_group" id="slide-12-layer-76" data-x="['left','center','left','left']" data-hoffset="['767','150','774','775']" data-y="['bottom','bottom','top','top']" data-voffset="['76','0','304','304']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['on','off','off','off']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":1460,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 8; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 33 -->
								<div class="tp-caption rev-btn   mcc-slider-tondo" id="slide-12-layer-74" data-x="['left','left','left','left']" data-hoffset="['-1','-1','-1','-1']" data-y="['top','top','top','top']" data-voffset="['-1','-1','-1','-1']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['on','off','off','off']" data-type="button" data-actions='[{"event":"click","action":"simplelink","target":"_self","url":"https:\/\/www.mcc.it\/wizard","delay":""}]' data-responsive_offset="on" data-frames='[{"delay":"+10","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(14,125,87);bg:rgb(255,255,255);bs:solid;bw:0 0 0 0;"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['center','center','center','center']" data-paddingtop="[99,99,99,99]" data-paddingright="[130,130,130,130]" data-paddingbottom="[130,130,130,130]" data-paddingleft="[42,42,42,42]" style="z-index: 9; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 24px; line-height: 28px; font-weight: 400; color: rgba(255,255,255,1); letter-spacing: px;font-family:Barlow;background-color:rgba(3,164,115,0.7);border-color:rgba(0,0,0,1);border-radius:130px 130px 130px 130px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Trova la soluzione<br />per la tua impresa </div>
							</div>

							<!-- LAYER NR. 34 -->
							<div class="tp-caption     rev_group" id="slide-12-layer-77" data-x="['left','center','left','left']" data-hoffset="['990','400','1008','1008']" data-y="['top','middle','top','top']" data-voffset="['144','0','128','128']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['on','off','off','off']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":1430,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 10; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 35 -->
								<a class="tp-caption rev-btn   mcc-slider-tondo-bianco" href="index.php" target="_self" id="slide-12-layer-79" data-x="['left','left','left','left']" data-hoffset="['-1','-1','-1','-1']" data-y="['top','top','top','top']" data-voffset="['-2','-2','-2','-2']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['on','off','off','off']" data-type="button" data-actions='' data-responsive_offset="on" data-frames='[{"delay":"+10","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgba(0,0,0,0.4);bs:solid;bw:0 0 0 0;"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['center','center','center','center']" data-paddingtop="[99,99,99,99]" data-paddingright="[42,42,42,42]" data-paddingbottom="[130,130,130,130]" data-paddingleft="[42,42,42,42]" style="z-index: 11; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 24px; line-height: 28px; font-weight: 400; color: #0e7d57; letter-spacing: px;font-family:Barlow;background-color:rgba(255,255,255,0.9);border-color:rgba(0,0,0,1);border-radius:130px 130px 130px 130px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">Scopri la nostra<br />offerta </a>
							</div>

							<!-- LAYER NR. 36 -->
							<div class="tp-caption     rev_group" id="slide-12-layer-118" data-x="['left','left','left','left']" data-hoffset="['107','78','78','78']" data-y="['top','middle','middle','middle']" data-voffset="['301','16','16','16']" data-width="538" data-height="117" data-whitespace="nowrap" data-visibility="['off','on','off','off']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 12; min-width: 538px; max-width: 538px; max-width: 117px; max-width: 117px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 37 -->
								<h1 class="tp-caption  " id="slide-12-layer-119" data-x="['left','left','left','center']" data-hoffset="['0','0','128','128']" data-y="['top','middle','top','middle']" data-voffset="['0','0','358','358']" data-fontsize="['50','44','44','50']" data-lineheight="['68','40','40','68']" data-letterspacing="['0','1','1','-2']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['off','on','off','off']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":"+770","speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeIn"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 13; white-space: nowrap; font-size: 50px; line-height: 68px; font-weight: 500; color: #ffffff; letter-spacing: 0px;font-family:Barlow;margin:0px;">La banca per le Imprese </h1>

								<!-- LAYER NR. 38 -->
								<h1 class="tp-caption  " id="slide-12-layer-120" data-x="['left','left','left','center']" data-hoffset="['0','0','127','127']" data-y="['bottom','bottom','top','middle']" data-voffset="['11','0','405','405']" data-fontsize="['30','24','24','34']" data-lineheight="['38','30','30','68']" data-letterspacing="['1','1','1','-2']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['off','on','off','off']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":"+860","speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeIn"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 14; white-space: nowrap; font-size: 30px; line-height: 38px; font-weight: 300; color: #ffffff; letter-spacing: 1px;font-family:Barlow;margin:0px;">Creare, Crescere, Innovare </h1>
							</div>

							<!-- LAYER NR. 39 -->
							<div class="tp-caption     rev_group" id="slide-12-layer-112" data-x="['left','center','left','left']" data-hoffset="['779','130','774','775']" data-y="['top','bottom','top','top']" data-voffset="['308','75','304','304']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['off','on','off','off']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":1460,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 15; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 40 -->
								<div class="tp-caption rev-btn   mcc-slider-tondo" id="slide-12-layer-113" data-x="['left','left','left','left']" data-hoffset="['-1','-1','-1','-1']" data-y="['top','top','top','top']" data-voffset="['-1','-1','-1','-1']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['off','on','off','off']" data-type="button" data-actions='[{"event":"click","action":"simplelink","target":"_self","url":"https:\/\/www.mcc.it\/wizard","delay":""}]' data-responsive_offset="on" data-frames='[{"delay":"+10","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(14,125,87);bg:rgb(255,255,255);bs:solid;bw:0 0 0 0;"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['center','center','center','center']" data-paddingtop="[99,99,99,99]" data-paddingright="[130,130,130,130]" data-paddingbottom="[130,130,130,130]" data-paddingleft="[42,42,42,42]" style="z-index: 16; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 24px; line-height: 28px; font-weight: 400; color: rgba(255,255,255,1); letter-spacing: px;font-family:Barlow;background-color:rgba(3,164,115,0.7);border-color:rgba(0,0,0,1);border-radius:130px 130px 130px 130px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Trova la soluzione<br />per la tua impresa </div>
							</div>

							<!-- LAYER NR. 41 -->
							<div class="tp-caption     rev_group" id="slide-12-layer-115" data-x="['left','center','left','left']" data-hoffset="['1013','391','1008','1008']" data-y="['top','middle','top','top']" data-voffset="['133','-20','128','128']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['off','on','off','off']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":1430,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 17; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 42 -->
								<a class="tp-caption rev-btn   mcc-slider-tondo-bianco" href="index.php" target="_self" id="slide-12-layer-116" data-x="['left','left','left','left']" data-hoffset="['-1','-1','-1','-1']" data-y="['top','top','top','top']" data-voffset="['-2','-2','-2','-2']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['off','on','off','off']" data-type="button" data-actions='' data-responsive_offset="on" data-frames='[{"delay":"+10","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgba(0,0,0,0.2);bs:solid;bw:0 0 0 0;"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['center','center','center','center']" data-paddingtop="[99,99,99,99]" data-paddingright="[42,42,42,42]" data-paddingbottom="[130,130,130,130]" data-paddingleft="[42,42,42,42]" style="z-index: 18; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 24px; line-height: 28px; font-weight: 400; color: #0e7d57; letter-spacing: px;font-family:Barlow;background-color:rgba(255,255,255,0.9);border-color:rgba(0,0,0,1);border-radius:130px 130px 130px 130px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">Scopri la nostra<br />offerta </a>
							</div>

							<!-- LAYER NR. 43 -->
							<div class="tp-caption     rev_group" id="slide-12-layer-82" data-x="['left','left','center','center']" data-hoffset="['219','219','0','0']" data-y="['top','top','middle','middle']" data-voffset="['152','152','-132','-133']" data-width="['538','538','455','550']" data-height="['117','117','92','92']" data-whitespace="nowrap" data-visibility="['off','off','on','off']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 19; min-width: 538px; max-width: 538px; max-width: 117px; max-width: 117px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 44 -->
								<h1 class="tp-caption   tp-resizeme" id="slide-12-layer-83" data-x="['left','left','center','center']" data-hoffset="['0','107','0','0']" data-y="['top','top','top','middle']" data-voffset="['0','265','0','-96']" data-fontsize="['50','44','44','50']" data-lineheight="['68','40','40','68']" data-letterspacing="['0','1','1','-2']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['off','off','on','off']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":"+770","speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeIn"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 20; white-space: nowrap; font-size: 50px; line-height: 68px; font-weight: 500; color: #ffffff; letter-spacing: 0px;font-family:Barlow;margin:0px;">La Banca per le Imprese </h1>

								<!-- LAYER NR. 45 -->
								<h1 class="tp-caption   tp-resizeme" id="slide-12-layer-84" data-x="['left','left','left','center']" data-hoffset="['0','107','81','0']" data-y="['top','top','top','middle']" data-voffset="['68','309','50','-49']" data-fontsize="['30','24','24','34']" data-lineheight="['38','30','30','68']" data-letterspacing="['1','1','1','-2']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['off','off','on','off']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":"+860","speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeIn"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 21; white-space: nowrap; font-size: 30px; line-height: 38px; font-weight: 300; color: #ffffff; letter-spacing: 1px;font-family:Barlow;margin:0px;">Creare, Crescere, Innovare </h1>
							</div>

							<!-- LAYER NR. 46 -->
							<div class="tp-caption     rev_group" id="slide-12-layer-85" data-x="['left','left','left','left']" data-hoffset="['774','774','117','117']" data-y="['top','top','top','top']" data-voffset="['304','304','456','456']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['off','off','on','off']" data-type="group" data-actions='[{"event":"click","action":"simplelink","target":"_self","url":"http:\/\/swwwfu032\/wizard","delay":""}]' data-responsive_offset="on" data-frames='[{"delay":1460,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 22; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 47 -->
								<div class="tp-caption rev-btn   mcc-slider-tondo" id="slide-12-layer-87" data-x="['left','left','center','center']" data-hoffset="['-1','-1','0','0']" data-y="['top','top','middle','middle']" data-voffset="['-1','-1','0','0']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['off','off','on','off']" data-type="button" data-actions='[{"event":"click","action":"simplelink","target":"_self","url":"https:\/\/www.mcc.it\/wizard","delay":""}]' data-responsive_offset="on" data-frames='[{"delay":"+10","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(14,125,87);bg:rgb(255,255,255);bs:solid;bw:0 0 0 0;"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['center','center','center','center']" data-paddingtop="[99,99,99,99]" data-paddingright="[130,130,130,130]" data-paddingbottom="[130,130,130,130]" data-paddingleft="[42,42,42,42]" style="z-index: 23; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 24px; line-height: 28px; font-weight: 400; color: rgba(255,255,255,1); letter-spacing: px;font-family:Barlow;background-color:rgba(3,164,115,0.7);border-color:rgba(0,0,0,1);border-radius:130px 130px 130px 130px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Trova la soluzione<br />per la tua impresa </div>
							</div>

							<!-- LAYER NR. 48 -->
							<div class="tp-caption     rev_group" id="slide-12-layer-88" data-x="['left','left','left','left']" data-hoffset="['1008','1008','404','403']" data-y="['top','top','top','top']" data-voffset="['128','128','369','370']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['off','off','on','off']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":1430,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 24; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 49 -->
								<a class="tp-caption rev-btn   mcc-slider-tondo-bianco" href="index.php" target="_self" id="slide-12-layer-89" data-x="['left','left','left','left']" data-hoffset="['-1','-1','-1','-1']" data-y="['top','top','top','top']" data-voffset="['-2','-2','-2','-2']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['off','off','on','off']" data-type="button" data-actions='' data-responsive_offset="on" data-frames='[{"delay":"+10","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgba(0,0,0,0.2);bs:solid;bw:0 0 0 0;"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['center','center','center','center']" data-paddingtop="[99,99,99,99]" data-paddingright="[42,42,42,42]" data-paddingbottom="[130,130,130,130]" data-paddingleft="[42,42,42,42]" style="z-index: 25; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 24px; line-height: 28px; font-weight: 400; color: #0e7d57; letter-spacing: px;font-family:Barlow;background-color:rgba(255,255,255,0.9);border-color:rgba(0,0,0,1);border-radius:130px 130px 130px 130px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">Scopri la nostra<br />offerta </a>
							</div>

							<!-- LAYER NR. 50 -->
							<div class="tp-caption     rev_group" id="slide-12-layer-91" data-x="['left','left','center','center']" data-hoffset="['219','219','0','0']" data-y="['top','top','middle','middle']" data-voffset="['152','152','-56','-57']" data-width="['538','538','455','454']" data-height="['117','117','92','107']" data-whitespace="nowrap" data-visibility="['off','off','off','on']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 26; min-width: 538px; max-width: 538px; max-width: 117px; max-width: 117px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 51 -->
								<h1 class="tp-caption   tp-resizeme" id="slide-12-layer-92" data-x="['left','left','center','center']" data-hoffset="['0','107','0','-2']" data-y="['top','top','top','middle']" data-voffset="['0','265','0','-12']" data-fontsize="['50','44','44','50']" data-lineheight="['68','40','40','68']" data-letterspacing="['0','1','1','-1']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['off','off','off','on']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":"+770","speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeIn"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 27; white-space: nowrap; font-size: 50px; line-height: 68px; font-weight: 500; color: #ffffff; letter-spacing: 0px;font-family:Barlow;margin:0px;">La banca per le Imprese </h1>

								<!-- LAYER NR. 52 -->
								<h1 class="tp-caption   tp-resizeme" id="slide-12-layer-93" data-x="['left','left','center','center']" data-hoffset="['0','107','0','0']" data-y="['top','top','top','middle']" data-voffset="['68','309','50','34']" data-fontsize="['30','24','24','34']" data-lineheight="['38','30','30','68']" data-fontweight="['300','300','500','400']" data-letterspacing="['1','1','1','0']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['off','off','off','on']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":"+860","speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeIn"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 28; white-space: nowrap; font-size: 30px; line-height: 38px; font-weight: 300; color: #ffffff; letter-spacing: 1px;font-family:Barlow;margin:0px;">Creare, Crescere, Innovare </h1>
							</div>

							<!-- LAYER NR. 53 -->
							<div class="tp-caption     rev_group" id="slide-12-layer-97" data-x="['left','left','left','left']" data-hoffset="['1008','1008','419','305']" data-y="['top','top','top','top']" data-voffset="['128','128','443','421']" data-width="['260','260','200','160']" data-height="['260','260','200','160']" data-whitespace="nowrap" data-visibility="['off','off','off','on']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":1430,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 29; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 54 -->
								<a class="tp-caption rev-btn   mcc-slider-tondo-bianco" href="index.php" target="_self" id="slide-12-layer-98" data-x="['left','left','center','left']" data-hoffset="['-1','-1','0','0']" data-y="['top','top','middle','top']" data-voffset="['-2','-2','0','0']" data-fontsize="['24','24','24','18']" data-lineheight="['28','28','28','20']" data-width="['260','260','200','160']" data-height="['260','260','200','160']" data-whitespace="nowrap" data-visibility="['off','off','off','on']" data-type="button" data-actions='' data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":"+10","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['center','center','center','center']" data-paddingtop="[99,99,67,60]" data-paddingright="[42,42,0,0]" data-paddingbottom="[130,130,80,70]" data-paddingleft="[42,42,0,0]" style="z-index: 30; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 24px; line-height: 28px; font-weight: 400; color: #0e7d57; letter-spacing: px;font-family:Barlow;background-color:rgba(255,255,255,0.9);border-radius:130px 130px 130px 130px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">Scopri la nostra<br />offerta </a>
							</div>

							<!-- LAYER NR. 55 -->
							<div class="tp-caption     rev_group" id="slide-12-layer-106" data-x="['left','left','left','left']" data-hoffset="['774','774','163','78']" data-y="['top','top','top','top']" data-voffset="['304','304','481','497']" data-width="['260','260','200','160']" data-height="['260','260','200','160']" data-whitespace="nowrap" data-visibility="['off','off','off','on']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":1460,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 31; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 56 -->
								<div class="tp-caption rev-btn   mcc-slider-tondo" id="slide-12-layer-108" data-x="['left','left','center','center']" data-hoffset="['-1','-1','0','0']" data-y="['top','top','middle','middle']" data-voffset="['-1','-1','0','0']" data-fontsize="['24','24','22','18']" data-lineheight="['28','28','26','20']" data-width="['260','260','200','160']" data-height="['260','260','200','160']" data-whitespace="nowrap" data-visibility="['off','off','off','on']" data-type="button" data-actions='[{"event":"click","action":"simplelink","target":"_self","url":"https:\/\/www.mcc.it\/wizard\/","delay":""}]' data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":"+10","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['center','center','center','center']" data-paddingtop="[99,99,64,55]" data-paddingright="[130,130,0,0]" data-paddingbottom="[130,130,88,88]" data-paddingleft="[42,42,0,0]" style="z-index: 32; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 24px; line-height: 28px; font-weight: 400; color: rgba(255,255,255,1); letter-spacing: px;font-family:Barlow;background-color:rgba(3,164,115,0.7);border-radius:130px 130px 130px 130px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Trova la soluzione<br />per la tua impresa </div>
							</div>
						</li>
						<!-- SLIDE  -->
						<li data-index="rs-13" data-transition="slidehorizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-rotate="0" data-saveperformance="off" data-title="Slide-3" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
							<!-- MAIN IMAGE -->
							<img src="wp-content/uploads/revslider/Nuovo-slider/slide-3.jpg" alt="Centennial Bank - Soluzioni per lo sviluppo" title="Centennial Bank &#8211; Soluzioni per lo sviluppo" width="1920" height="1280" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
							<!-- LAYERS -->

							<!-- LAYER NR. 57 -->
							<div class="tp-caption     rev_group" id="slide-13-layer-81" data-x="['left','left','left','left']" data-hoffset="['102','100','100','100']" data-y="['top','middle','middle','middle']" data-voffset="['296','0','0','0']" data-width="538" data-height="117" data-whitespace="nowrap" data-visibility="['on','off','off','off']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; min-width: 538px; max-width: 538px; max-width: 117px; max-width: 117px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 58 -->
								<h1 class="tp-caption   tp-resizeme" id="slide-13-layer-1" data-x="['left','left','left','center']" data-hoffset="['0','0','128','128']" data-y="['top','middle','top','middle']" data-voffset="['0','0','358','358']" data-fontsize="['50','44','44','50']" data-lineheight="['68','40','40','68']" data-letterspacing="['0','1','1','-2']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['on','off','off','off']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":"+770","speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeIn"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 50px; line-height: 68px; font-weight: 500; color: #ffffff; letter-spacing: 0px;font-family:Barlow;margin:0px;">Soluzioni per lo sviluppo </h1>

								<!-- LAYER NR. 59 -->
								<h1 class="tp-caption   tp-resizeme" id="slide-13-layer-23" data-x="['left','left','left','center']" data-hoffset="['0','0','127','127']" data-y="['bottom','bottom','top','middle']" data-voffset="['11','0','405','405']" data-fontsize="['30','24','24','34']" data-lineheight="['38','30','30','68']" data-letterspacing="['1','1','1','-2']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['on','off','off','off']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":"+860","speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeIn"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; font-size: 30px; line-height: 38px; font-weight: 300; color: #ffffff; letter-spacing: 1px;font-family:Barlow;margin:0px;">Finanziamenti, Agevolazioni, Servizi </h1>
							</div>

							<!-- LAYER NR. 60 -->
							<div class="tp-caption     rev_group" id="slide-13-layer-76" data-x="['left','center','left','left']" data-hoffset="['767','150','774','775']" data-y="['bottom','bottom','top','top']" data-voffset="['76','0','304','304']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['on','off','off','off']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":1460,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 8; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 61 -->
								<div class="tp-caption rev-btn   mcc-slider-tondo" id="slide-13-layer-74" data-x="['left','left','left','left']" data-hoffset="['-1','-1','-1','-1']" data-y="['top','top','top','top']" data-voffset="['-1','-1','-1','-1']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['on','off','off','off']" data-type="button" data-actions='[{"event":"click","action":"simplelink","target":"_self","url":"https:\/\/www.mcc.it\/wizard","delay":""}]' data-responsive_offset="on" data-frames='[{"delay":"+10","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(14,125,87);bg:rgb(255,255,255);bs:solid;bw:0 0 0 0;"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['center','center','center','center']" data-paddingtop="[99,99,99,99]" data-paddingright="[130,130,130,130]" data-paddingbottom="[130,130,130,130]" data-paddingleft="[42,42,42,42]" style="z-index: 9; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 24px; line-height: 28px; font-weight: 400; color: rgba(255,255,255,1); letter-spacing: px;font-family:Barlow;background-color:rgba(3,164,115,0.7);border-color:rgba(0,0,0,1);border-radius:130px 130px 130px 130px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Trova la soluzione<br />per la tua impresa </div>
							</div>

							<!-- LAYER NR. 62 -->
							<div class="tp-caption     rev_group" id="slide-13-layer-77" data-x="['left','center','left','left']" data-hoffset="['990','400','1008','1008']" data-y="['top','middle','top','top']" data-voffset="['144','0','128','128']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['on','off','off','off']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":1430,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 10; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 63 -->
								<a class="tp-caption rev-btn   mcc-slider-tondo-bianco" href="index.php" target="_self" id="slide-13-layer-79" data-x="['left','left','left','left']" data-hoffset="['-1','-1','-1','-1']" data-y="['top','top','top','top']" data-voffset="['-2','-2','-2','-2']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['on','off','off','off']" data-type="button" data-actions='' data-responsive_offset="on" data-frames='[{"delay":"+10","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgba(0,0,0,0.2);bs:solid;bw:0 0 0 0;"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['center','center','center','center']" data-paddingtop="[99,99,99,99]" data-paddingright="[42,42,42,42]" data-paddingbottom="[130,130,130,130]" data-paddingleft="[42,42,42,42]" style="z-index: 11; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 24px; line-height: 28px; font-weight: 400; color: #0e7d57; letter-spacing: px;font-family:Barlow;background-color:rgba(255,255,255,0.9);border-color:rgba(0,0,0,1);border-radius:130px 130px 130px 130px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">Scopri la nostra<br />offerta </a>
							</div>

							<!-- LAYER NR. 64 -->
							<div class="tp-caption     rev_group" id="slide-13-layer-118" data-x="['left','left','left','left']" data-hoffset="['107','78','78','78']" data-y="['top','middle','middle','middle']" data-voffset="['301','16','16','16']" data-width="538" data-height="117" data-whitespace="nowrap" data-visibility="['off','on','off','off']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 12; min-width: 538px; max-width: 538px; max-width: 117px; max-width: 117px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 65 -->
								<h1 class="tp-caption  " id="slide-13-layer-119" data-x="['left','left','left','center']" data-hoffset="['0','0','128','128']" data-y="['top','middle','top','middle']" data-voffset="['0','0','358','358']" data-fontsize="['50','44','44','50']" data-lineheight="['68','40','40','68']" data-letterspacing="['0','1','1','-2']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['off','on','off','off']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":"+770","speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeIn"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 13; white-space: nowrap; font-size: 50px; line-height: 68px; font-weight: 500; color: #ffffff; letter-spacing: 0px;font-family:Barlow;margin:0px;">Soluzioni per lo sviluppo </h1>

								<!-- LAYER NR. 66 -->
								<h1 class="tp-caption  " id="slide-13-layer-120" data-x="['left','left','left','center']" data-hoffset="['0','0','127','127']" data-y="['bottom','bottom','top','middle']" data-voffset="['11','0','405','405']" data-fontsize="['30','24','24','34']" data-lineheight="['38','30','30','68']" data-letterspacing="['1','1','1','-2']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['off','on','off','off']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":"+860","speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeIn"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 14; white-space: nowrap; font-size: 30px; line-height: 38px; font-weight: 300; color: #ffffff; letter-spacing: 1px;font-family:Barlow;margin:0px;">Finanziamenti, Agevolazioni, Servizi </h1>
							</div>

							<!-- LAYER NR. 67 -->
							<div class="tp-caption     rev_group" id="slide-13-layer-112" data-x="['left','center','left','left']" data-hoffset="['779','130','774','775']" data-y="['top','bottom','top','top']" data-voffset="['308','75','304','304']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['off','on','off','off']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":1460,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 15; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 68 -->
								<div class="tp-caption rev-btn   mcc-slider-tondo" id="slide-13-layer-113" data-x="['left','left','left','left']" data-hoffset="['-1','-1','-1','-1']" data-y="['top','top','top','top']" data-voffset="['-1','-1','-1','-1']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['off','on','off','off']" data-type="button" data-actions='[{"event":"click","action":"simplelink","target":"_self","url":"https:\/\/www.mcc.it\/wizard","delay":""}]' data-responsive_offset="on" data-frames='[{"delay":"+10","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0,0,0,1);bg:rgb(255,255,255);bs:solid;bw:0 0 0 0;"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['center','center','center','center']" data-paddingtop="[99,99,99,99]" data-paddingright="[130,130,130,130]" data-paddingbottom="[130,130,130,130]" data-paddingleft="[42,42,42,42]" style="z-index: 16; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 24px; line-height: 28px; font-weight: 400; color: rgba(255,255,255,1); letter-spacing: px;font-family:Barlow;background-color:rgba(3,164,115,0.7);border-color:rgba(0,0,0,1);border-radius:130px 130px 130px 130px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Trova la soluzione<br />per la tua impresa </div>
							</div>

							<!-- LAYER NR. 69 -->
							<div class="tp-caption     rev_group" id="slide-13-layer-115" data-x="['left','center','left','left']" data-hoffset="['1013','391','1008','1008']" data-y="['top','middle','top','top']" data-voffset="['133','-20','128','128']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['off','on','off','off']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":1430,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 17; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 70 -->
								<a class="tp-caption rev-btn   mcc-slider-tondo-bianco" href="index.php" target="_self" id="slide-13-layer-116" data-x="['left','left','left','left']" data-hoffset="['-1','-1','-1','-1']" data-y="['top','top','top','top']" data-voffset="['-2','-2','-2','-2']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['off','on','off','off']" data-type="button" data-actions='' data-responsive_offset="on" data-frames='[{"delay":"+10","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgba(0,0,0,0.2);bs:solid;bw:0 0 0 0;"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['center','center','center','center']" data-paddingtop="[99,99,99,99]" data-paddingright="[42,42,42,42]" data-paddingbottom="[130,130,130,130]" data-paddingleft="[42,42,42,42]" style="z-index: 18; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 24px; line-height: 28px; font-weight: 400; color: #0e7d57; letter-spacing: px;font-family:Barlow;background-color:rgba(255,255,255,0.9);border-color:rgba(0,0,0,1);border-radius:130px 130px 130px 130px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">Scopri la nostra<br />offerta </a>
							</div>

							<!-- LAYER NR. 71 -->
							<div class="tp-caption     rev_group" id="slide-13-layer-82" data-x="['left','left','center','center']" data-hoffset="['219','219','0','0']" data-y="['top','top','middle','middle']" data-voffset="['152','152','-132','-132']" data-width="['538','538','455','455']" data-height="['117','117','92','92']" data-whitespace="nowrap" data-visibility="['off','off','on','off']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 19; min-width: 538px; max-width: 538px; max-width: 117px; max-width: 117px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 72 -->
								<h1 class="tp-caption   tp-resizeme" id="slide-13-layer-83" data-x="['left','left','center','center']" data-hoffset="['0','107','0','0']" data-y="['top','top','top','middle']" data-voffset="['0','265','0','-96']" data-fontsize="['50','44','44','50']" data-lineheight="['68','40','40','68']" data-letterspacing="['0','1','1','-2']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['off','off','on','off']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":"+770","speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeIn"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 20; white-space: nowrap; font-size: 50px; line-height: 68px; font-weight: 500; color: #ffffff; letter-spacing: 0px;font-family:Barlow;margin:0px;">Soluzioni per lo sviluppo </h1>

								<!-- LAYER NR. 73 -->
								<h1 class="tp-caption   tp-resizeme" id="slide-13-layer-84" data-x="['left','left','center','center']" data-hoffset="['0','107','0','0']" data-y="['top','top','top','middle']" data-voffset="['68','309','50','-49']" data-fontsize="['30','24','24','34']" data-lineheight="['38','30','30','68']" data-letterspacing="['1','1','1','-2']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['off','off','on','off']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":"+860","speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeIn"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 21; white-space: nowrap; font-size: 30px; line-height: 38px; font-weight: 300; color: #ffffff; letter-spacing: 1px;font-family:Barlow;margin:0px;">Finanziamenti, Agevolazioni, Servizi </h1>
							</div>

							<!-- LAYER NR. 74 -->
							<div class="tp-caption     rev_group" id="slide-13-layer-85" data-x="['left','left','left','left']" data-hoffset="['774','774','117','117']" data-y="['top','top','top','top']" data-voffset="['304','304','456','456']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['off','off','on','off']" data-type="group" data-actions='[{"event":"click","action":"simplelink","target":"_self","url":"http:\/\/swwwfu032\/wizard","delay":""}]' data-responsive_offset="on" data-frames='[{"delay":1460,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 22; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 75 -->
								<div class="tp-caption rev-btn   mcc-slider-tondo" id="slide-13-layer-87" data-x="['left','left','center','center']" data-hoffset="['-1','-1','0','0']" data-y="['top','top','middle','middle']" data-voffset="['-1','-1','0','0']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['off','off','on','off']" data-type="button" data-actions='[{"event":"click","action":"simplelink","target":"_self","url":"https:\/\/www.mcc.it\/wizard","delay":""}]' data-responsive_offset="on" data-frames='[{"delay":"+10","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0,0,0,1);bg:rgb(255,255,255);bs:solid;bw:0 0 0 0;"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['center','center','center','center']" data-paddingtop="[99,99,99,99]" data-paddingright="[130,130,130,130]" data-paddingbottom="[130,130,130,130]" data-paddingleft="[42,42,42,42]" style="z-index: 23; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 24px; line-height: 28px; font-weight: 400; color: rgba(255,255,255,1); letter-spacing: px;font-family:Barlow;background-color:rgba(3,164,115,0.7);border-color:rgba(0,0,0,1);border-radius:130px 130px 130px 130px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Trova la soluzione<br />per la tua impresa </div>
							</div>

							<!-- LAYER NR. 76 -->
							<div class="tp-caption     rev_group" id="slide-13-layer-88" data-x="['left','left','left','left']" data-hoffset="['1008','1008','404','403']" data-y="['top','top','top','top']" data-voffset="['128','128','369','370']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['off','off','on','off']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":1430,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 24; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 77 -->
								<a class="tp-caption rev-btn   mcc-slider-tondo-bianco" href="index.php" target="_self" id="slide-13-layer-89" data-x="['left','left','left','left']" data-hoffset="['-1','-1','-1','-1']" data-y="['top','top','top','top']" data-voffset="['-2','-2','-2','-2']" data-width="260" data-height="260" data-whitespace="nowrap" data-visibility="['off','off','on','off']" data-type="button" data-actions='' data-responsive_offset="on" data-frames='[{"delay":"+10","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgba(0,0,0,0.2);bs:solid;bw:0 0 0 0;"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['center','center','center','center']" data-paddingtop="[99,99,99,99]" data-paddingright="[42,42,42,42]" data-paddingbottom="[130,130,130,130]" data-paddingleft="[42,42,42,42]" style="z-index: 25; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 24px; line-height: 28px; font-weight: 400; color: #0e7d57; letter-spacing: px;font-family:Barlow;background-color:rgba(255,255,255,0.9);border-color:rgba(0,0,0,1);border-radius:130px 130px 130px 130px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">Scopri la nostra<br />offerta </a>
							</div>

							<!-- LAYER NR. 78 -->
							<div class="tp-caption     rev_group" id="slide-13-layer-91" data-x="['left','left','center','center']" data-hoffset="['219','219','0','0']" data-y="['top','top','middle','middle']" data-voffset="['152','152','-56','-80']" data-width="['538','538','455','550']" data-height="['117','117','92','107']" data-whitespace="nowrap" data-visibility="['off','off','off','on']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 26; min-width: 538px; max-width: 538px; max-width: 117px; max-width: 117px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 79 -->
								<h1 class="tp-caption   tp-resizeme" id="slide-13-layer-92" data-x="['left','left','center','center']" data-hoffset="['0','107','0','-2']" data-y="['top','top','top','middle']" data-voffset="['0','265','0','-12']" data-fontsize="['50','44','44','50']" data-lineheight="['68','40','40','68']" data-letterspacing="['0','1','1','-1']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['off','off','off','on']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":"+770","speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeIn"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 27; white-space: nowrap; font-size: 50px; line-height: 68px; font-weight: 500; color: #ffffff; letter-spacing: 0px;font-family:Barlow;margin:0px;">Soluzioni per lo sviluppo </h1>

								<!-- LAYER NR. 80 -->
								<h1 class="tp-caption   tp-resizeme" id="slide-13-layer-93" data-x="['left','left','center','center']" data-hoffset="['0','107','0','0']" data-y="['top','top','top','middle']" data-voffset="['68','309','50','34']" data-fontsize="['30','24','24','34']" data-lineheight="['38','30','30','68']" data-fontweight="['300','300','500','400']" data-letterspacing="['1','1','1','0']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['off','off','off','on']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":"+860","speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeIn"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 28; white-space: nowrap; font-size: 30px; line-height: 38px; font-weight: 300; color: #ffffff; letter-spacing: 1px;font-family:Barlow;margin:0px;">Finanziamenti, Agevolazioni, Servizi </h1>
							</div>

							<!-- LAYER NR. 81 -->
							<div class="tp-caption     rev_group" id="slide-13-layer-97" data-x="['left','left','left','left']" data-hoffset="['1008','1008','419','305']" data-y="['top','top','top','top']" data-voffset="['128','128','443','421']" data-width="['260','260','200','160']" data-height="['260','260','200','160']" data-whitespace="nowrap" data-visibility="['off','off','off','on']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":1430,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 29; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 82 -->
								<a class="tp-caption rev-btn   mcc-slider-tondo-bianco" href="index.php" target="_self" id="slide-13-layer-98" data-x="['left','left','center','left']" data-hoffset="['-1','-1','0','0']" data-y="['top','top','middle','top']" data-voffset="['-2','-2','0','0']" data-fontsize="['24','24','24','18']" data-lineheight="['28','28','28','20']" data-width="['260','260','200','160']" data-height="['260','260','200','160']" data-whitespace="nowrap" data-visibility="['off','off','off','on']" data-type="button" data-actions='' data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":"+10","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['center','center','center','center']" data-paddingtop="[99,99,67,60]" data-paddingright="[42,42,0,0]" data-paddingbottom="[130,130,80,70]" data-paddingleft="[42,42,0,0]" style="z-index: 30; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 24px; line-height: 28px; font-weight: 400; color: #0e7d57; letter-spacing: px;font-family:Barlow;background-color:rgba(255,255,255,0.9);border-radius:130px 130px 130px 130px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">Scopri la nostra<br />offerta </a>
							</div>

							<!-- LAYER NR. 83 -->
							<div class="tp-caption     rev_group" id="slide-13-layer-106" data-x="['left','left','left','left']" data-hoffset="['774','774','163','78']" data-y="['top','top','top','top']" data-voffset="['304','304','481','497']" data-width="['260','260','200','160']" data-height="['260','260','200','160']" data-whitespace="nowrap" data-visibility="['off','off','off','on']" data-type="group" data-responsive_offset="on" data-frames='[{"delay":1460,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 31; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
								<!-- LAYER NR. 84 -->
								<div class="tp-caption rev-btn   mcc-slider-tondo" id="slide-13-layer-108" data-x="['left','left','center','center']" data-hoffset="['-1','-1','0','0']" data-y="['top','top','middle','middle']" data-voffset="['-1','-1','0','0']" data-fontsize="['24','24','22','18']" data-lineheight="['28','28','26','20']" data-width="['260','260','200','160']" data-height="['260','260','200','160']" data-whitespace="nowrap" data-visibility="['off','off','off','on']" data-type="button" data-actions='[{"event":"click","action":"simplelink","target":"_self","url":"https:\/\/www.mcc.it\/wizard\/","delay":""}]' data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":"+10","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['center','center','center','center']" data-paddingtop="[99,99,64,55]" data-paddingright="[130,130,0,0]" data-paddingbottom="[130,130,88,88]" data-paddingleft="[42,42,0,0]" style="z-index: 32; min-width: 260px; max-width: 260px; max-width: 260px; max-width: 260px; white-space: nowrap; font-size: 24px; line-height: 28px; font-weight: 400; color: rgba(255,255,255,1); letter-spacing: px;font-family:Barlow;background-color:rgba(3,164,115,0.7);border-radius:130px 130px 130px 130px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Trova la soluzione<br />per la tua impresa </div>
							</div>
						</li>
					</ul>
				</div>

				<script type="text/javascript">
					if (setREVStartSize !== undefined) setREVStartSize({
						c: '#rev_slider_4_1',
						responsiveLevels: [1240, 1024, 778, 480],
						gridwidth: [1320, 1144, 778, 580],
						gridheight: [647, 561, 860, 800],
						sliderLayout: 'fullwidth'
					});

					var revapi4,
						tpj;
					(function() {
						if (!/loaded|interactive|complete/.test(document.readyState)) document.addEventListener("DOMContentLoaded", onLoad);
						else onLoad();

						function onLoad() {
							if (tpj === undefined) {
								tpj = jQuery;
								if ("off" == "on") tpj.noConflict();
							}
							if (tpj("#rev_slider_4_1").revolution == undefined) {
								revslider_showDoubleJqueryError("#rev_slider_4_1");
							} else {
								revapi4 = tpj("#rev_slider_4_1").show().revolution({
									sliderType: "standard",
									jsFileLocation: "wp-content/plugins/revslider/public/assets/js/",
									sliderLayout: "fullwidth",
									dottedOverlay: "none",
									delay: 4000,
									navigation: {
										keyboardNavigation: "off",
										keyboard_direction: "horizontal",
										mouseScrollNavigation: "off",
										mouseScrollReverse: "default",
										onHoverStop: "on",
										touch: {
											touchenabled: "on",
											touchOnDesktop: "off",
											swipe_threshold: 0,
											swipe_min_touches: 1,
											swipe_direction: "horizontal",
											drag_block_vertical: false
										},
										arrows: {
											style: "uranus",
											enable: true,
											hide_onmobile: false,
											hide_onleave: true,
											hide_delay: 200,
											hide_delay_mobile: 1200,
											tmp: '',
											left: {
												h_align: "left",
												v_align: "center",
												h_offset: 0,
												v_offset: 55
											},
											right: {
												h_align: "right",
												v_align: "center",
												h_offset: 0,
												v_offset: 55
											}
										}
									},
									responsiveLevels: [1240, 1024, 778, 480],
									visibilityLevels: [1240, 1024, 778, 480],
									gridwidth: [1320, 1144, 778, 580],
									gridheight: [647, 561, 860, 800],
									lazyType: "none",
									scrolleffect: {
										fade: "on",
										on_slidebg: "on",
										on_layers: "on",
									},
									shadow: 0,
									spinner: "off",
									stopLoop: "off",
									stopAfterLoops: -1,
									stopAtSlide: -1,
									shuffle: "off",
									autoHeight: "off",
									disableProgressBar: "on",
									hideThumbsOnMobile: "off",
									hideSliderAtLimit: 0,
									hideCaptionAtLimit: 0,
									hideAllCaptionAtLilmit: 0,
									debugMode: false,
									fallbacks: {
										simplifyAll: "off",
										nextSlideOnWindowFocus: "off",
										disableFocusListener: false,
									}
								});
							}; /* END OF revapi call */

						}; /* END OF ON LOAD FUNCTION */
					}()); /* END OF WRAPPING FUNCTION */
				</script>

			</div><!-- END REVOLUTION SLIDER -->
		</div>



		<main id="main" role="main" class="clearfix " style="">
			<div class="fusion-row" style="">

				<section id="content" style="width: 100%;">
					<div id="post-10" class="post-10 page type-page status-publish hentry">
						<div class="post-content">

							<div class="vc_row wpb_row vc_row-fluid mpc-row mcc-home-text">
								<div class="wpb_column vc_column_container vc_col-sm-12 mpc-column" data-column-id="">
									<div class="vc_column-inner">
										<div class="wpb_wrapper">
											<h1 class="mcc-home-text-title">Centennial Bank, the bank for businesses</h1>
											<h4 class="vc_custom_heading mcc-home-text-description">We implement and integrate policies public to support the Italian production system</h4>
										</div>
									</div>
								</div>
							</div>
							<div class="mcc-content-double-carousel vc_row wpb_row vc_row-fluid mpc-row">
								<div class="wpb_column vc_column_container vc_col-sm-12 mpc-column" data-column-id="">
									<div class="vc_column-inner">
										<div class="wpb_wrapper">
											<div class="wpb_text_column wpb_content_element ">
												<div class="wpb_wrapper">
													<div class="owl-carousel double-carousel owl-theme">
														<div class="item">
															<div class="content-carousel">
																<img width="787" height="301" src="wp-content/uploads/2019/02/news-787x301.jpg" class="attachment-carosello_home_doppio size-carosello_home_doppio wp-post-image" alt="reform of the Guarantee Fund for SMEs" /> <a href="index.php">
																	<div class="permalink">
																		<h3>The reform of the Guarantee Fund for SMEs starts</h3>
																		<span>FIND OUT MORE</span>
																		<div class="triangolo"></div>
																	</div>
																</a>
															</div>
														</div>
														<div class="item">
															<div class="content-carousel">
																<img width="787" height="301" src="wp-content/uploads/2018/11/resto-al-sud-787x301.jpg" class="attachment-carosello_home_doppio size-carosello_home_doppio wp-post-image" alt="mcc resto al sud Centennial Bank" /> <a href="index.php">
																	<div class="permalink">
																		<h3>Chiro Rest in the South</h3>
																		<div class="mcc-box-subtitle-home">
																			For young entrepreneurs from the South </div>
																		<span>FIND OUT MORE</span>
																		<div class="triangolo"></div>
																	</div>
																</a>
															</div>
														</div>
														<div class="item">
															<div class="content-carousel">
																<img width="787" height="301" src="wp-content/uploads/2018/11/Finanziamenti-per-ricerca-e-innovazione-BEI-MIUR-787x301.jpg" class="attachment-carosello_home_doppio size-carosello_home_doppio wp-post-image" alt="Finanziamenti per ricerca e innovazione (BEI-MIUR)" /> <a href="index.php">
																	<div class="permalink">
																		<h3>Funding for research and innovation (BEI-MIUR)</h3>
																		<div class="mcc-box-subtitle-home">
																			High-tech projects in the South </div>
																		<span>FIND OUT MORE</span>
																		<div class="triangolo"></div>
																	</div>
																</a>
															</div>
														</div>
														<div class="item">
															<div class="content-carousel">
																<img width="787" height="301" src="wp-content/uploads/2018/11/Finanziamento-Chirofast-mediocredito-787x301.jpg" class="attachment-carosello_home_doppio size-carosello_home_doppio wp-post-image" alt="Finanziamento Chirofast Centennial Bank" /> <a href="index.php">
																	<div class="permalink">
																		<h3>Chiro Fast</h3>
																		<div class="mcc-box-subtitle-home">
																			For faster financing </div>
																		<span>FIND OUT MORE</span>
																		<div class="triangolo"></div>
																	</div>
																</a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="vc_row wpb_row vc_row-fluid mpc-row">
								<div class="wpb_column vc_column_container vc_col-sm-12 mpc-column" data-column-id="">
									<div class="vc_column-inner">
										<div class="wpb_wrapper">
											<h2 class="vc_custom_heading">
												COMMITMENT TO THE COUNTRY</h2>
											<h4 class="vc_custom_heading mcc-home-text-description">The numbers that testify to Centennial Bank's support for the business system</h4>
											<p class="mcc-home-date-counter">Data at 31/12/2018 </p>
										</div>
									</div>
								</div>
							</div>

							<div class="vc_row wpb_row vc_row-fluid mpc-row">
								<div class="wpb_column vc_column_container vc_col-sm-4 mpc-column" data-column-id="">
									<div class="vc_column-inner">
										<div class="wpb_wrapper">
											<div data-id="" class="mcc-box-counter">
												<div class="mpc-counter__counter">
													<div class="mpc-counter--target">84 thousand </div>
													<div class="mpc-counter--sizer">84 thousand </div>
												</div>
												<h3 class="mpc-counter__heading">Funded companies </h3>
											</div>
										</div>
									</div>
								</div>
								<div class="wpb_column vc_column_container vc_col-sm-4 mpc-column" data-column-id="">
									<div class="vc_column-inner">
										<div class="wpb_wrapper">
											<div data-id="" class="mcc-box-counter">
												<div class="mpc-counter__counter">
													<div class="mpc-counter--target">99 %</div>
													<div class="mpc-counter--sizer">99 %</div>
												</div>
												<h3 class="mpc-counter__heading">SME's</h3>
											</div>
										</div>
									</div>
								</div>
								<div class="wpb_column vc_column_container vc_col-sm-4 mpc-column" data-column-id="">
									<div class="vc_column-inner">
										<div class="wpb_wrapper">
											<div data-id="" class="mcc-box-counter">
												<div class="mpc-counter__counter">
													<div class="mpc-counter--target">$ 5,8 billion</div>
													<div class="mpc-counter--sizer">$ 5,8 billion</div>
												</div>
												<h3 class="mpc-counter__heading">Investments activated</h3>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="vc_row wpb_row vc_row-fluid mpc-row">
								<div class="wpb_column vc_column_container vc_col-sm-12 mpc-column" data-column-id="">
									<div class="vc_column-inner">
										<div class="wpb_wrapper">
											<h2 class="vc_custom_heading">IN THE SPOTLIGHT</h2>
											<h4 class="vc_custom_heading mcc-home-text-description">News and success stories on Centennial Bank loans and facilities</h4>
										</div>
									</div>
								</div>
							</div>

							<div class="vc_row wpb_row vc_row-fluid mpc-row mcc-row-single-carousel">
								<div class="wpb_column vc_column_container vc_col-sm-12 mpc-column" data-column-id="">
									<div class="vc_column-inner">
										<div class="wpb_wrapper">
											<div class="wpb_text_column wpb_content_element ">
												<div class="wpb_wrapper">
													<div class="owl-carousel single-carousel owl-theme">
														<div class="item">
															<div class="content-carousel">
																<img width="630" height="325" src="wp-content/uploads/2019/06/GROTTA-DI-SALE-630x325.jpg" class="attachment-carosello_home_singolo size-carosello_home_singolo wp-post-image" alt="" /> <a title="" href="index.php">
																	<div style="background: rgba(35,173,130,0.8)" class="permalink">
																		<div class="permalink-title">
																			<h3>Sixth Sense, Natural techniques and innovation for well-being</h3>
																		</div>
																		<div class="permalink-link">
																			<span class="mcc-sc-item-type">
																				success stories </span>
																			<span class="mcc-sc-link">Find out more</span>
																		</div>
																		<div class="triangolo"></div>
																	</div>
																</a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="mcc-primopiano vc_row wpb_row vc_row-fluid row-content-box-news mpc-row">
								<div class="wpb_column vc_column_container vc_col-sm-4 mpc-column" data-column-id="">
									<div class="vc_column-inner">
										<div class="wpb_wrapper">
											<div class="wpb_raw_code wpb_content_element wpb_raw_html">
												<div class="wpb_wrapper">
													<div class="content-box-news">
														<img width="498" height="255" src="wp-content/uploads/2019/06/festival-del-lavoro-2.jpg" class="attachment-blocchi size-blocchi wp-post-image" alt="" srcset="https://www.mcc.it/wp-content/uploads/2019/06/festival-del-lavoro-2-200x102.jpg 200w, https://www.mcc.it/wp-content/uploads/2019/06/festival-del-lavoro-2-300x154.jpg 300w, https://www.mcc.it/wp-content/uploads/2019/06/festival-del-lavoro-2-400x205.jpg 400w, https://www.mcc.it/wp-content/uploads/2019/06/festival-del-lavoro-2.jpg 498w" sizes="(max-width: 498px) 100vw, 498px" /> <a href="index.php">
															<div class="permalink-news">
																<div class="permalink-title">
																	<h3>Funding for SMEs at the Labor Festival</h3>
																</div>
																<div class="permalink-link">
																	<span class="mcc-sc-item-type">
																		news </span>
																	<span class="mcc-sc-link">Read</span>
																</div>
																<div style="border-color:transparent transparent #8d959a transparent" class="triangolo"></div>
															</div>
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="wpb_column vc_column_container vc_col-sm-4 mpc-column" data-column-id="">
									<div class="vc_column-inner">
										<div class="wpb_wrapper">
											<div class="wpb_raw_code wpb_content_element wpb_raw_html">
												<div class="wpb_wrapper">
													<div class="content-box-news">
														<img width="669" height="350" src="wp-content/uploads/2019/05/photo-1468259275383-c4f1b88d5772-669x350.jpg" class="attachment-blocchi size-blocchi wp-post-image" alt="" /> <a href="index.php">
															<div class="permalink-news">
																<div class="permalink-title">
																	<h3>The road show starts Doing business in Sicily</h3>
																</div>
																<div class="permalink-link">
																	<span class="mcc-sc-item-type">
																		news </span>
																	<span class="mcc-sc-link">Read</span>
																</div>
																<div style="border-color:transparent transparent #8d959a transparent" class="triangolo"></div>
															</div>
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="wpb_column vc_column_container vc_col-sm-4 mpc-column" data-column-id="">
									<div class="vc_column-inner">
										<div class="wpb_wrapper">
											<div class="wpb_raw_code wpb_content_element wpb_raw_html">
												<div class="wpb_wrapper">
													<div class="content-box-news">
														<img width="669" height="350" src="wp-content/uploads/2019/05/Exploracity-669x350.png" class="attachment-blocchi size-blocchi wp-post-image" alt="Exploracity Genova, itinerari turistici tra il reale e il virtuale" /> <a href="index.php">
															<div class="permalink-news">
																<div class="permalink-title">
																	<h3>Exploracity Genova, tourist itineraries between real and virtual</h3>
																</div>
																<div class="permalink-link">
																	<span class="mcc-sc-item-type">
																		Success Stories </span>
																	<span class="mcc-sc-link">Read</span>
																</div>
																<div style="border-color:transparent transparent #23AD82 transparent" class="triangolo"></div>
															</div>
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

				<div class="fraud-container">
					<div class="fraud-button"><img alt="Sicurezza a frodi" src="wp-content/themes/mcc/assets/images/sicurezza.svg" />
					</div>
					<div class="fraud-panel">
						<div class="fraud-box">
							<h5>Security and Fraud</h5>
							<p>Dear Customer, in this period Centennial Bank SpA and many companies are victims of deceptive actions with which financial products are offered, abusively using the name of Centennial Bank SpA, whose concession is subject to the stipulation of an insurance policy and the payment of the relative premium .
								<br />We emphasize that this is a phenomenon to which the Bank is completely foreign.<br />
								<a href="index.php" title="Read" target="_blank"><strong>Read</strong></a>
							</p>
						</div>
					</div>
				</div>

				<script>
					jQuery(document).ready(function($) {
						var width = $(window).width();
						// alert(width);
						$('.double-carousel').owlCarousel({
							//afterInit : attachEvent(),
							loop: true,
							margin: 10,
							nav: true,
							slideBy: 2,
							dots: false,
							responsiveClass: true,
							autoHeight: false,
							autoplay: true,
							autoplayTimeout: 4000,
							responsive: {
								0: {
									items: 1,
									nav: false,
									margin: 0,
									slideBy: 1,
									loop: true
								},
								768: {
									items: 2,
									nav: true,
									loop: true,
									margin: 0
								},
								1000: {
									items: 2,
									nav: true,
									loop: true,
									margin: 0
								}
							},
						});

						var owl = $('.single-carousel');
						owl.owlCarousel({
							items: 1,
							loop: true,
							nav: true,
							dots: false,
							margin: 0,
							autoplay: false,
							autoplayTimeout: 4000,
							autoplayHoverPause: true
						});
						$('.play').on('click', function() {
							owl.trigger('play.owl.autoplay', [1000])
						})
						$('.stop').on('click', function() {
							owl.trigger('stop.owl.autoplay')
						})
					})

					jQuery('.owl-carousel').on('initialized.owl.carousel', function(event) {
						jQuery('.owl-item').each(function(index) {
							var div = jQuery(this);
							if (index % 2 != 0) {
								div.addClass('dispari');
							}
						});
					});
				</script>


			</div> <!-- fusion-row -->
		</main> <!-- #main -->




		<div class="fusion-footer">

			<footer role="contentinfo" class="fusion-footer-widget-area fusion-widget-area">
				<div class="fusion-row">
					<div class="fusion-columns fusion-columns-4 fusion-widget-area">

						<div class="fusion-column col-lg-3 col-md-3 col-sm-3">
							<section id="media_image-2" class="fusion-footer-widget-column widget widget_media_image"><img width="188" height="118" src="images/logo/cover.png" class="image wp-image-115  attachment-full size-full" alt="Logo Centennial Bank" style="width:400px; height: auto;" />
								<div style="clear:both;"></div>
							</section>
						</div>
						<div class="fusion-column col-lg-3 col-md-3 col-sm-3">
							<section id="nav_menu-2" class="fusion-footer-widget-column widget widget_nav_menu">
								<div class="menu-menu-footer-1-container">
									<ul id="menu-menu-footer-1" class="menu">
										<li id="menu-item-3321" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3321"><a href="index.php" data-name="Disclaimer">Disclaimer</a></li>
										<li id="menu-item-3320" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3320"><a href="index.php" data-name="Warnings">Warnings</a></li>
										<li id="menu-item-3323" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3323"><a href="index.php" data-name="Privacy policy">Privacy policy</a></li>
										<li id="menu-item-3322" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3322"><a href="index.php" data-name="Transparency">Transparency</a></li>
										<li id="menu-item-3324" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3324"><a href="index.php" data-name="Security and Fraud">Security and Fraud</a></li>
									</ul>
								</div>
								<div style="clear:both;"></div>
							</section>
						</div>
						<div class="fusion-column col-lg-3 col-md-3 col-sm-3">
							<section id="nav_menu-3" class="fusion-footer-widget-column widget widget_nav_menu">
								<div class="menu-menu-footer-2-container">
									<ul id="menu-menu-footer-2" class="menu">
										<li id="menu-item-3338" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3338"><a href="sospensione-rate-index.php" data-name="Suspension of loan installments">Suspension of loan installments</a></li>
										<li id="menu-item-3339" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3339"><a href="index.php" data-name="Referee for Disputes">Referee for disputes</a></li>
										<li id="menu-item-3340" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3340"><a href="index.php" data-name="Company Data">Company data</a></li>
										<li id="menu-item-5243" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5243"><a href="index.php" data-name="PSD2 &#038; Open Banking">PSD2 &#038; Open Banking</a></li>
									</ul>
								</div>
								<div style="clear:both;"></div>
							</section>
						</div>
						<div class="fusion-column fusion-column-last col-lg-3 col-md-3 col-sm-3">
							<!-- <section id="social_links-widget-2" class="fusion-footer-widget-column widget social_links">
								<h4 class="widget-title">FOLLOW US</h4>
								<div class="fusion-social-networks">

									<div class="fusion-social-networks-wrapper">

										<a class="fusion-social-network-icon fusion-tooltip fusion-rss fusion-icon-rss" href="index.php" data-placement="top" data-title="Rss" data-toggle="tooltip" data-original-title="" title="Rss" aria-label="Rss" rel="nofollow noopener noreferrer" target="_self" style="font-size:16px;color:#bebdbd;"></a>


										<a class="fusion-social-network-icon fusion-tooltip fusion-linkedin fusion-icon-linkedin" href="https://www.linkedin.com/company/mediocredito-centrale/" data-placement="top" data-title="LinkedIn" data-toggle="tooltip" data-original-title="" title="LinkedIn" aria-label="LinkedIn" rel="nofollow noopener noreferrer" target="_self" style="font-size:16px;color:#bebdbd;"></a>


										<a class="fusion-social-network-icon fusion-tooltip fusion-mail fusion-icon-mail" href="mailto:re&#108;az&#105;&#111;n&#105;&#046;e&#115;t&#101;rn&#101;&#064;&#109;cc.i&#116;" data-placement="top" data-title="Mail" data-toggle="tooltip" data-original-title="" title="Mail" aria-label="Mail" rel="nofollow noopener noreferrer" target="_self" style="font-size:16px;color:#bebdbd;"></a>



									</div>
								</div>

								<div style="clear:both;"></div>
							</section> -->
						</div>

						<div class="fusion-clearfix"></div>
					</div> <!-- fusion-columns -->
				</div> <!-- fusion-row -->
			</footer> <!-- fusion-footer-widget-area -->


			<footer id="footer" class="fusion-footer-copyright-area fusion-footer-copyright-center">
				<div class="fusion-row">
					<div class="fusion-copyright-content">

						<div class="fusion-copyright-notice">
							<div>
								Centennial Bank © 2020 Tax Code 00594040586 VAT number 00915101000 </div>
						</div>
						<div class="fusion-social-links-footer">
						</div>

					</div> <!-- fusion-fusion-copyright-content -->
				</div> <!-- fusion-row -->
			</footer> <!-- #footer -->
		</div> <!-- fusion-footer -->

	</div> <!-- wrapper -->

	<a class="fusion-one-page-text-link fusion-page-load-link"></a>

	<div class='asp_hidden_data' id="asp_hidden_data" style="display: none !important;">
		<svg style="position:absolute" height="0" width="0">
			<filter id="aspblur">
				<feGaussianBlur in="SourceGraphic" stdDeviation="4" />
			</filter>
		</svg>
		<svg style="position:absolute" height="0" width="0">
			<filter id="no_aspblur"></filter>
		</svg>
	</div>

	<link rel='stylesheet' id='js_composer_front-css' href='wp-content/plugins/js_composer/assets/css/js_composer.min40df.css?ver=5.6' type='text/css' media='all' />
	<script type='text/javascript'>
		/* <![CDATA[ */
		var wpcf7 = {
			"apiSettings": {
				"root": "https:\/\/www.mcc.it\/wp-json\/contact-form-7\/v1",
				"namespace": "contact-form-7\/v1"
			},
			"cached": "1"
		};
		/* ]]> */
	</script>
	<script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/scripts3c21.js?ver=5.1.1'></script>
	<!--[if IE 9]>
<script type='text/javascript' src='https://www.mcc.it/wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-ie9.js?ver=1'></script>
<![endif]-->
	<script type='text/javascript' src='wp-content/plugins/ajax-search-pro/js/nomin/photostacke154.js?ver=A9LIR0'></script>
	<script type='text/javascript' src='wp-content/plugins/ajax-search-pro/js/min/chosen.jquery.mine154.js?ver=A9LIR0'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var ajaxsearchpro = {
			"ajaxurl": "https:\/\/www.mcc.it\/wp-admin\/admin-ajax.php",
			"backend_ajaxurl": "https:\/\/www.mcc.it\/wp-admin\/admin-ajax.php",
			"js_scope": "jQuery"
		};
		var ASP = {
			"ajaxurl": "https:\/\/www.mcc.it\/wp-admin\/admin-ajax.php",
			"backend_ajaxurl": "https:\/\/www.mcc.it\/wp-admin\/admin-ajax.php",
			"js_scope": "jQuery",
			"asp_url": "https:\/\/www.mcc.it\/wp-content\/plugins\/ajax-search-pro\/",
			"upload_url": "https:\/\/www.mcc.it\/wp-content\/uploads\/asp_upload\/",
			"detect_ajax": "0",
			"media_query": "A9LIR0",
			"version": "4978",
			"scrollbar": "1",
			"css_loaded": "1",
			"js_retain_popstate": "0",
			"fix_duplicates": "1"
		};
		/* ]]> */
	</script>
	<script type='text/javascript' src='wp-content/plugins/ajax-search-pro/js/min/jquery.ajaxsearchpro-noui-isotope.mine154.js?ver=A9LIR0'></script>
	<script type='text/javascript' src='wp-includes/js/jquery/ui/core.mine899.js?ver=1.11.4'></script>
	<script type='text/javascript' src='wp-includes/js/jquery/ui/datepicker.mine899.js?ver=1.11.4'></script>
	<script type='text/javascript'>
		jQuery(document).ready(function(jQuery) {
			jQuery.datepicker.setDefaults({
				"closeText": "Chiudi",
				"currentText": "Oggi",
				"monthNames": ["gennaio", "febbraio", "marzo", "aprile", "maggio", "giugno", "luglio", "agosto", "settembre", "ottobre", "novembre", "dicembre"],
				"monthNamesShort": ["Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic"],
				"nextText": "Successivo",
				"prevText": "Precedente",
				"dayNames": ["domenica", "luned\u00ec", "marted\u00ec", "mercoled\u00ec", "gioved\u00ec", "venerd\u00ec", "sabato"],
				"dayNamesShort": ["dom", "lun", "mar", "mer", "gio", "ven", "sab"],
				"dayNamesMin": ["D", "L", "M", "M", "G", "V", "S"],
				"dateFormat": "d MM yy",
				"firstDay": 1,
				"isRTL": false
			});
		});
	</script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.hoverintent68b3.js?ver=1'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-vertical-menu-widget68b3.js?ver=1'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/cssuac93e.js?ver=2.1.28'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/modernizr3d36.js?ver=3.3.1'></script>
	<script type='text/javascript' src='wp-content/plugins/js_composer/assets/lib/bower/isotope/dist/isotope.pkgd.min40df.js?ver=5.6'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/packery001e.js?ver=2.0.0'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/bootstrap.collapseb12b.js?ver=3.1.1'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/bootstrap.modalb12b.js?ver=3.1.1'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/bootstrap.tooltip46df.js?ver=3.3.5'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/bootstrap.popover46df.js?ver=3.3.5'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/bootstrap.transitione485.js?ver=3.3.6'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/bootstrap.tabb12b.js?ver=3.1.1'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.waypointsb95e.js?ver=2.0.3'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.requestAnimationFrame68b3.js?ver=1'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.appear68b3.js?ver=1'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.carouFredSelf731.js?ver=6.2.1'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.cycle19ce.js?ver=3.0.3'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.easing4e44.js?ver=1.3'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.easyPieCharte7f3.js?ver=2.1.7'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.fitvids4963.js?ver=1.1'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.flexslider605a.js?ver=2.2.2'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var fusionMapsVars = {
			"admin_ajax": "https:\/\/www.mcc.it\/wp-admin\/admin-ajax.php"
		};
		/* ]]> */
	</script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.fusion_maps605a.js?ver=2.2.2'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.hoverflow68b3.js?ver=1'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var fusionLightboxVideoVars = {
			"lightbox_video_width": "1280",
			"lightbox_video_height": "720"
		};
		/* ]]> */
	</script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.ilightbox950a.js?ver=2.2.3'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.infinitescroll53cf.js?ver=2.1'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.mousewheel7c45.js?ver=3.0.6'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.placeholder5d0a.js?ver=2.0.7'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.touchSwipe7514.js?ver=1.6.6'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.fade68b3.js?ver=1'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/imagesLoaded3a79.js?ver=3.1.8'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-alert68b3.js?ver=1'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var fusionEqualHeightVars = {
			"content_break_point": "800"
		};
		/* ]]> */
	</script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-equal-heights68b3.js?ver=1'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/fusion-parallax68b3.js?ver=1'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var fusionVideoGeneralVars = {
			"status_vimeo": "0",
			"status_yt": "1"
		};
		/* ]]> */
	</script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/fusion-video-general68b3.js?ver=1'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var fusionVideoBgVars = {
			"status_vimeo": "0",
			"status_yt": "1"
		};
		/* ]]> */
	</script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/fusion-video-bg68b3.js?ver=1'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-waypoints68b3.js?ver=1'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var fusionLightboxVars = {
			"status_lightbox": "1",
			"lightbox_gallery": "1",
			"lightbox_skin": "metro-white",
			"lightbox_title": "1",
			"lightbox_arrows": "1",
			"lightbox_slideshow_speed": "5000",
			"lightbox_autoplay": "",
			"lightbox_opacity": "0.9",
			"lightbox_desc": "1",
			"lightbox_social": "1",
			"lightbox_deeplinking": "1",
			"lightbox_path": "vertical",
			"lightbox_post_images": "1",
			"lightbox_animation_speed": "Normal"
		};
		/* ]]> */
	</script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-lightbox68b3.js?ver=1'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var fusionCarouselVars = {
			"related_posts_speed": "2500",
			"carousel_speed": "5000"
		};
		/* ]]> */
	</script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-carousel68b3.js?ver=1'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var fusionFlexSliderVars = {
			"status_vimeo": "",
			"page_smoothHeight": "false",
			"slideshow_autoplay": "1",
			"slideshow_speed": "7000",
			"pagination_video_slide": "",
			"status_yt": "1",
			"flex_smoothHeight": "false"
		};
		/* ]]> */
	</script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-flexslider68b3.js?ver=1'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-popover68b3.js?ver=1'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-tooltip68b3.js?ver=1'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-sharing-box68b3.js?ver=1'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var fusionBlogVars = {
			"infinite_blog_text": "<em>Loading the next set of posts...<\/em>",
			"infinite_finished_msg": "<em>All items displayed.<\/em>",
			"slideshow_autoplay": "1",
			"slideshow_speed": "7000",
			"pagination_video_slide": "",
			"status_yt": "1",
			"lightbox_behavior": "all",
			"blog_pagination_type": "Pagination",
			"flex_smoothHeight": "false"
		};
		/* ]]> */
	</script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-blog68b3.js?ver=1'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-button68b3.js?ver=1'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-general-global68b3.js?ver=1'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var fusionIe1011Vars = {
			"form_bg_color": "#ffffff"
		};
		/* ]]> */
	</script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-ie101168b3.js?ver=1'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var avadaHeaderVars = {
			"header_position": "top",
			"header_layout": "v2",
			"header_sticky": "0",
			"header_sticky_type2_layout": "menu_only",
			"side_header_break_point": "1024",
			"header_sticky_mobile": "0",
			"header_sticky_tablet": "0",
			"mobile_menu_design": "flyout",
			"sticky_header_shrinkage": "0",
			"nav_height": "54",
			"nav_highlight_border": "3",
			"nav_highlight_style": "bar",
			"logo_margin_top": "31px",
			"logo_margin_bottom": "31px",
			"layout_mode": "wide",
			"header_padding_top": "0px",
			"header_padding_bottom": "0px",
			"offset_scroll": "full"
		};
		/* ]]> */
	</script>
	<script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-header9f31.js?ver=5.7.2'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var avadaMenuVars = {
			"header_position": "Top",
			"logo_alignment": "Left",
			"header_sticky": "0",
			"side_header_break_point": "1024",
			"mobile_menu_design": "flyout",
			"dropdown_goto": "Go to...",
			"mobile_nav_cart": "Shopping Cart",
			"mobile_submenu_open": "Open Sub Menu",
			"mobile_submenu_close": "Close Sub Menu",
			"submenu_slideout": "1"
		};
		/* ]]> */
	</script>
	<script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-menu9f31.js?ver=5.7.2'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var fusionScrollToAnchorVars = {
			"content_break_point": "800",
			"container_hundred_percent_height_mobile": "0"
		};
		/* ]]> */
	</script>
	<script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-scroll-to-anchor68b3.js?ver=1'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/library/bootstrap.scrollspyd617.js?ver=3.3.2'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var avadaCommentVars = {
			"title_style_type": "",
			"title_margin_top": "",
			"title_margin_bottom": ""
		};
		/* ]]> */
	</script>
	<script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-comments9f31.js?ver=5.7.2'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-general-footer9f31.js?ver=5.7.2'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-quantity9f31.js?ver=5.7.2'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-scrollspy9f31.js?ver=5.7.2'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-select9f31.js?ver=5.7.2'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var avadaSidebarsVars = {
			"header_position": "top",
			"header_layout": "v2",
			"header_sticky": "0",
			"header_sticky_type2_layout": "menu_only",
			"side_header_break_point": "1024",
			"header_sticky_tablet": "0",
			"sticky_header_shrinkage": "0",
			"nav_height": "54",
			"content_break_point": "800"
		};
		/* ]]> */
	</script>
	<script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-sidebars9f31.js?ver=5.7.2'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/library/jquery.sticky-kit9f31.js?ver=5.7.2'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-tabs-widget9f31.js?ver=5.7.2'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-container-scroll9f31.js?ver=5.7.2'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var toTopscreenReaderText = {
			"label": "Go to Top"
		};
		/* ]]> */
	</script>
	<script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/library/jquery.toTop62ea.js?ver=1.2'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var avadaToTopVars = {
			"status_totop_mobile": "0"
		};
		/* ]]> */
	</script>
	<script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-to-top9f31.js?ver=5.7.2'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-drop-down9f31.js?ver=5.7.2'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var avadaRevVars = {
			"avada_rev_styles": "1"
		};
		/* ]]> */
	</script>
	<script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-rev-styles9f31.js?ver=5.7.2'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-contact-form-79f31.js?ver=5.7.2'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var avadaPrivacyVars = {
			"name": "privacy_embeds",
			"days": "30",
			"path": "\/",
			"types": [],
			"defaults": [],
			"button": "0"
		};
		/* ]]> */
	</script>
	<script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-privacy9f31.js?ver=5.7.2'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var _mpc_ajax = "index.php\/\/www.mcc.it\/wp-admin\/admin-ajax.php";
		var _mpc_animations = "0";
		var _mpc_parallax = "0";
		var _mpc_scroll_to_id = "1";
		/* ]]> */
	</script>
	<script type='text/javascript' src='wp-content/plugins/mpc-massive/assets/js/mpc-vendor.min9ab0.js?ver=2.4.3.2'></script>
	<script type='text/javascript' src='wp-content/plugins/mpc-massive/assets/js/mpc-scripts.min9ab0.js?ver=2.4.3.2'></script>
	<script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-maind87f.js?ver=4.9.9'></script>
	<script type='text/javascript' src='wp-includes/js/wp-embed.mind87f.js?ver=4.9.9'></script>
	<script type='text/javascript' src='wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min40df.js?ver=5.6'></script>
</body>

<!-- Mirrored from www.mcc.it/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 24 Jun 2019 08:39:18 GMT -->

</html>

<!-- Dynamic page generated in 3.248 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2019-06-20 16:55:56 -->

<!-- Compression = gzip -->