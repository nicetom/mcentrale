﻿jQuery.support.cors = true;

var map = null;
var directionsManager = null;

function LoadMap(bingKey) {

    if (typeof (map) != "undefined" && map != null) {
        map = map;
    }
    else if (typeof (topMenuMap) != "undefined" && topMenuMap != null) {
        map = topMenuMap;
    }
    else {
        map = new Microsoft.Maps.Map('#myMap', {
            credentials: bingKey
        });
        LogEvent("Error", "Driving Directions::Bing Map Call :" + window.location.href);
    }
    //https://msdn.microsoft.com/en-us/library/mt750407.aspx

    Microsoft.Maps.loadModule('Microsoft.Maps.Directions', function () {
        //Create an instance of the directions manager.
        directionsManager = new Microsoft.Maps.Directions.DirectionsManager(map);
        onGeocodeClick();
       
    });
}

function UnloadMap() {
    if (map != null) {
        map.Dispose();
    }
}

function getDirections() {

    $("#drivingDirections").show();
    directionsManager.clearAll();

    // Create start and end waypoints
    var startWaypoint = new Microsoft.Maps.Directions.Waypoint({ address: $("#update-start").val() });
    var endWaypoint = new Microsoft.Maps.Directions.Waypoint({ address: toAddress });

    directionsManager.addWaypoint(startWaypoint);
    directionsManager.addWaypoint(endWaypoint);

    // Set the id of the div to use to display the directions
    directionsManager.setRenderOptions({ itineraryContainer: document.getElementById('drivingDirections') });

    // Specify a handler for when an error occurs
    Microsoft.Maps.Events.addHandler(directionsManager, 'directionsError', directionError);

    // Calculate directions, which displays a route on the map
    directionsManager.calculateDirections();
}

function directionsUpdated() {
    window.print();
}

function directionError(e) {
    alert(e.message);
}

function onGeocodeClick() {
    if ($("#update-start").val() == "") {
        
        return;
    }

    map.entities.clear();

    map.getCredentials(MakeGeocodeRequest);
}

//https://social.msdn.microsoft.com/Forums/en-US/19549671-58ea-421c-b66b-477baeaf2d54/bing-maps-v8-rest-service-url-for-v8?forum=bingmapsservices 
function MakeGeocodeRequest(credentials) {

    var where = $("#update-start").val();
    var geocodeRequest = "https://dev.virtualearth.net/REST/v1/Locations?query=" + encodeURI(where) + "&output=json&jsonp=GeocodeCallback&key=" + credentials;
   
    CallRestService(geocodeRequest);
}
function LogEvent(level, message) {
    level = level || 'Error';
    $.ajax({
        url: '/Home/LogErrorEvent',
        type: "POST",
        data: { message: message },
        success: function (result) {
          //  console.log('Log done');
        }
    });
}
function CallRestService(request) {
    var script = document.createElement("script");
    script.setAttribute("type", "text/javascript");
    script.setAttribute("src", request);
    document.body.appendChild(script);
}

var searchedViewBoundary = null;

function GeocodeCallback(result) {
    // if there are no results, display the error message and return
    if (!(result
        && result.resourceSets
        && result.resourceSets.length > 0
        && result.resourceSets[0].resources
        && result.resourceSets[0].resources.length > 0)) {
        //alert("No Results Found");
        $('#error-update-start').html('No results found.');
        return;
    } else {
        $('#error-update-start').html('');
    }

    var resource = result.resourceSets[0].resources[0];
    var bbox = resource.bbox;
    // Add a pushpin at the found location
    getDirections();
}

$(document).ready(function () {
    //$(window).scroll(function () {

    //    var scrollTimer;

    //    $(window).bind('scroll', function () {
    //        clearTimeout(scrollTimer);
    //        scrollTimer = setTimeout(refresh, 200);
    //    });

    //    var refresh = function () {
    //        var doc = document.documentElement, body = document.body;
    //        var top = Math.max((doc && doc.scrollTop || body && body.scrollTop || 0) - 560, 0);

    //        $('#myMap').stop().animate({ top: top }, 500, function () { });
    //    };
    //});

    $("#frmDirectionsAddress").on("submit", function (e) {
        e.preventDefault();
        onGeocodeClick();
    });

    $('#update-start').keydown(function (event) {
       if (event.keyCode == 13) {
            onGeocodeClick();
        }
    });

    $(document).unload(UnloadMap);

    $('#link-back').on("click", function (e) {
        e.preventDefault();
        var lnk = $(this);
        var url = lnk.attr("href") + "?proximity=" + proximity + "&address=" + $('#update-start').val();
        window.location = url;
    });

    $('#print-link').on("click", function () {
        window.print();
    });
});