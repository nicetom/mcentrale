﻿var isContactSending = false;

$(function () {

    $("#contact-form").on("submit", function (e) {
        e.preventDefault();

        if (isContactSending)
        {
            return;
        }

        var $form = $(this);

        var recapchaResponse = grecaptcha.getResponse();

        if (recapchaResponse != null && recapchaResponse.length == 0)
        {
            $form.find("[name='captcha-error']").show();
        }
        else {
            $form.find("[name='captcha-error']").hide();
        }

        if ($form.valid() && recapchaResponse.length > 0) {
            
            var url = $form.attr("action");
            var data = $form.serialize();

            var spinner = new Spinner().spin($form[0]);
            isContactSending = true;

            $.post(url, data, function (data) {
                isContactSending = false;
                spinner.stop();
                if (data.MessageSent) {
                    $("#divContactUsSuccess").show();
                    $form[0].reset();
                    window.scrollTo(0, 0);
                }
            });
        }
    });
});