//Functions 

// User clicks on the find my branch search button
function crossDissolveTransition(element) { 
    var elementFadeOut = $(element).parent(),
        elementFadeIn = $(elementFadeOut).next(),
        wrapper = $(element).parents(".navigation__dropdown-wrap--clipped");

    $(".navigation__dropdown-wrap--branch").fadeOut();
    $("#top-menu-branches  .navigation__dropdown-wrap--branch-results--result-block--accordion--elem:first").slideDown(200, function () {
        $(this).parent().addClass("active");
        $(this).siblings("em").removeClass("results-block__accordion--closed").addClass("results-block__accordion--open");
    });
    $(elementFadeIn).fadeIn(400, "easeInOutExpo");
} //crossDissolveTransition

//a function to set all mega menu position
function allMegaMenuXpos() {
    var x = $("#small-mid-sized-bus__trigger").position(),
        newPos = x.left - 332;
    $(".mega__menu.small-mid-sized-bus").css("left", newPos + "px");
    x = $("#personal-mega__trigger").position(),
        newPos = x.left - 374 + 26.25 + 15;
    $(".mega__menu.personal").css("left", newPos + "px"); 
}
 
//Show the small-mid-sized business mega menu for screen readers pressing 'ENTER' on focus
function accesibleShowSmallMidBusMegaMenu(event) {
    var key = event.keyCode || event.which;

    if (key === 13) { //ENTER
        event.preventDefault();
        $(".mega__menu.small-mid-sized-bus").fadeToggle();
        return;
    }
} //accesibleShowSmallMidBusMegaMenu

//Show a hidden widget for screen readers pressing 'ENTER' on focus
function accesibleMakeMeVisible(event, element) {
    var key = event.keyCode || event.which;

    if (key === 13) { //ENTER
        event.preventDefault();
        $(element).next().fadeToggle();
        return;
    }
} //accesibleShowSmallMidBusMegaMenu

//Open the branch locator accordion for screen readers pressing 'ENTER' on focus
function accesibleBranchLocatorAccordion(event, element) {
    var key = event.keyCode || event.which,
        allHiddenPanels = $(".branch-locator-results-page__accordion-hidden"),
        parent = $(element).parents(".branch-locator-results-page__results"),
        allParents = $(".branch-locator-results-page").find(".branch-locator-results-page__results"),
        arrow = parent.find("em"),
        allArrows = $(".branch-locator-results-page").find(".result-block__accordion"),
        showThisHiddenPanel = parent.find(".branch-locator-results-page__accordion-hidden"),
        allMyBranchCheckboxes = $(".branch-locator-results-page__my-branch"),
        thisMyBranchCheckbox = parent.find(".branch-locator-results-page__my-branch");

    if (key === 13 && parent.hasClass("active")) { //ENTER
        event.preventDefault();
        allHiddenPanels.slideUp();
        parent.removeClass("active");
        parent.attr("aria-expanded", "false");
        arrow.removeClass("results-block__accordion--open").addClass("results-block__accordion--closed");
        allMyBranchCheckboxes.fadeOut();
    } else if (key === 13) {
        allHiddenPanels.slideUp();
        allParents.removeClass("active");
        allParents.attr("aria-expanded", "false");
        allArrows.removeClass("results-block__accordion--open results-block__accordion--closed");
        arrow.addClass("results-block__accordion--open");
        parent.addClass("active");
        parent.attr("aria-expanded", "true");
        showThisHiddenPanel.slideDown();
        allMyBranchCheckboxes.fadeOut();
        thisMyBranchCheckbox.fadeIn();
    }
} //accesibleBranchLocatorAccordion


//Open the branch locator accordion for screen readers pressing 'ENTER' on focus
function accesiblePaginationFade(event, element) {
    var key = event.keyCode || event.which,
        pageNumber = $(element).index(),
        allPageNumbers = $(".carousel--fade-pagination > span"),
        isVisible = $(element).hasClass("active"),
        allPanels = $(element).parents(".branch-locator-results-page").find(".carousel--fade"),
        activePanel = $(element).parents(".branch-locator-results-page").find(".carousel--fade.active");

    //Get the clicked pagination number's index
    //Get all the result sections in a jQuery objecdt
    //Get all the pagination numbers in a jQuery object

    if (key === 13 && isVisible) { } else if (key === 13) {
        activePanel.hide();
        allPanels.removeClass("active");
        allPageNumbers.removeClass("active");
        $(this).addClass("active");
        allPanels.eq(pageNumber).show(0, function () {
            $(this).css("opacity", "0").animate({
                opacity: "1"
            }, 400, "easeInCirc");
            $(this).addClass("active");
        });
    }
} //accesiblePaginationFade


//Change the teritary nav first two links href's text 
function tertiraryNavTextSwitch() {

    return;

    var a = $(".mobile-dropdown-trigger > a"),
        winWidth = $(window).width();

    if (winWidth < 576) {
        $(a[0])
            .contents()
            .filter(function () {
                return this.nodeType !== 1;
            })
            .replaceWith("Branch Locator");
        $(a[1])
            .contents()
            .filter(function () {
                return this.nodeType !== 1;
            })
            .replaceWith("Login");
    } else {
        $(a[0])
            .contents()
            .filter(function () {
                return this.nodeType !== 1;
            })
            .replaceWith("Branch Locator");
        $(a[1])
            .contents()
            .filter(function () {
                return this.nodeType !== 1;
            })
            .replaceWith("Online Banking Login");
    }
} // tertiraryNavTextSwitch



//Scroll top js from art program

var scrolltotop = {
    //startline: Integer. Number of pixels from top of doc scrollbar is scrolled before showing control
    //scrollto: Keyword (Integer, or "Scroll_to_Element_ID"). How far to scroll document up when control is clicked on (0=top).
    setting: {
        startline: 100,
        scrollto: 0,
        scrollduration: 1000,
        fadeduration: [100, 100]
    },
    controlHTML: '<img src="../../../../../Content/img/arrow13.png" /  class="scrollup" alt="Scroll Up" title="Scroll Up">', //HTML for control, which is auto wrapped in DIV w/ ID="topcontrol"
    controlattrs: {
        offsetx: 5,
        offsety: 5
    }, //offset of control relative to right/ bottom of window corner
    anchorkeyword: '#top', //Enter href value of HTML anchors on the page that should also act as "Scroll Up" links
    state: {
        isvisible: false,
        shouldvisible: false
    },
    scrollup: function () {
        if (!this.cssfixedsupport) //if control is positioned using JavaScript
            this.$control.css({
                opacity: 0
            }) //hide control immediately after clicking it
        var dest = isNaN(this.setting.scrollto) ? this.setting.scrollto : parseInt(this.setting.scrollto)
        if (typeof dest == "string" && jQuery('#' + dest).length == 1) //check element set by string exists
            dest = jQuery('#' + dest).offset().top
        else
            dest = 0
        this.$body.animate({
            scrollTop: dest
        }, this.setting.scrollduration);
    },

    keepfixed: function () {
        var $window = jQuery(window)
        var controlx = $window.scrollLeft() + $window.width() - this.$control.width() - this.controlattrs.offsetx
        var controly = $window.scrollTop() + $window.height() - this.$control.height() - this.controlattrs.offsety
        this.$control.css({
            left: controlx + 'px',
            top: controly + 'px'
        })
    },

    togglecontrol: function () {
        var scrolltop = jQuery(window).scrollTop()
        if (!this.cssfixedsupport)
            this.keepfixed()
        this.state.shouldvisible = (scrolltop >= this.setting.startline) ? true : false
        if (this.state.shouldvisible && !this.state.isvisible) {
            this.$control.stop().animate({
                opacity: 1
            }, this.setting.fadeduration[0])
            this.state.isvisible = true
        } else if (this.state.shouldvisible == false && this.state.isvisible) {
            this.$control.stop().animate({
                opacity: 0
            }, this.setting.fadeduration[1])
            this.state.isvisible = false
        }
    },

    init: function () {
        jQuery(document).ready(function ($) {
            var mainobj = scrolltotop
            var iebrws = document.all
            mainobj.cssfixedsupport = !iebrws || iebrws && document.compatMode == "CSS1Compat" && window.XMLHttpRequest //not IE or IE7+ browsers in standards mode
            mainobj.$body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body')
            mainobj.$control = $('<div id="topcontrol" tabindex="0" role="button">' + mainobj.controlHTML + '</div>')
                .css({
                    position: mainobj.cssfixedsupport ? 'fixed' : 'absolute',
                    bottom: mainobj.controlattrs.offsety,
                    right: mainobj.controlattrs.offsetx,
                    opacity: 0,
                    cursor: 'pointer'
                })
                .attr({
                    title: 'Scroll to Top'
                }) 
                .keypress(function (e) {
                    if (e.which == 13) {//Enter key pressed 
                        mainobj.scrollup();
                        return false
                    }
                })
                .click(function () {
                    mainobj.scrollup();
                    return false
                })
                .appendTo('body')
            if (document.all && !window.XMLHttpRequest && mainobj.$control.text() != '') //loose check for IE6 and below, plus whether control contains any text
                mainobj.$control.css({
                    width: mainobj.$control.width()
                }) //IE6- seems to require an explicit width on a DIV containing text
            mainobj.togglecontrol()

            $('a[href="' + mainobj.anchorkeyword + '"]').click(function () {
                mainobj.scrollup()
                return false
            })
            $(window).bind('scroll resize', function (e) {
                mainobj.togglecontrol()
            })
        })
    }
}
//Slider 

$(function () {

  
    //Global variables
    var myBranchResultsAllPanels = "#top-menu-branches .navigation__dropdown-wrap--branch-results--result-block--accordion--elem",
        myBranchResultsActivePanel = "#top-menu-branches .navigation__dropdown-wrap--branch-results--result-block.active .navigation__dropdown-wrap--branch-results--result-block--accordion--elem",
        allDropdownPanels = ".navigation__dropdown-wrap",
        winHeight = $(window).height();

    $(myBranchResultsAllPanels).slideUp();
    $(myBranchResultsActivePanel).slideDown(); 
    //Set the tertiary nav's text based on the screen width
    tertiraryNavTextSwitch();

    //On window resize, recalculate the small-mid-sized mega menu's left position and set the tertiary nav's text
    $(window).resize(function () { 
        tertiraryNavTextSwitch();
        if ($(window).width() >= 1024) {
            var m = $(".mobile-icon");
            $("#mobile-menu").hide();
            m.attr("alt", "Mobile menu icon, click to expand menu");
            $(m).attr("src", function () {
                this.src = this.src.replace("Mobile-menu-close-btn", "mobile-menu");
            });
            // Close the tertiary dropdown on mouseleave
            //$(".navigation__dropdown-wrap").on("mouseleave", function (event) {
            //    event.stopPropagation();
            //    $(this).stop().slideUp(400, "easeInOutExpo");
            //});
        } //Hide the mobile accordion menu if someone opened it in portrait then rotated to landscape		
    }); //Resize the mega menu

    $(document).mouseup(function (e) {
        var container = $(".navigation__dropdown-wrap");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            container.parents(".mobile-dropdown-trigger").find(".navigation__arrowup").css("display", "none");
            container.slideUp(400, "easeInOutExpo"); 
        }
    });
    //Change the language when a new language is clicked
    $(".navigation__dropdown-wrap--language a").click(function () {

        var newSelected = $(this).text(),
            changeText = $(".navigation__tertiary > li:nth-child(4) > a > span");

        $(changeText).contents()
            .filter(function () {
                return this.nodeType !== 1;
            })
            .replaceWith(newSelected);
    }); //Language's text in nav

    //Back button on the my branch search results
    $(".navigation__dropdown-wrap--back").on("click", function (e) {

        e.stopPropagation();
        $(this).parents(".navigation__dropdown-wrap--clipped").prev(".scrollUIindicator").animate({
            opacity: 0
        }, 400, "easeInQuint");
        $(myBranchResultsAllPanels).slideUp();
        $(".navigation__dropdown-wrap--branch-results--result-block.active").removeClass("active");
        $(".navigation__dropdown-wrap--branch-results").fadeOut(400, "easeInOutExpo");
        $(".navigation__dropdown-wrap--branch").fadeIn();
    }); //Dropdown's back button

    //Back button on the set my branch screen
    $(".navigation__dropdown-wrap--back-to-results").on("click", function (e) {
        e.stopPropagation();
        $(this).parent().fadeOut();
        $(".navigation__dropdown-wrap--branch-results").fadeIn(400, "easeInOutExpo");
    }); //Set my branch back button

    //User clicks on "Set My Branch" button
    $(".setMyBranch").click(function (e) {
        e.stopPropagation();
        $(this).parents(".navigation__dropdown-wrap--branch-results").fadeOut();
        $(this).parents(".navigation__dropdown-wrap--clipped").find(".navigation__dropdown-wrap--branch--my--branch-wrapper").fadeIn();
    });

    //User clicks on "Find Another Branch" button
    $("#myBranchContainer").on("click", ".find-branch-reset", function (e) {
        //e.stopPropagation();
        $(this).parents(".navigation__dropdown-wrap--clipped").find(".navigation__dropdown-wrap--branch").fadeIn();
        $(this).parents(".navigation__dropdown-wrap--branch--my--branch-wrapper").fadeOut();
        $(myBranchResultsAllPanels).slideUp();
        $(this).parents(".navigation__dropdown-wrap--clipped").find(".navigation__dropdown-wrap--branch-results--result-block").removeClass("active");
        $(this).parents(".navigation__dropdown-wrap--clipped").find(".navigation__dropdown-wrap--branch-results--result-block > em").removeClass("results-block__accordion--open").addClass("results-block__accordion--closed");

        RemoveMyBranch();
    });

    //calculate mega menu dropdown position when mega menu links on hover
    $(".mega-menu__trigger").hover(allMegaMenuXpos); 

    $(".navigation__dropdown-wrap--branch--my--branch-wrapper").on("mouseleave", function (e) {
        e.stopPropagation();
    });

    //My branch accordion
    $("#top-menu-branches").on("click", ".navigation__dropdown-wrap--branch-results--result-block", function () {
        var clickedPanelIsAlreadyOpen = $(this).hasClass("active");
        if (clickedPanelIsAlreadyOpen) {
            $(myBranchResultsAllPanels).slideUp(400, "easeInOutExpo");
            $(this).removeClass("active");
            $(this).find(".results-block__accordion--open").removeClass("results-block__accordion--open").addClass("results-block__accordion--closed");
        } else {
            $(myBranchResultsAllPanels).slideUp(400, "easeInOutExpo", function () {
                $(myBranchResultsAllPanels).parent().removeClass("active");
                $(this).parent().find(".results-block__accordion--open").removeClass("results-block__accordion--open").addClass("results-block__accordion--closed");
            });
            $(this).find(".navigation__dropdown-wrap--branch-results--result-block--accordion--elem").slideDown(400, function () {
                $(this).parent().addClass("active");
            });
            $(this).find(".results-block__accordion--closed").removeClass("results-block__accordion--closed").addClass("results-block__accordion--open");

        }
    }); //My branch accordion

    //Mobile menu accordion
    $("#mobile-menu .main-menu__mobile__navigation li").on("click", function (e) {
        e.stopPropagation();
        var parent = $(this);
        if (parent.find("em").length == 0) { } else {
            $(parent).find(" > i").toggleClass("expanded");
            $(parent).toggleClass("expanded");
            $(parent).find(" > ul").stop().slideToggle();
        }
    }); //Mobile menu accordion

    //Universal accordion
    $(".result-block__accordion, .branch-locator-results-page__title").on("click", function () {
        var allHiddenPanels = $(".branch-locator-results-page__accordion-hidden"),
            parent = $(this).parents(".branch-locator-results-page__results"),
            allParents = $(".branch-locator-results-page").find(".branch-locator-results-page__results"),
            arrow = parent.find("em"),
            allArrows = $(".branch-locator-results-page").find(".result-block__accordion"),
            showThisHiddenPanel = parent.find(".branch-locator-results-page__accordion-hidden"),
            allMyBranchCheckboxes = $(".branch-locator-results-page__my-branch"),
            thisMyBranchCheckbox = parent.find(".branch-locator-results-page__my-branch");

        if (parent.hasClass("active")) {
            allHiddenPanels.slideUp();
            parent.removeClass("active");
            parent.attr("aria-expanded", "false");
            arrow.removeClass("results-block__accordion--open").addClass("results-block__accordion--closed");
            allMyBranchCheckboxes.fadeOut();
        } else {
            allHiddenPanels.slideUp();
            allParents.removeClass("active");
            allParents.attr("aria-expanded", "false");
            allArrows.removeClass("results-block__accordion--open results-block__accordion--closed");
            arrow.addClass("results-block__accordion--open");
            parent.addClass("active");
            parent.attr("aria-expanded", "true");
            showThisHiddenPanel.slideDown();
            allMyBranchCheckboxes.fadeOut();
            thisMyBranchCheckbox.fadeIn();
        }
    });

    $("#branches").on("click", ".result-block__accordion", function (e) {
        var allHiddenPanels = $(".branch-locator-results-page__accordion-hidden"),
            parent = $(this).parents(".branch-locator-results-page__results"),
            allParents = $(".branch-locator-results-page").find(".branch-locator-results-page__results"),
            arrow = parent.find("em"),
            allArrows = $(".branch-locator-results-page").find(".result-block__accordion"),
            showThisHiddenPanel = parent.find(".branch-locator-results-page__accordion-hidden"),
            allMyBranchCheckboxes = $(".branch-locator-results-page__my-branch"),
            thisMyBranchCheckbox = parent.find(".branch-locator-results-page__my-branch"),
            link = parent.find("a");

        if (parent.hasClass("active")) {
            allHiddenPanels.slideUp();
            parent.removeClass("active");
            parent.attr("aria-expanded", "false");
            link.attr("aria-expanded", "false");
            arrow.removeClass("results-block__accordion--open").addClass("results-block__accordion--closed");
            allMyBranchCheckboxes.fadeOut();
        } else {
            allHiddenPanels.slideUp();
            allParents.removeClass("active");
            allParents.attr("aria-expanded", "false");
            allArrows.removeClass("results-block__accordion--open results-block__accordion--closed");
            arrow.addClass("results-block__accordion--open");
            parent.addClass("active");
            parent.attr("aria-expanded", "true");
            link.attr("aria-expanded", "true");
            showThisHiddenPanel.slideDown();
            allMyBranchCheckboxes.fadeOut();
            thisMyBranchCheckbox.fadeIn();
        }
    });

    //Universal fade in/out pagination
    $(".carousel--fade-pagination > span").on("click", function () {
        var pageNumber = $(this).index(),
            allPageNumbers = $(".carousel--fade-pagination > span"),
            isVisible = $(this).hasClass("active"),
            allPanels = $(this).parents(".container-fluid").find(".carousel--fade"),
            activePanel = $(this).parents(".container-fluid").find(".carousel--fade.active"),
            offsetNav = $(".container-fluid.main-menu").height(),
            top = $("#first-pagination").position().top - offsetNav; //offsetNav = the height of the tertiary menu and menu bar

        //Get the clicked pagination number's index
        //Get all the result sections in a jQuery objecdt
        //Get all the pagination numbers in a jQuery object

        if (isVisible) { } else {
            activePanel.hide();
            allPanels.removeClass("active");
            allPageNumbers.removeClass("active");
            $(this).addClass("active");
            allPanels.eq(pageNumber).show(0, function () {
                $(this).css("opacity", "0").animate({
                    opacity: "1"
                }, 400, "easeInCirc");
                $(this).addClass("active");
            });
            $("html, body").animate({
                scrollTop: top
            }, 600, "easeInOutCirc");
        }
    });

    //Show/hide Map
    $(".branch-locator-results-map-wrap__toggle").on("click", function () {
        var isVisible = $(this).prev().css("display");

        if (isVisible === "none") {
            $(this).prev().slideDown(400, "easeInOutCirc");
            $(this).text("Hide map");
        } else {
            $(this).prev().slideUp(400, "easeInOutCirc");
            $(this).text("Show map");
        }

    });
     
    //Expand the search box on mouseover
    $(".nav-square").click(function () {
        $(".nav-square__searchbox").stop().fadeIn(100);
        $(".nav-square__searchbox .searchbox__input").stop().animate({
            left: '4'
        }, 600, "easeOutCirc");
        $(allDropdownPanels).slideUp();
        $(".navigation__arrowup").css("display", "none");

    }); //Expand the search box

    //Cancel a site search
    $(".searchbox__cancel").click(function (event) {
        event.stopPropagation();
        $(this).parent().stop().animate({
            left: '300px'
        }, 600, "easeInCirc", function () {
            $(".nav-square__searchbox").stop().fadeOut();
        });
    });
     

    //Open a tertiary dropdown
    $(".mobile-dropdown-trigger").keypress(function (event) {
        console.log('this is it');
        var keycode = (event.keyCode ? event.keyCode : event.which); 
        if (keycode == '13') {//Enter key pressed 
            console.log('this is it');
            var allTertDropDownArrows = $(".navigation__arrowup");
            event.stopPropagation();
            $(allDropdownPanels).stop().slideUp();
            $(this).find(".navigation__dropdown-wrap").stop().slideDown(600, "easeOutExpo");

            var x = $(this),
                xpos = $(this).offset(),
                xwidth = $(this).width(),
                setXpos = '';

            //Set the position of the arrow pointing on mobile
            if ($(window).width() < 576) {
                setXpos = xpos.left + xwidth / 2 - 5 + "px";
                $(this).find(".navigation__arrowup").css("left", setXpos);
            } else if ($(window).width() < 768) {
                setXpos = xpos.left + xwidth / 2 + 12 + "px";
                $(this).find(".navigation__arrowup").css("left", setXpos);
            } else {
                setXpos = xwidth / 2 + 10 + "px";
                $(this).find(".navigation__arrowup").css("left", setXpos);
            }
            $(allTertDropDownArrows).css("display", "none");
            $(this).find(".navigation__arrowup").css("display", "block");
        }
    })
    $(".mobile-dropdown-trigger").click(function (event) {  
        var allTertDropDownArrows = $(".navigation__arrowup");
        event.stopPropagation();
        $(allDropdownPanels).stop().slideUp();
        $(this).find(".navigation__dropdown-wrap").stop().slideDown(600, "easeOutExpo");

        var x = $(this),
            xpos = $(this).offset(),
            xwidth = $(this).width(),
            setXpos = '';

        //Set the position of the arrow pointing on mobile
        if ($(window).width() < 576) {
            setXpos = xpos.left + xwidth / 2 - 5 + "px";
            $(this).find(".navigation__arrowup").css("left", setXpos);
        } else if ($(window).width() < 768) {
            setXpos = xpos.left + xwidth / 2 + 12 + "px";
            $(this).find(".navigation__arrowup").css("left", setXpos);
        } else {
            setXpos = xwidth / 2 + 10 + "px";
            $(this).find(".navigation__arrowup").css("left", setXpos);
        }
        $(allTertDropDownArrows).css("display", "none");
        $(this).find(".navigation__arrowup").css("display", "block");
    });

    //Close the tertiary dropdown menu by clicking the up arrow
    $(".slideUp-button-circle-arrow").click(function (e) {
        e.stopPropagation();
        $(this).parents(".navigation__dropdown-wrap").slideUp(400, "easeInOutExpo");
        $(this).parents(".mobile-dropdown-trigger").find(".navigation__arrowup").css("display", "none");
    });

    //Prevent a tertiary dropdown click from sliding the menu up
    $(".navigation__dropdown-wrap").click(function (event) {
        event.stopPropagation();
    }); //Tertiary dropdown

    //Add active class or remove active class on tertiary nav language dropdown
    $(".navigation__dropdown-wrap--language a").click(function () {
         
        var allOptions = $(".navigation__dropdown-wrap--language a");
        allOptions.removeClass("active");
        $(this).addClass("active");
    });

    //Mobile hamburger menu
    $(".mobile-menu-icon").click(function (event) {
        event.preventDefault();
        var m = $(".mobile-icon"),
            menu = $("#mobile-menu").css("display");

        $("#mobile-menu").slideToggle(600, "easeInOutExpo");

        if (menu === 'none') {
            m.attr("alt", "Mobile menu close icon, click to close menu");
            $(this).find("img").attr("src", function () {
                this.src = this.src.replace("mobile-menu", "Mobile-menu-close-btn");
            });
        } else if (menu === 'block') {
            m.attr("alt", "Mobile menu icon, click to expand menu");
            $(this).find("img").attr("src", function () {
                this.src = this.src.replace("Mobile-menu-close-btn", "mobile-menu");
            });
        }
    });

    //Login tab toggle
    $("#login-tabs-dropdown > li").click(function () {
        var href = $(this).find('a').attr('href');
        if ($(this).hasClass("active")) {
            return false;
        } else if (href === '#businesslogindropdown') {
            $(this).addClass("active");
            $(this).siblings().removeClass("active");
            $(".businesslogindropdown").addClass("active in");
            $(".personallogindropdown").removeClass("active in");
            $(this).parents("#login-tabs-dropdown").next().addClass("gunmetal");
        } else if (href === '#personallogindropdown') {
            $(this).addClass("active");
            $(this).siblings().removeClass("active");
            $(".businesslogindropdown").removeClass("active in");
            $(".personallogindropdown").addClass("active in");
            $(this).parents("#login-tabs-dropdown").next().removeClass("gunmetal");
        }
    });

    $("#login-tabs > li:nth-child(2)").click(function () {
        $(this).parents(".login").find(".tab-content").addClass("gunmetal");
    });

    $("#login-tabs > li:nth-child(1)").click(function () {
        $(this).parents(".login").find(".tab-content").removeClass("gunmetal");
    });
     

    //Gallery 3x3 slide up details
    $(".gallery__3x3 ul.industry-expertise--hover li").hover(
        //mouseover
        function () {
            //Because the width of the text in the hidden div varies based on the width of the parent, we need to get the height and store it
            var card = $(this).find("a.industry-expertise--slideup > div"),
                y = $(this).find("a.industry-expertise--slideup > div").height();
            $(card).stop().animate({
                top: y * -1
            }, 500, "easeInOutExpo");
        },
        //mouseout
        function () {
            var card = $(this).find("a.industry-expertise--slideup > div");
            $(card).stop().animate({
                top: "-50px"
            }, 500, "easeInOutExpo");
        }
    );

    //Video lightbox show
    $(".video-lightbox-trigger").click(function () {
        $(".video-lightbox").fadeIn(400);
    });

    //Video lightbox close
    $(".video-lightbox__close").click(function () {
        var video = $("#careers-youtube").attr("src");
        $(".video-lightbox").fadeOut(400);

        $("#careers-youtube").attr("src", "");
        $("#careers-youtube").attr("src", video);
    });

    //Show the on-page subnav on mobile devices
    $(".subnav__band__value").click(function () {
        $(".subnav__band__menu-wrapper").stop().slideToggle();
        $(this).find(".material-icons.result-block__accordion").toggleClass("results-block__accordion--open");
    });

    //Slide the red quadrant in on the landing pages
    $(".intro-paragraph.no-button").addClass("slidein");

    //Fade in elements in a hero on page load
    $(".hero-message.velo > h1, .hero-message.velo > h2").addClass("fadeInOnLoad");

    //Scale up elements in a hero on page load
    $(".velo__logo").addClass("scaleUpOnLoad");
    

    //Accessibility scripts
    //Listen for clicks and prevent default behavior
    $(".prevDef").click(function (event) {
        event.preventDefault();
    });

 

    //if id "sticky tabs" presents, run its functions
    if ($('#stickyTabs').length) {
        var headerHeight = $(".navbar").height(),
            prevAllHeight = headerHeight;
        $("#stickyTabSection").prevAll().each(function () {
            prevAllHeight += $(this).height();
        });
        var allHeight = prevAllHeight + $("#stickyTabSection").height();
        function fixTabInit() {
            var headerHeight = $(".navbar").height(),
                prevAllHeight = headerHeight;
            $("#stickyTabSection").prevAll().each(function () {
                prevAllHeight += $(this).height();
            });
            var allHeight = prevAllHeight + $("#stickyTabSection").height();
        }

        function fixTabMenu() {
            $('#stickyTabs').addClass('fixed');
            $('#stickyTabs').css({
                top: headerHeight
            });
        }

        function unfixTabMenu() {
            $('#stickyTabs').css("display", 'none');
            $('#stickyTabs').css("top", 0.5);
            $('#stickyTabs').removeClass('fixed');
            $('#stickyTabs').css("display", 'block');
        }

        $(window).resize(function () {
            headerHeight = $(".navbar").height();
            prevAllHeight = headerHeight;
            $("#stickyTabSection").prevAll().each(function () {
                prevAllHeight += $(this).height();
            });
            allHeight = prevAllHeight + $("#stickyTabSection").height();
            $(window).scroll();
        });

        //stick tab menu right after header
        $(window).scroll(function () {
            var distanceFromTop = $(this).scrollTop();
            if (distanceFromTop > prevAllHeight && distanceFromTop < allHeight - 100) {
                fixTabMenu();
            } else if (distanceFromTop > allHeight - 101 || distanceFromTop < prevAllHeight) {
                unfixTabMenu();
            }
        });
    }

    $('#stickyTabSection a[data-toggle="tab"]').keydown(function (e) {
         
        if (e.keyCode == 37) {
            $(this).parent().prev().find('a').focus().click(); 
                console.log('show left', $(this).parent().next().find('a'));
        } else if (e.keyCode == 39) {
            $(this).parent().next().find('a').focus().click(); 
                console.log('show right');
                return true;
            } 
    });

    //ADA fixes for sticky tabs
    var stickyTabsTabindex = 1;
    $('#stickyTabs > li').each(function () {
        //let tabsTruthy = $(this).hasClass('active');
        //console.log("does it have the active class? ", tabsTruthy);
        //if (tabsTruthy) {
           //$(this).find('a').attr('tabindex', '1');
        //} else {
            //$(this).find('a').attr('tabindex', '-1');
        //}
        $(this).find('a').attr('tabindex', stickyTabsTabindex++);
        stickyTabsTabindex = stickyTabsTabindex;
    });
    //$('#stickyTabs > li').click(function () {
        //$(this).find('a').attr('tabindex', '1');
        //$(this).siblings('li').find('a').attr('tabindex', '-1');
   //});

    //scroll to the top of tab content when switching tab
    $('#stickyTabSection a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var prevAllHeight = 0;
        var windowsWidth = $(window).width();
        if (windowsWidth < 1025 && windowsWidth > 520) {
            prevAllHeight = 70; 
        } else if (windowsWidth <= 520) {
            prevAllHeight = 79; 
        } 
        $("#stickyTabSection").prevAll().each(function () {
            prevAllHeight += $(this).height() + parseInt($(this).css('padding-bottom'));
        });
        $("html, body").animate({
            scrollTop: prevAllHeight
        }, 600);
    })

    //Init Scroll top icon function
    scrolltotop.init();

    //mega menu hover
    $(".nav-stacked a[data-toggle='tab']").hover(function (e) {
        e.preventDefault(); 
        $(this).tab('show'); 
    })
    $(".nav-stacked a[data-toggle='tab']").focus(function (e) {
        e.preventDefault();
        $(this).tab('show');
    })
    $('#login-tabs li').keydown(function (e) { 
        if ($(this).is(':first-child')) { 
            if (e.keyCode == 37) {  
                return true;
            } else if (e.keyCode == 39) { 
                $('#business-login-tab-trigger a').focus().click();
            } 
        } else {  
            if (e.keyCode == 37) {  
                $('#personal-login-tab-trigger a').focus().click();
            } else if (e.keyCode == 39) {  
                return true;
            }
        }  
    });
    $(".nav-stacked a").focus(function (e) {
        allMegaMenuXpos();
        $(this).closest('.mega__menu ').css({ "visibility": "visible", "opacity": "1" }); 
    })
    $(document).focusin(function (e) {
        $('.mega__menu').each(function (i, obj) {
            if ($(":focus").closest(obj).length > 0) {
                $(this).closest('.mega__menu ').css({ "visibility": "visible", "opacity": "1" });  
            } else if ($(":focus").hasClass('mega-menu__trigger__A')) { 
                $(this).closest('.mega__menu ').css({ "visibility": "hidden", "opacity": "0" }); 
                $(":focus").next().css({ "visibility": "visible", "opacity": "1" });  
            } else { 
                $(this).closest('.mega__menu ').css({ "visibility": "hidden", "opacity": "0" }); 
            }
        }) 
    })
     
    function initTextResize() {
        var iBase = TextResizeDetector.addEventListener(onFontResize, null); 
    }
    function onFontResize(e, args) {
        //var msg = "\nThe base font size in pixels: " + args[0].iBase;
        //msg += "\nThe current font size in pixels: " + args[0].iSize;
        //msg += "\nThe change in pixels from the last  size: " + args[0].iDelta; 
        if (args[0].iSize > 30) {
            $('body').addClass('text-resize');
        } else { 
            $('body').removeClass('text-resize');
        }
    }
    TextResizeDetector.TARGET_ELEMENT_ID = 'personal-mega__trigger';
    TextResizeDetector.USER_INIT_FUNC = initTextResize; 
}); //Automatically run these jQuery scripts when the page loads


/**  
 *  Detects changes to font sizes when user changes browser settings
 *  <br>Fires a custom event with the following data:<br><br>
 * 	iBase  : base font size  	
 *	iDelta : difference in pixels from previous setting<br>
 *  	iSize  : size in pixel of text<br> 
 */
 
TextResizeDetector = function () {
    var el = null;
    var iIntervalDelay = 200;
    var iInterval = null;
    var iCurrSize = -1;
    var iBase = -1;
    var aListeners = [];
    var createControlElement = function () {
        el = document.createElement('span');
        el.id = 'textResizeControl';
        el.innerHTML = '&nbsp;';
        el.style.position = "absolute";
        el.style.left = "-9999px";
        var elC = document.getElementById(TextResizeDetector.TARGET_ELEMENT_ID);
        // insert before firstChild
        if (elC)
            elC.insertBefore(el, elC.firstChild);
        iBase = iCurrSize = TextResizeDetector.getSize();
    };

    function _stopDetector() {
        window.clearInterval(iInterval);
        iInterval = null;
    };
    function _startDetector() {
        if (!iInterval) {
            iInterval = window.setInterval('TextResizeDetector.detect()', iIntervalDelay);
        }
    };

    function _detect() {
        var iNewSize = TextResizeDetector.getSize();

        if (iNewSize !== iCurrSize) {
            for (var i = 0; i < aListeners.length; i++) {
                aListnr = aListeners[i];
                var oArgs = { iBase: iBase, iDelta: ((iCurrSize != -1) ? iNewSize - iCurrSize + 'px' : "0px"), iSize: iCurrSize = iNewSize };
                if (!aListnr.obj) {
                    aListnr.fn('textSizeChanged', [oArgs]);
                }
                else {
                    aListnr.fn.apply(aListnr.obj, ['textSizeChanged', [oArgs]]);
                }
            }

        }
        return iCurrSize;
    };
    var onAvailable = function () {

        if (!TextResizeDetector.onAvailableCount_i) {
            TextResizeDetector.onAvailableCount_i = 0;
        }

        if (document.getElementById(TextResizeDetector.TARGET_ELEMENT_ID)) {
            TextResizeDetector.init();
            if (TextResizeDetector.USER_INIT_FUNC) {
                TextResizeDetector.USER_INIT_FUNC();
            }
            TextResizeDetector.onAvailableCount_i = null;
        }
        else {
            if (TextResizeDetector.onAvailableCount_i < 600) {
                TextResizeDetector.onAvailableCount_i++;
                setTimeout(onAvailable, 200)
            }
        }
    };
    setTimeout(onAvailable, 500);

    return {
        /*
         * Initializes the detector
         * 
         * @param {String} sId The id of the element in which to create the control element
         */
        init: function () {

            createControlElement();
            _startDetector();
        },
        /**
         * Adds listeners to the ontextsizechange event. 
         * Returns the base font size
         * 
         */
        addEventListener: function (fn, obj, bScope) {
            aListeners[aListeners.length] = {
                fn: fn,
                obj: obj
            }
            return iBase;
        },
        /**
         * performs the detection and fires textSizeChanged event
         * @return the current font size
         * @type {integer}
         */
        detect: function () {
            return _detect();
        },
        /**
         * Returns the height of the control element
         * 
     * @return the current height of control element
     * @type {integer}
         */
        getSize: function () {
            var iSize;
            return el.offsetHeight;


        },
        /**
         * Stops the detector
         */
        stopDetector: function () {
            return _stopDetector();
        },
        /*
         * Starts the detector
         */
        startDetector: function () {
            return _startDetector();
        }
    }
}();

TextResizeDetector.TARGET_ELEMENT_ID = 'doc';
TextResizeDetector.USER_INIT_FUNC = null;