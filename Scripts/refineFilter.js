﻿var refineFilter = function () {
    this.allBranches = true;
    this.openSaturday = false;
    this.openSundays = false;
    this.ranch99 = false;
    
    this.mortgageloanofficer = false;
}

refineFilter.prototype = {
    serialize: function () {
        var branchSerialized = "";

        if (this.allBranches)
            branchSerialized += "1";
        else
            branchSerialized += "0";

        if (this.openSaturday)
            branchSerialized += "1";
        else
            branchSerialized += "0";

        if (this.openSundays)
            branchSerialized += "1";
        else
            branchSerialized += "0";

        if (this.ranch99)
            branchSerialized += "1";
        else
            branchSerialized += "0";
        
        if (this.mortgageloanofficer)
            branchSerialized += "1";
        else
            branchSerialized += "0";
        return branchSerialized;
    },

    deserialize: function (serializedString) {
        if (serializedString[0] == "1")
            this.allBranches = true;
        else if (serializedString[0] == "0")
            this.allBranches = false;

        if (serializedString[1] == "1")
            this.openSaturday = true;
        else if (serializedString[1] == "0")
            this.openSaturday = false;

        if (serializedString[2] == "1")
            this.openSundays = true;
        else if (serializedString[2] == "0")
            this.openSundays = false;

        if (serializedString[3] == "1")
            this.ranch99 = true;
        else if (serializedString[3] == "0")
            this.ranch99 = false;

        if (serializedString[4] == "1")
            this.mortgageloanofficer = true;
        else if (serializedString[4] == "0")
            this.mortgageloanofficer = false;
    }
}

function getRefineFilter() {
    var filter = new refineFilter();
    filter.openSaturday = $("[name='OpenSats']").is(':checked');
    filter.openSundays = $("[name='OpenSuns']").is(':checked');
    filter.ranch99 = $("[name='Ranch99']").is(':checked');
    filter.mortgageloanofficer = $("[name='Mln']").is(':checked');
    return filter;
}