﻿(function ($) {
    $.clearableTextBox = function (e, defaultText) {
        this._element = e;
        this.self = this;
        this._defaultText = defaultText;

        if ($(e).val() == "")
            $(e).val(this._defaultText);

        $(e).on('click', null, this.click);
        $(e).on('blur', null, this.change);
    }

    var $ctx = $.clearableTextBox;

    $ctx.fn = $ctx.prototype = {
    };

    $ctx.fn.extend = $ctx.extend = $.extend;

    $ctx.fn.extend({
        _defaultText: '',
        click: function () {
            var instance = $(this).data();
            if ($(this).val() == instance._defaultText) {
                $(this).val('');
            }
        },

        change: function () {
            var instance = $(this).data();
            if ($(this).val().length == 0) {
                $(this).val(instance._defaultText);
            }
        },

        isDefault: function () {
            return $(this._element).val() == this._defaultText;
        }
    });

    $.fn.clearableTextBox = function (defaultText) {
        if (defaultText != undefined) {
            $(this).data(new $ctx(this, defaultText));
        }
        return $(this).data();
    }
})(jQuery);