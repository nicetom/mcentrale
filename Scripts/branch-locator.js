﻿jQuery.support.cors = true;

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

var pagination = {
    currentPage: 1,
    itemsPerPage: 10,
    totalRecords: 0,

    getStartIndex: function () {
        return ((this.currentPage - 1) * this.itemsPerPage);
    },

    getEndIndex: function () {
        return (this.currentPage * this.itemsPerPage);
    }
};

var map = null;
var infobox = null;
var currentPushPin = null;
var searchedViewBoundary = null;
var startpt = null;
var endpt = null;
var searchedBranches = new Array();
var previousPushpin = null;

function LoadMap(key) {
    if (typeof (map) != "undefined" && map != null) {
        map = map;
    }
    else if (typeof (topMenuMap) != "undefined" && topMenuMap != null) {
        map = topMenuMap;
    }
    else {
        map = new Microsoft.Maps.Map('#myMap', {
            credentials: key
        });
        LogEvent("Error", "Branch-Locator::Bing Map Call :" + window.location.href);
    }
   
   // alert(JSON.stringify(map));
    infobox = new Microsoft.Maps.Infobox(map.getCenter(), {
        visible: false
    });

    infobox.setMap(map);

    StartGeocoding();
}

function UnloadMap() {
    if (map != null) {
        map.Dispose();
    }
}

function MakeGeocodeRequest(credentials) {
    var where = getWhereAddress();
    console.log("Bing Map Call :" + window.location.href);
    var geocodeRequest = "https://dev.virtualearth.net/REST/v1/Locations?query=" + encodeURI(where) + "&output=json&key=" + credentials + "&jsonp=GeocodeCallback";
   
    CallRestService(geocodeRequest);
}

function CallRestService(request) {
    var script = document.createElement("script");
    script.setAttribute("type", "text/javascript");
    script.setAttribute("src", request);
    document.body.appendChild(script);
}

function StartGeocoding() {
    map.getCredentials(MakeGeocodeRequest);
}

function GeocodeCallback(result) {
    
    // if there are no results, display the error message and return
    if (!(result
            && result.resourceSets
            && result.resourceSets.length > 0
            && result.resourceSets[0].resources
            && result.resourceSets[0].resources.length > 0)) {
        alert("There were no results.");
        return;
    }

    var resource = result.resourceSets[0].resources[0];
    var bbox = resource.bbox;

    searchedViewBoundary = Microsoft.Maps.LocationRect.fromLocations(new Microsoft.Maps.Location(bbox[0], bbox[1]), new Microsoft.Maps.Location(bbox[2], bbox[3]));
    
    // Add a pushpin at the found location
    var location = new Microsoft.Maps.Location(result.resourceSets[0].resources[0].point.coordinates[0], result.resourceSets[0].resources[0].point.coordinates[1]);
    var pushpin = new Microsoft.Maps.Pushpin(location,
        {
            icon: '/Content/img/branch-locator/pinGreenCenter.png',
            text: resource.Name
        }
     );

    map.entities.push(pushpin);
    currentPushPin = pushpin;

    // Add the information to the resultsDiv html, including a link
    // that recenters the map over the pin

    pagination.currentPage = 1;
    getBranches(location);

    startpt = location;
}

function MapSetCenter(latitude, longitude) {
    endpt = new Microsoft.Maps.Location(latitude, longitude);
}

function getBranches(location) {

    var distance = $('#search-radius').val();
    var refineFilter = getRefineFilter();
    var parameters =
        {
            cache: false,
            latitude: location.latitude,
            longitude: location.longitude,
            distance: distance,
            startIndex: pagination.getStartIndex(),
            endIndex: pagination.getEndIndex(),
            OpenSats: refineFilter.openSaturday,
            OpenSuns: refineFilter.openSundays,
            Ranch99: refineFilter.ranch99,
            Mln: refineFilter.mortgageloanofficer
        };
    var url = "/en/BranchLocator/GetLocations";
    
    $.getJSON(url, parameters)
    .done(function (data) {
        $("#branches").html("");

        var where = getWhereAddress();
        $("#spanAddress").text(where);

        pagination.totalRecords = data.TotalBranches;
        addPushPins(data.Branches);
        updatePaginationLinks();
    })
    .fail(function (jqxhr, textStatus, error) {
        //alert(jqxhr + ' ' + textStatus + ' ' + error);
    });
}

function updatePaginationLinks() {

    var $paginationContainer = $("#pagination-container");
    $paginationContainer.empty();

    if (pagination.totalRecords <= 10) {
        $paginationContainer.hide();
    }
    else {
        $paginationContainer.show();

        var pageNo = 1;
        for (var i = 0; i < pagination.totalRecords; i = i + pagination.itemsPerPage) {
            var tabindex = 50 + pageNo;

            if (pagination.currentPage == pageNo) {
                $paginationContainer.append("<span class=\"active\" onkeydown=\"accesiblePaginationFade(event, this)\">" + pageNo + "</span>");
            }
            else {
                $paginationContainer.append("<span tabindex=\"" + tabindex + "\" onkeydown=\"accesiblePaginationFade(event, this)\">" + pageNo + "</span>");
            }
            pageNo = pageNo + 1;
        }

        $("#pagination-container > SPAN").on("click keydown", null, function (e) {

            if (e.type == "keydown") {
                var key = e.keyCode || e.which;

                if (key != 13) {
                    return;
                }
            }

            var pageNo = $(this).html();
            pagination.currentPage = pageNo;
            map.entities.clear();
            map.entities.push(currentPushPin);
            getBranches(startpt);
        });
    }
}



function addPushPins(branches) {
    searchedBranches = new Array();

    var rect = {
        x1: searchedViewBoundary.center.latitude,
        y1: searchedViewBoundary.center.longitude,
        x2: searchedViewBoundary.center.latitude,
        y2: searchedViewBoundary.center.longitude
    };

    for (var i = 0; i < branches.length && i < 10; i++) {
        var branch = branches[i];

        if (i < 10) {
            if (branch.Latitude < rect.x1)
                rect.x1 = branch.Latitude;

            if (branch.Latitude > rect.x2)
                rect.x2 = branch.Latitude;

            if (branch.Longitude < rect.y1)
                rect.y1 = branch.Longitude;

            if (branch.Longitude > rect.y2)
                rect.y2 = branch.Longitude;
        }

        var newLocation = new Microsoft.Maps.Location(branch.Latitude, branch.Longitude);
        
        var storeNumber = getStoreNumber(i);

        var pushPinOptions = getPushPinOptions(storeNumber, "Black");
        var pushpin = new Microsoft.Maps.Pushpin(newLocation, pushPinOptions);
        
        var desc = branch.Address1 + "<br />" +
                branch.City + ", " + branch.State + " " + branch.ZipCode + "<br />" +
                "Ph: " + branch.PhoneNumber;
        
        pushpin.metadata = {
            title: branch.Name,
            description: desc
        };
        
        Microsoft.Maps.Events.addHandler(pushpin, 'click', pinPushed);
        map.entities.push(pushpin);

        var phtml = createBranchItem(i, branch);
        $("#branches").append(phtml);

        searchedBranches.push({
            branch: branch,
            pushpin: pushpin,
            refBox: $("#branch_" + storeNumber)
        });
    }

    if (branches.length > 0) {

        setTimeout(function () {
            $(".material-icons.result-block__accordion:eq(0)").trigger("click");
        });        

        searchedViewBoundary = Microsoft.Maps.LocationRect.fromLocations(new Microsoft.Maps.Location(rect.x1, rect.y1), new Microsoft.Maps.Location(rect.x2, rect.y2));
        map.setView({ bounds: searchedViewBoundary });

        $(".branch-item").on('click', null, function () {
            var storeNumber = $(this).find(".storeNumber").html();
            var indexNumber = getIndexNumber(storeNumber);
            showInfoBox(indexNumber);
            setCenter(indexNumber);
            selectBranchItem(indexNumber);

        });
    }
}

function pinPushed(mouseEvent) {
    var storeNumber = mouseEvent.target.entity.iconText;
    var indexNumber = storeNumber - pagination.getStartIndex() - 1;
    showInfoBox(indexNumber);
    selectBranchItem(indexNumber);
}

function getPushPinOptions(storeNumber, color) {
    return {
        text: '' + storeNumber,
        icon: '/Content/img/branch-locator/pin' + color + '.png'
    };
}



function getStoreNumber(indexNumber) {
    return indexNumber + pagination.getStartIndex() + 1;
}

function showInfoBox(indexNumber) {
    var branch = searchedBranches[indexNumber].branch;
    var storeNumber = getStoreNumber(indexNumber);
    var where = getWhereAddress();
    // set pushpin
    if (previousPushpin != null) {
        previousPushpin.setOptions(getPushPinOptions(previousPushpin.getText(), 'Black'));
    }

    searchedBranches[indexNumber].pushpin.setOptions(getPushPinOptions(storeNumber, 'Red'));

    var newLocation = new Microsoft.Maps.Location(branch.Latitude, branch.Longitude);
    var label = "<a class='direction-button button' href='branch-locator/directions?&id=" + branch.Id + "&proximity=50&address=" + where + "&refineFilter=" + getRefineFilter().serialize() + "'>Directions</a>";
    var desc = branch.Address1 + "<br />" +
            branch.City + ", " + branch.State + " " + branch.ZipCode + "<br />" +
            "Ph: " + branch.PhoneNumber + "<br /><br />" +
            label;
    
    infobox.setOptions({
        location: newLocation,
        title: branch.Name,
        description: desc,
        maxHeight: 256,
        maxWidth: 382,
        offset: new Microsoft.Maps.Point(0, 10),
        visible: true
    });

    previousPushpin = searchedBranches[indexNumber].pushpin;
}

function createBranchItem(index, branch) {
    var branchItemHtml = $("#branch-item-template").html();
    //alert(JSON.stringify(branch));

    var showAddress2 = branch.Address2 != null;
    var showWalkThru = branch.WalkThruHours != '';
    var showDriveThru = branch.DriveThruHours != '';

    var storeNumber = getStoreNumber(index);
    
    var branchData = {
        Id: branch.Id,
        StoreNumber: storeNumber,
        Name: branch.Name,
        DistanceMiles: Math.round(branch.DistanceMiles * 10) / 10,
        Address1: branch.Address1,
        ShowAddress2: showAddress2,
        Address2: (branch.Address2 == null ? "" : branch.Address2),
        City: branch.City,
        State: branch.State,
        Zip: branch.ZipCode,
        PhoneNumber: branch.PhoneNumber,
        FromAddress: getWhereAddress(),
        Proximity: $("#search-radius").val(),
        BranchHours: branch.LocationHours,
        ShowWalkThru: showWalkThru,
        WalkThruHours: branch.WalkThruHours,
        ShowDriveThru: showDriveThru,
        DriveThruHours: branch.DriveThruHours
    }
        
    branchItemHtml = Mustache.render(branchItemHtml, branchData);
        
    return branchItemHtml;
}

function MatchCode(code) {
    if (code == VEMatchCode.None) {
        return "No match";
    }

    var codeDesc = "";
    var cVal;

    cVal = code & VEMatchCode.Good;
    if (cVal > 0) {
        codeDesc += "Good ";
    }

    cVal = code & VEMatchCode.Ambiguous;
    if (cVal > 0) {
        codeDesc += "Ambiguous ";
    }

    cVal = code & VEMatchCode.UpHierarchy;
    if (cVal > 0) {
        codeDesc += "UpHierarchy ";
    }

    cVal = code & VEMatchCode.Modified;
    if (cVal > 0) {
        codeDesc += "Modified ";
    }

    return (codeDesc + "Match");
}

function getWhereAddress() {
    var where = $("#br-search").val();
    if (where == "")
        where = "Los Angeles, CA";
    
    return where;
}
function LogEvent(level, message) {
    level = level || 'Error';
    $.ajax({
        url: '/Home/LogErrorEvent',
        type: "POST",
        data: { message: message },
        success: function (result) {
           // console.log('Log done');
        }
    });
}

function onGeocodeClick() {

    //if ($("#search") == "") {
    //    alert("Please enter an address.");
    //    return;
    //}

    map.entities.clear();
    
    StartGeocoding();
}


function setCenter(indexNumber) {
    var branch = searchedBranches[indexNumber].branch;
    map.setView({ zoom: 12, center: new Microsoft.Maps.Location(branch.Latitude, branch.Longitude) });
}

function selectBranchItem(indexNumber) {
    $(".red-box").each(function () {
        $(this).removeClass("red-box");
        $(this).addClass("grey-box");
    });

    $("#branch_" + indexNumber).find(".marker-icon").removeClass("grey-box");
    $("#branch_" + indexNumber).find(".marker-icon").addClass("red-box");
}

function getIndexNumber(storeNumber) {
    return storeNumber - pagination.getStartIndex() - 1;
}
