﻿jQuery.support.cors = true;

var topMenuMap = null;
var credentialskey = null;
function LoadTopMenuMap(key) {
    var branchId = GetMyBranchId();
    credentialskey = key;
    if (branchId > 0) {
        credentialskey = key;
        CheckToShowMyBranch();
    }
    else {
        $("#miBranchLocator>.branch-locator-loading-text").hide();
        setTimeout(function () {
            $("#miBranchLocator>.branch-name").fadeIn();
        }, 0);
    }
        //setTimeout(function () {
        //    if (typeof (map) != "undefined" && map != null) {
        //        topMenuMap = map;
        //    }
        //    else {
        //        topMenuMap = new Microsoft.Maps.Map('#top-menu-map', {
        //            credentials: key
        //        });
        //    }

        //    CheckToShowMyBranch();
        //}, 1000);
    
}

function UnloadTopMenuMap() {
    if (topMenuMap != null) {
        topMenuMap.Dispose();
    }
}

function MakeTopMenuGeocodeRequest(credentials) {
    var where = getTopMenuWhereAddress();

    var geocodeRequest = "https://dev.virtualearth.net/REST/v1/Locations?query=" + encodeURI(where) + "&output=json&key=" + credentials + "&jsonp=TopMenuGeocodeCallback";
   
    CallTopMenuRestService(geocodeRequest);
}

function CallTopMenuRestService(request) {
    var script = document.createElement("script");
    script.setAttribute("type", "text/javascript");
    script.setAttribute("src", request);
    document.body.appendChild(script);
}

function StartTopMenuGeocoding() {
    
    topMenuMap.getCredentials(MakeTopMenuGeocodeRequest);
}

function LogEvent(level, message) {
    level = level || 'Error';
     $.ajax({
        url: '/Home/LogErrorEvent',
        type: "POST",
        data: { message: message },
        success: function (result) {
          //  console.log('Log done');
        }
    });
}

var topMenuSearchedViewBoundary = null;
var topMenuStartpt = null;
var topMenuEndpt = null;
var latitude = null;
var longitude = null;
function TopMenuGeocodeCallback(result) {

    // if there are no results, display the error message and return
    if (!(result && result.resourceSets && result.resourceSets.length > 0 && result.resourceSets[0].resources && result.resourceSets[0].resources.length > 0)) {
        alert("There were no results.");
        return;
    }

    var resource = result.resourceSets[0].resources[0];
    var bbox = resource.bbox;

    //topMenuSearchedViewBoundary = Microsoft.Maps.LocationRect.fromLocations(new Microsoft.Maps.Location(bbox[0], bbox[1]), new Microsoft.Maps.Location(bbox[2], bbox[3]));

    // Add a pushpin at the found location
    var location = new Microsoft.Maps.Location(result.resourceSets[0].resources[0].point.coordinates[0], result.resourceSets[0].resources[0].point.coordinates[1]);
   
    getTopMenuBranches(location);

    topMenuStartpt = location;
}

function getTopMenuBranches(location) {

    var distance = 100;
    var parameters =
    {
        cache: false,
        latitude: location.latitude,
        longitude: location.longitude,
        distance: distance,
        startIndex: 0,
        endIndex: 3,
    };
    var url = "/en/BranchLocator/GetLocations";

    $.getJSON(url, parameters)
        .done(function (data) {
            $("#top-menu-branches").html("");
            //pagination.totalRecords = data.TotalBranches;
            $(".search-result-count").text(data.TotalBranches);

            createTopMenuBranches(data.Branches);
        })
        .fail(function (jqxhr, textStatus, error) {
            alert(jqxhr + ' ' + textStatus + ' ' + error);
        });
}

var searchedBranches = new Array();

function createTopMenuBranches(branches) {

    searchedBranches = new Array();

    for (var i = 0; i < Math.min(branches.length, 3) && i < 10; i++) {
        var branch = branches[i];

        var phtml = createTopMenuBranchItem(i, branch);

        var $branchesContainer = $("#top-menu-branches")

        $branchesContainer.append(phtml);

        $branchesContainer.find(".navigation__dropdown-wrap--branch-results--result-block:not(:first)").removeClass("active")
        $branchesContainer.find(".results-block__accordion--open:not(:first)").removeClass("results-block__accordion--open").addClass("results-block__accordion--closed");
        $branchesContainer.find(".navigation__dropdown-wrap--branch-results--result-block--accordion--elem:not(:first)").hide();



        //searchedBranches.push({
        //    branch: branch,
        //    refBox: $("#branch_" + storeNumber)
        //});
    }

    //if (branches.length > 0) {
    //    searchedViewBoundary = Microsoft.Maps.LocationRect.fromLocations(new Microsoft.Maps.Location(rect.x1, rect.y1), new Microsoft.Maps.Location(rect.x2, rect.y2));
    //    map.setView({ bounds: searchedViewBoundary });

    //    $(".branch-item").on('click', null, function () {
    //        var storeNumber = $(this).find(".storeNumber").html();
    //        var indexNumber = getIndexNumber(storeNumber);
    //        showInfoBox(indexNumber);
    //        setCenter(indexNumber);
    //        selectTopMenuBranchItem(indexNumber);

    //    });
    //}
}

function getPushPinOptions(storeNumber, color) {
    return {
        text: '' + storeNumber,
        icon: '/Content/img/branch-locator/pin' + color + '.png'
    };
}

var previousPushpin = null;

function getStoreNumber(indexNumber) {
    return indexNumber + 1;
}

String.prototype.replaceAll = function (str1, str2, ignore) {
    return this.replace(new RegExp(str1.replace(/([\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g, function (c) { return "\\" + c; }), "g" + (ignore ? "i" : "")), str2);
};

function createTopMenuBranchItem(index, branch) {
    var branchItemHtml = $("#top-menu-branch-template").html();
    //alert(JSON.stringify(branch));

    var showWalkThru = branch.WalkThruHours != '';
    var showDriveThru = branch.DriveThruHours != '';

    var storeNumber = getStoreNumber(index);

    var branchData = {
        Id: branch.Id,
        StoreNumber: storeNumber,
        Name: branch.Name,
        DistanceMiles: Math.round(branch.DistanceMiles * 10) / 10,
        Address1: branch.Address1,
        Address2: (branch.Address2 == null ? "" : branch.Address2),
        City: branch.City,
        State: branch.State,
        Zip: branch.ZipCode,
        PhoneNumber: branch.PhoneNumber,
        FromAddress: getTopMenuWhereAddress(),
        Proximity: "100",
        BranchHours: branch.LocationHours,
        ShowWalkThru: showWalkThru,
        WalkThruHours: branch.WalkThruHours,
        ShowDriveThru: showDriveThru,
        DriveThruHours: branch.DriveThruHours
    }

    branchItemHtml = Mustache.render(branchItemHtml, branchData);

    return branchItemHtml;
}

//
function getTopMenuWhereAddress() {
    var where = $("#branch_address").val();
    if (where == "")
        where = "Los Angeles, CA";

    $("#search-address").text(where);
    var lnkMoreBranches = $("#lnkMoreBranches");
    var branchesHref = lnkMoreBranches.attr("href");
    var arr = branchesHref.split("=");
    var newHref = arr[0] + "=" + where;
    lnkMoreBranches.attr("href", newHref);

    return where;
}

function onTopMenuGeocodeClick(form) {

    var $form = $(form);
    if ($("#branch_address") == "") {
        alert("Please enter an address.");
        return false;
    }

    //topMenuMap.entities.clear();
    MakeTopMenuGeocodeRequest(credentials);
   // StartTopMenuGeocoding();

    crossDissolveTransition($form.find("submit")[0]);

    return false;
}


function setCenter(indexNumber) {
    var branch = searchedBranches[indexNumber].branch;
    topMenuMap.setView({ zoom: 12, center: new Microsoft.Maps.Location(branch.Latitude, branch.Longitude) });
}

function selectTopMenuBranchItem(indexNumber) {
    $(".red-box").each(function () {
        $(this).removeClass("red-box");
        $(this).addClass("grey-box");
    });

    $("#branch_" + indexNumber).find(".marker-icon").removeClass("grey-box");
    $("#branch_" + indexNumber).find(".marker-icon").addClass("red-box");
}

function getIndexNumber(storeNumber) {
    return storeNumber - pagination.getStartIndex() - 1;
}

function GetMyBranchId() {
    var id = $.cookie("branch_id");

    if (id == null) {
        id = 0;
    }

    return id;
}

function SetMyBranchId(branchId) {
    RemoveMyBranch();
    $.cookie("branch_id", branchId, { path: '/' });
}

function RemoveMyBranch() {
    $.cookie('branch_id', null, { path: '/', expires: -1 });

    var $span = $("#miBranchLocator>SPAN");
    $span.text($span.data("text-branch-locator"));
}

function CheckToShowMyBranch() {
    var branchId = GetMyBranchId();
    if (branchId > 0) {
        var url = "/en/BranchLocator/GetBranchById?id=" + branchId;
        $.getJSON(url)
            .done(function (branch) {


                var template = $("#myBranchTemplate").html();

                //var imageAddress = branch.Address1;

                //if (branch.Address2 != null && branch.Address2 != "") {
                //    imageAddress += " " + branch.Address2;
                //}

                //imageAddress += "%0A " + branch.City + ", " + branch.State + ", " + branch.ZipCode;

                var branchData = {
                    Id: branch.Id,
                    Name: branch.BranchName,
                    Address1: branch.Address1,
                    Address2: (branch.Address2 == null ? "" : branch.Address2),
                    State: branch.State,
                    City: branch.City,
                    PhoneNumber: branch.PhoneNumber,
                    OpenUntil: branch.OpenUntilString
                   // FromAddress: getTopMenuWhereAddress()
                    //  Latitude: branch.Latitude,
                    //  Longitude: branch.Longitude,
                    //  ImageAddress: imageAddress
                }

                var html = Mustache.render(template, branchData);
                $("#myBranchContainer").html(html);

                $("#miBranchLocator>.branch-name").text(branch.BranchName);

                $("#miBranchLocator>.branch-locator-loading-text").hide();
                setTimeout(function () {
                    $("#miBranchLocator>.branch-name").fadeIn();
                }, 0);
                $(".navigation__dropdown-wrap--branch").hide();
                $(".navigation__dropdown-wrap--branch-results").hide();
                $(".navigation__dropdown-wrap--branch--my--branch-wrapper").show();
            })
            .fail(function (jqxhr, textStatus, error) {
                alert(jqxhr + ' ' + textStatus + ' ' + error);
            });
    } else {
        $("#miBranchLocator>.branch-locator-loading-text").hide();
        setTimeout(function () {
            $("#miBranchLocator>.branch-name").fadeIn();
        }, 0);
    }
}

$(function () {

    $("#frmTopMenuBranchSearch").on("submit", function (e) {
        e.preventDefault();
        var $form = $(this);
        if ($("#branch_address") == "") {
            alert("Please enter an address.");
            return false;
        }

        setTimeout(function () {
            if (typeof (topMenuMap) != "undefined" && topMenuMap != null) {
                topMenuMap = topMenuMap;
            }
            else {
                topMenuMap = new Microsoft.Maps.Map('#top-menu-map', {
                    credentials: credentialskey
                });
                LogEvent("Error", "Branch-Locator-Topmenu::Bing Map Call :" + window.location.href);
            }
            StartTopMenuGeocoding();

            crossDissolveTransition($form.find("#top_menu_branch_search")[0]);

            return false;
            
        }, 1000);


       
    });


    $("#top-menu-branches,#branches").on("click", "[name='btn-directions'],[name='set-my-branch']", function (e) {
        e.preventDefault();
        if (this.name == "btn-directions") {
            var url = $(this).attr("href");
            document.location.href = url;
        }
        else {
           
            var id = $(this).data("id");
            SetMyBranchId(id);
            CheckToShowMyBranch();
        }
    });
});