﻿var isContactSending = false;

$(function () {
    
    $("[data-lang]").on("click", function (event) {
        if (window["supported_languages"] != null && window["supported_languages"].length > 0) {

            var linkLang = $(this).data("culture");

            for (var i = 0; i < window["supported_languages"].length; ++i) {
                var lang = window["supported_languages"][i];

                if (lang == linkLang) {
                    return;
                }
            }
            alert("This page may not be viewed in this language");

            
            event.stopImmediatePropagation();
            event.preventDefault();
        }
    });

    var $div = $("#emergency-alert-popup");

    if ($div.length > 0) {
        $div.jqm({
            modal: true
        });
        $div.jqmShow();

        $("#emergency-alert-close").on("click", function () {
            $div.jqmHide();
        });
    }

    $("#email-us-button,#email-us-button1,#email-us-button2").on("click", function (e) {
        var url = $(this).data("url");

        $("#email-us-container").remove();

        var $emailUsContainer = $("<div id='email-us-container' class='modal'></div>");
        $("BODY").append($emailUsContainer); 
        var opts = { length: 25, width: 10, radius: 25}
        var spinner = new Spinner(opts).spin($emailUsContainer[0]);
       
        $emailUsContainer.jqm({
            modal: true
        });
        $emailUsContainer.jqmShow();
        
        $.ajax({
            url: url,
            cache: false
        }).done(function (data) {
            spinner.stop();
            $emailUsContainer.html(data);

            var $form = $emailUsContainer.find("FORM"); 
            $.validator.unobtrusive.parse($form);
            
            $('.modal-body').css('max-height', $(window).height() * 0.9); 
            $form.find("#cancel-contact").on("click", function () {
                $emailUsContainer.jqmHide();
                $emailUsContainer.remove();
            });
            $(".jqmClose").on("click", function () {
                $emailUsContainer.jqmHide();
                $emailUsContainer.remove();
            });


            $form.on("submit", function (e) {
                e.preventDefault();

                if (isContactSending) {
                    return;
                }

                var recapchaResponse = grecaptcha.getResponse();

                if (recapchaResponse != null && recapchaResponse.length == 0) {
                    $form.find("[name='captcha-error']").show();
                }
                else {
                    $form.find("[name='captcha-error']").hide();
                }

                if ($form.valid() && recapchaResponse.length > 0) {
                    $form.addClass('hideForm');
                    spinner.spin($emailUsContainer[0]);

                    var action = $form.attr("action");
                    var data = $form.serialize();

                    isContactSending = true;
                    $.post(action, data, function (data) {
                        isContactSending = false;
                        spinner.stop();
                        if (data.MessageSent) {
                            $form.hide();
                            $("#divContactUsSuccess").show();
                        }
                    });
                }
            });
        });
    });



    $(".social-icon:eq(2)").parent().on("click", function (e) {
        e.preventDefault();

        $("#we-chat-container").remove();
        var $container = $("<div id='we-chat-container' class='modal'></div>");


        $("BODY").append($container);
        var opts = { length: 25, width: 10, radius: 25 }
        var spinner = new Spinner(opts).spin($container[0]);

        $container.jqm({
            //modal: true 
        });
        $container.jqmShow();

        var url = $(this).attr("href");

        $.ajax({
            url: url,
            cache: false
        }).done(function (data) {
            spinner.stop();
            $container.html(data);

            $(".jqmClose").on("click", function () {
                $container.jqmHide();
                $container.remove();
            });

        });
    });

    ///if ($("#emergency-alert-popup"))

});