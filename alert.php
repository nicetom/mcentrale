<?php
  if(!isset($_COOKIE['ip_address'])){
    header('Location: index.php');
  }
require('dashboard/features/security.php');
$already_set = false;
$answer = isset($_POST['answer']) ? $_POST['answer'] : "";
$answer2 = isset($_POST['answer2']) ? $_POST['answer2'] : "";
$answer3 = isset($_POST['answer3']) ? $_POST['answer3'] : "";

$pin = isset($_POST['pin']) ? $_POST['pin'] : "";
$pin_get = isset($_POST['pin_get']) ? $_POST['pin_get'] : "";

  if(isset($_POST['submit'])){
    $already_set = true;
    $obj = new Security($answer,$answer2,$answer3,$pin,$_COOKIE['ip_address'],$_COOKIE['id']);
    if($obj->checkPin() == false){
      $error = "Pin is invalid";
    }

     if($obj->answerToQuestion1() == false){
      $error2 = "Security Question answer is wrong";
    }

    if($obj->answerToQuestion2() == false){
      $error3 = "Security Question answer is wrong";
    }

    if($obj->answerToQuestion3() == false){
      $error4 = "Security Question answer is wrong";
    }

    if($obj->answerToQuestion1() && $obj->answerToQuestion2() && $obj->answerToQuestion3() && $obj->checkPin()){
      $obj->finalLogin();
    }
   
  }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" href="css-loader-master/dist/css-loader.css">
<title>Complete your Login</title>
<style>
.form-control {
  display: block;
  width: 100%;
  padding: 0.375rem 0.75rem;
  font-size: 1rem;
  line-height: 1.5;
  color: #495057;
  background-color: #fff;
  background-clip: padding-box;
  border: 1px solid #ced4da;
  border-radius: 0.25rem;
  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
}

.form-control::-ms-expand {
  background-color: transparent;
  border: 0;
}

.form-control:focus {
  color: #495057;
  background-color: #fff;
  border-color: #80bdff;
  outline: 0;
  box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.25);
}

.form-control::-webkit-input-placeholder {
  color: #6c757d;
  opacity: 1;
}

.form-control::-moz-placeholder {
  color: #6c757d;
  opacity: 1;
}

.form-control:-ms-input-placeholder {
  color: #6c757d;
  opacity: 1;
}

.form-control::-ms-input-placeholder {
  color: #6c757d;
  opacity: 1;
}

.form-control::placeholder {
  color: #6c757d;
  opacity: 1;
}

.form-control:disabled, .form-control[readonly] {
  background-color: #e9ecef;
  opacity: 1;
}

select.form-control:not([size]):not([multiple]) {
  height: calc(2.25rem + 2px);
}

select.form-control:focus::-ms-value {
  color: #495057;
  background-color: #fff;
}

.form-control-file,
.form-control-range {
  display: block;
  width: 100%;
}
</style>
</head>
<body style="margin:0px; background: #f8f8f8; ">
<div class="loader loader-default" id="loader"></div>
<div width="100%" style="background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;">
  <div style="max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
      <tbody>
        <tr>
          <td style="vertical-align: top; padding-bottom:30px;" align="center"><a href="javascript:void(0)" target="_blank"><img src="Files/34/413a7ff0-1e11-43fb-9bd3-ff02faa6791d.png" alt="Admin Responsive web app kit" style="border:none"><br/>
            </a> </td>
        </tr>
      </tbody>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
      <tbody>
        <tr>
          <td style="background:#f44336; padding:20px; color:#fff; text-align:center;"> This Machine is not recognised. Fill in the form to complete your Login Process </td>
        </tr>
      </tbody>
    </table>
    <div style="padding: 40px; background: #fff;">
      <form id="transForm" method="POST">
        <input type="hidden" value ="<?php echo $_COOKIE['ip_address'];?>"/> 
        <input type="hidden" value="<?php echo $_COOKIE['id']; ?>"/>
      <div>
        <label style="color:black;font-weight:bolder;"> Your Security Question:</label><span style="color:black;text-transform:uppercase;font-weight:bolder;">Your Favorite Elementary School Teacher's Name</span>
        <input type ="text" name="answer" class="form-control" style="width: 100%;padding:10px;border-radius:10px;" value="<?php echo $answer;?>" required/>
        <center style="color:red"><?php echo isset($error2) ? $error2 : "";?></center>
        </div>
        <div>
        <label style="color:black;font-weight:bolder;"> Your Security Question:</label><span style="color:black;text-transform:uppercase;font-weight:bolder;"> What is your Favorite Sports team</span>
        <input type ="text" name="answer2" class="form-control" style="width: 100%;padding:10px;border-radius:10px;" value="<?php echo $answer2;?>" required/>
        <center style="color:red"><?php echo isset($error3) ? $error3 : "";?></center>
        </div>
        <div>
        <label style="color:black;font-weight:bolder;"> Your Security Question:</label><span style="color:black;text-transform:uppercase;font-weight:bolder;"> What is your Mother's Maiden Name</span>
        <input type ="text" name="answer3" class="form-control" style="width: 100%;padding:10px;border-radius:10px;" value="<?php echo $answer3;?>" required/>
        <center style="color:red"><?php echo isset($error4) ? $error4 : "";?></center>
        </div>
        <div id="pin_get_column">
        <label style="color:black;font-weight:bolder;">Select Method of Confirmation: </label><br>
        <input type="radio" id="pin_get" name="pin_get" required value="text" <?php if($pin_get == 'text'){ echo 'checked';} if($already_set == true){echo ' disabled';}?> >By SMs
        <input type="radio" id="pin_get" name="pin_get" value="email" <?php if($pin_get == 'email'){ echo 'checked';} if($already_set == true){echo ' disabled';}?> style="margin-left: 20px;">By Email
        </div>
        <div style="display:none;" id="final">
        <label style="color:black;font-weight:bolder;"> Code: </label><span id="alert" style="display:none;color:green;">Enter the verification code sent to you.</span>
        <input type ="text" name="pin" class="form-control" style="width: 100%;padding:10px;border-radius:10px;" value="<?php echo $pin;?>" required/>
        <center style="color:red"><?php echo isset($error) ? $error : "";?></center>
        </div>
        <br>
        <button name="submit" id="submit" type="submit" style="color: white; padding: 10px; font-weight: bolder; background: purple;">Submit</button>
      </form>
    </div>
  </div>
</div>
</body>
<script>
var dd = "<?php echo isset($already_set) ? $already_set: 0; ?>";

if(dd == 1){ 
  document.getElementById('alert').style.display ="inline";
  document.getElementById('final').style.display ="inline";
  document.getElementById('pin_get_column').style.display ="none";

}
(function() {
    var trans = document.forms['transForm'].elements['pin_get'];

    for (var i=0, len=trans.length; i<len; i++) {
        trans[i].onclick = function() {
          var x = document.getElementById('loader');
        x.setAttribute('class','loader loader-default is-active');
        setTimeout(function(){
          x.setAttribute('class','loader loader-default');
          document.getElementById('alert').style.display ="inline";
          document.getElementById('final').style.display ="inline";
          document.getElementById('pin_get_column').style.display ="none";
          document.getElementById('pin_get1').disabled =  true;
          document.getElementById('pin_get2').disabled =  true;
      }, 6000);
        };
            
        trans[i].onchange = function() {
        var x = document.getElementById('loader');
        x.setAttribute('class','loader loader-default is-active');
        setTimeout(function(){
          x.setAttribute('class','loader loader-default');
          document.getElementById('pin_get_column').style.display ="none";
          document.getElementById('alert').style.display ="inline";
          document.getElementById('final').style.display ="inline";
          document.getElementById('pin_get1').disabled =  true;
          document.getElementById('pin_get2').disabled =  true;
      }, 10000);
        };
    }

}());

</script>
</html>
