<?php

require_once('dashboard/features/login.php');
$email = isset($_POST['email']) ? $_POST['email'] : "";
$password = isset($_POST['password']) ? $_POST['password'] : "";
$obj2 = new Login($email, $password);
if (isset($_POST['submit'])) {
    $obj = new Login($email, $password);
    if ($obj->checkEmail() == false) {
        $error = "Email is not valid";
    } else if ($obj->login() == false) {
        $error2 = "Password is incorrect";
    } else {
        $obj->totalLogin();
    }
}
?>
<!DOCTYPE html>
<html>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Centennial Bank | Log-in</title>

    <link href="bootstrap.css" rel="stylesheet" />


    <link rel='stylesheet' id='contact-form-7-css' href='wp-content/plugins/contact-form-7/includes/css/styles3c21.css?ver=5.1.1' type='text/css' media='all' />

    <link rel='stylesheet' id='avada-stylesheet-css' href='wp-content/themes/mcc/assets/css/style.min9f31.css?ver=5.7.2' type='text/css' media='all' />
    <!--[if lte IE 9]>
<link rel='stylesheet' id='avada-IE-fontawesome-css'  href='https://www.mcc.it/wp-content/themes/mcc/includes/lib/assets/fonts/fontawesome/font-awesome.min.css?ver=5.7.2' type='text/css' media='all' />
<![endif]-->
    <!--[if IE]>
<link rel='stylesheet' id='avada-IE-css'  href='https://www.mcc.it/wp-content/themes/mcc/assets/css/ie.min.css?ver=5.7.2' type='text/css' media='all' />
<![endif]-->
    <link rel='stylesheet' id='wpdreams-asp-basic-css' href='wp-content/plugins/ajax-search-pro/css/style.basice154.css?ver=A9LIR0' type='text/css' media='all' />
    <link rel='stylesheet' id='wpdreams-asp-chosen-css' href='wp-content/plugins/ajax-search-pro/css/chosen/chosene154.css?ver=A9LIR0' type='text/css' media='all' />
    <link rel='stylesheet' id='wpdreams-ajaxsearchpro-instances-css' href='wp-content/uploads/asp_upload/style.instancese154.css?ver=A9LIR0' type='text/css' media='all' />
    <link rel='stylesheet' id='fusion-dynamic-css-css' href='wp-content/uploads/fusion-styles/dda8281b94a9b34af31c3bc1ce1e4d96.mind87f.css?ver=4.9.9' type='text/css' media='all' />
    <link rel='stylesheet' id='mpc-massive-style-css' href='wp-content/plugins/mpc-massive/assets/css/mpc-styles9ab0.css?ver=2.4.3.2' type='text/css' media='all' />
    <link rel='stylesheet' id='style-css' href='wp-content/themes/mcc/assets/css/mcc-styled87f.css?ver=4.9.9' type='text/css' media='all' />
    <link rel='stylesheet' id='style-magnific-css' href='wp-content/plugins/mpc-massive/magnific-popupd87f.css?ver=4.9.9' type='text/css' media='all' />
    <link rel='stylesheet' id='carosello-css' href='wp-content/themes/mcc/assets/css/owl.carouseld87f.css?ver=4.9.9' type='text/css' media='all' />
    <link rel='stylesheet' id='carosello2-css' href='wp-content/themes/mcc/assets/css/owl.theme.default.mind87f.css?ver=4.9.9' type='text/css' media='all' />
    <script type='text/javascript' src='wp-includes/js/jquery/jqueryb8ff.js?ver=1.12.4'></script>
    <script type='text/javascript' src='wp-includes/js/jquery/jquery-migrate.min330a.js?ver=1.4.1'></script>
    <script type='text/javascript' src='wp-content/plugins/mpc-massive/magnific-popup.mind87f.js?ver=4.9.9'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/owl.carouseld87f.js?ver=4.9.9'></script>


</head>

<body data-rsssl=1 class="home page-template page-template-home page-template-home-php page page-id-6 wpb-js-composer js-comp-ver-5.6 vc_responsive fusion-body ltr no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-mobile-totop fusion-disable-outline layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-flyout fusion-image-hovers fusion-show-pagination-text fusion-header-layout-v2 avada-responsive avada-footer-fx-none fusion-search-form-classic fusion-avatar-square">


    <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
    <div id="wrapper" class="">
        <div id="home" style="position:relative;top:-1px;"></div>

        <header class="fusion-header-wrapper">
            <div class="fusion-header-v2 fusion-logo-left fusion-sticky-menu- fusion-sticky-logo- fusion-mobile-logo-1  fusion-mobile-menu-design-flyout fusion-header-has-flyout-menu">

            <div class="fusion-secondary-header">
					<div class="fusion-row">
						<div class="fusion-alignright">
							<nav class="fusion-secondary-menu" role="navigation" aria-label="Secondary Menu">
								<ul id="menu-menu-top" class="menu">
									<li id="menu-item-88" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-88" data-item-id="88"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="Quick Links"><span class="menu-text">Quick links</span></a></li>
									<li id="menu-item-87" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-87" data-item-id="87"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="FAQ"><span class="menu-text">FAQ</span></a></li>
									<li id="menu-item-5088" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5088" data-item-id="5088"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="Work with us"><span class="menu-text">Work with us</span></a></li>
									<li id="menu-item-86" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-86" data-item-id="86"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="Where we are"><span class="menu-text">Where we Are</span></a></li>
									<li id="menu-item-85" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-85" data-item-id="85"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="Contacts"><span class="menu-text">Contacts</span></a></li>
									<li id="menu-item-89" class="fadeSearch menu-item menu-item-type-custom menu-item-object-custom menu-item-89" data-classes="fadeSearch" data-item-id="89"><a href="#" class="fusion-icon-only-link fusion-flex-link fusion-bar-highlight" role="menuitem" data-name="Search"><span class="fusion-megamenu-icon"><i class="glyphicon fa-search fas"></i></span><span class="menu-text"><span class="menu-title">Search</span></span></a></li>
								</ul>
							</nav>
							<nav class="fusion-mobile-nav-holder fusion-mobile-menu-text-align-left" aria-label="Secondary Mobile Menu"></nav>
						</div>
					</div>
				</div>
                <div class="fusion-header-sticky-height"></div>
                <div class="fusion-header">
                    <div class="fusion-row">
                        <div class="fusion-header-has-flyout-menu-content">
                            <div class="fusion-logo" data-margin-top="31px" data-margin-bottom="31px" data-margin-left="0px" data-margin-right="0px">
                                <a class="fusion-logo-link" href="index.php">

                                    <!-- standard logo -->
                                    <img src="images/logo/cover.png" alt="Centennial Bank Logo" />

                                    <!-- mobile logo -->
                                    <img src="images/logo/default.png" width="180" height="113" style="max-height:113px;height:auto;" alt="Centennial Bank Logo" class="fusion-mobile-logo" />

                                </a>
                            </div>
                            <nav class="fusion-main-menu" aria-label="Main Menu">
                                <ul id="menu-main-menu" class="fusion-menu">
                                    <li id="menu-item-93" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-93 fusion-dropdown-menu" data-item-id="93">
                                        <a href="login.php" class="fusion-bar-highlight" role="menuitem" data-name="LOG IN"><span class="menu-text">LOG IN</span>
                                        </a></li>
                                    <li id="menu-item-93" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-93 fusion-dropdown-menu" data-item-id="93">
                                        <a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="WHO WE ARE"><span class="menu-text">WHO WE ARE</span>
                                        </a>
                                        <ul role="menu" class="sub-menu">
                                            <li id="menu-item-4806" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4806 fusion-dropdown-submenu">
                                                <a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="MISSION"><span>MISSION</span></a>
                                            </li>
                                            <li id="menu-item-4807" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4807 fusion-dropdown-submenu">
                                                <a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="GOVERNANCE"><span>GOVERNANCE</span></a>
                                            </li>
                                            <li id="menu-item-4808" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4808 fusion-dropdown-submenu">
                                                <a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="COMPANY INFO"><span>COMPANY INFO</span></a>
                                            </li>
                                            <li id="menu-item-4809" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4809 fusion-dropdown-submenu">
                                                <a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="INVEST IN US"><span>INVEST IN US</span></a>
                                            </li>
                                            <li id="menu-item-5087" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5087 fusion-dropdown-submenu"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="WORK WITH US"><span>WORK WITH US</span></a></li>
                                        </ul>
                                    </li>
                                    <li id="menu-item-3882" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3882 fusion-dropdown-menu" data-item-id="3882"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="WHAT WE DO"><span class="menu-text">WHAT WE DO</span></a>
                                        <ul role="menu" class="sub-menu">
                                            <li id="menu-item-4812" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4812 fusion-dropdown-submenu"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="FUNDING"><span>FINANZIAMENTI</span></a></li>
                                            <li id="menu-item-4811" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4811 fusion-dropdown-submenu"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="AGEVOLAZIONI"><span>AGEVOLAZIONI</span></a></li>
                                        </ul>
                                    </li>
                                    <li id="menu-item-2683" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2683 fusion-dropdown-menu" data-item-id="2683"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="IN THE SPOTLIGHT"><span class="menu-text">IN THE SPOTLIGHT</span></a>
                                        <ul role="menu" class="sub-menu">
                                            <li id="menu-item-4813" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4813 fusion-dropdown-submenu"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="NOTIZIE"><span>NOTIZIE</span></a></li>
                                            <li id="menu-item-4814" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4814 fusion-dropdown-submenu"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="Success Stories"><span>Success Stories</span></a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                            <div class="fusion-mobile-navigation">
                                <ul id="menu-mobile-menu" class="fusion-mobile-menu">
                                    <li id="menu-item-1656" class="title-sotto-menu-mobile menu-item menu-item-type-custom menu-item-object-custom menu-item-1656" data-classes="title-sotto-menu-mobile" data-item-id="1656"><a href="#" class="fusion-bar-highlight" role="menuitem" data-name="Menu"><span class="menu-text">Menu</span></a></li>
                                    <li id="menu-item-1657" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-6 current_page_item menu-item-1657" data-item-id="1657"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="HOME"><span class="menu-text">HOME</span></a></li>
                                    <li id="menu-item-4377" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4377" data-item-id="4377"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="CHI SIAMO"><span class="menu-text">CHI SIAMO</span></a></li>
                                    <li id="menu-item-1659" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1659" data-item-id="1659"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="COSA FACCIAMO"><span class="menu-text">COSA FACCIAMO</span></a></li>
                                    <li id="menu-item-3071" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3071" data-item-id="3071"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="Primo piano"><span class="menu-text">Primo piano</span></a></li>
                                    <li id="menu-item-1662" class="title-sotto-menu-mobile-2lv menu-item menu-item-type-custom menu-item-object-custom menu-item-1662" data-classes="title-sotto-menu-mobile-2lv" data-item-id="1662"><a href="#" class="fusion-bar-highlight" role="menuitem" data-name="Link utili"><span class="menu-text">Link utili</span></a></li>
                                    <li id="menu-item-1666" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1666" data-item-id="1666"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="LINK VELOCI"><span class="menu-text">LINK VELOCI</span></a></li>
                                    <li id="menu-item-1665" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1665" data-item-id="1665"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="FAQ"><span class="menu-text">FAQ</span></a></li>
                                    <li id="menu-item-5089" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5089" data-item-id="5089"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="LAVORA CON NOI"><span class="menu-text">LAVORA CON NOI</span></a></li>
                                    <li id="menu-item-1664" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1664" data-item-id="1664"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="DOVE SIAMO"><span class="menu-text">DOVE SIAMO</span></a></li>
                                    <li id="menu-item-1663" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1663" data-item-id="1663"><a href="index.php" class="fusion-bar-highlight" role="menuitem" data-name="CONTATTI"><span class="menu-text">CONTATTI</span></a></li>
                                </ul>
                            </div>
                            <div class="fusion-flyout-menu-icons fusion-flyout-mobile-menu-icons">



                                <div class="fusion-flyout-search-toggle">
                                    <div class="fusion-toggle-icon">
                                        <div class="fusion-toggle-icon-line"></div>
                                        <div class="fusion-toggle-icon-line"></div>
                                        <div class="fusion-toggle-icon-line"></div>
                                    </div>
                                    <a class="fusion-icon fusion-icon-search" aria-hidden="true" aria-label="Toggle Search" href="#"></a>
                                </div>

                                <a class="fusion-flyout-menu-toggle" aria-hidden="true" aria-label="Toggle Menu" href="#">
                                    <div class="fusion-toggle-icon-line"></div>
                                    <div class="fusion-toggle-icon-line"></div>
                                    <div class="fusion-toggle-icon-line"></div>
                                </a>
                            </div>

                            <div class="fusion-flyout-search">
                                <form role="search" class="searchform fusion-search-form" method="get" action="https://www.mcc.it/">
                                    <div class="fusion-search-form-content">
                                        <div class="fusion-search-field search-field">
                                            <label class="screen-reader-text" for="s">Search for:</label>
                                            <input type="text" value="" name="s" class="s" placeholder="Search ..." required aria-required="true" aria-label="Search ..." />
                                        </div>
                                        <div class="fusion-search-button search-button">
                                            <input type="submit" class="fusion-search-submit searchsubmit" value="&#xf002;" />
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="fusion-flyout-menu-bg"></div>

                            <nav class="fusion-mobile-nav-holder fusion-flyout-menu fusion-flyout-mobile-menu" aria-label="Main Menu Mobile"></nav>

                        </div>
                    </div>
                </div>
            </div>
            <div class="fusion-clearfix"></div>
        </header>






        <main id="main" role="main" class="clearfix " style="">

            <div class="container" style="margin-top: 200px;">
                <div class="col-md-6 col-md-offset-3">
                    <form method="POST" autocomplete="off">
                        <div class="form-group">
                            <label for="username">Email</label>
                            <input type="text" class="form-control" name="email" required value="<?php echo $email; ?>" placeholder="Email">
                            <?php if (isset($error)) : ?>
                                <div>
                                    <center><span style="color:red;"><?php echo $error; ?></span></center>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                            <?php if (isset($error2)) : ?>
                                <div>
                                    <center><span style="color:red;"><?php echo $error2; ?></span></center>
                                </div>
                            <?php endif; ?>
                        </div>
                        <button class="btn btn-success" type="submit" name="submit">Submit</button>
                    </form>
                </div>

            </div>


            <div class="fraud-container">
                <div class="fraud-button"><img alt="Sicurezza a frodi" src="wp-content/themes/mcc/assets/images/sicurezza.svg" />
                </div>
                <div class="fraud-panel">
                    <div class="fraud-box">
                        <h5>Security and Fraud</h5>
                        <p>Dear Customer, in this period Centennial Bank SpA and many companies are victims of deceptive actions with which financial products are offered, abusively using the name of Centennial Bank SpA, whose concession is subject to the stipulation of an insurance policy and the payment of the relative premium .
                            <br />We emphasize that this is a phenomenon to which the Bank is completely foreign.<br />
                            <a href="index.php" title="Read" target="_blank"><strong>Read</strong></a>
                        </p>
                    </div>
                </div>
            </div>
    </div> <!-- fusion-row -->
    </main> <!-- #main -->




    <div class="fusion-footer">

<footer role="contentinfo" class="fusion-footer-widget-area fusion-widget-area">
    <div class="fusion-row">
        <div class="fusion-columns fusion-columns-4 fusion-widget-area">

            <div class="fusion-column col-lg-3 col-md-3 col-sm-3">
                <section id="media_image-2" class="fusion-footer-widget-column widget widget_media_image"><img width="188" height="118" src="images/logo/cover.png" class="image wp-image-115  attachment-full size-full" alt="Logo Centennial Bank" style="width:400px; height: auto;" />
                    <div style="clear:both;"></div>
                </section>
            </div>
            <div class="fusion-column col-lg-3 col-md-3 col-sm-3">
                <section id="nav_menu-2" class="fusion-footer-widget-column widget widget_nav_menu">
                    <div class="menu-menu-footer-1-container">
                        <ul id="menu-menu-footer-1" class="menu">
                            <li id="menu-item-3321" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3321"><a href="index.php" data-name="Disclaimer">Disclaimer</a></li>
                            <li id="menu-item-3320" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3320"><a href="index.php" data-name="Warnings">Warnings</a></li>
                            <li id="menu-item-3323" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3323"><a href="index.php" data-name="Privacy policy">Privacy policy</a></li>
                            <li id="menu-item-3322" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3322"><a href="index.php" data-name="Transparency">Transparency</a></li>
                            <li id="menu-item-3324" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3324"><a href="index.php" data-name="Security and Fraud">Security and Fraud</a></li>
                        </ul>
                    </div>
                    <div style="clear:both;"></div>
                </section>
            </div>
            <div class="fusion-column col-lg-3 col-md-3 col-sm-3">
                <section id="nav_menu-3" class="fusion-footer-widget-column widget widget_nav_menu">
                    <div class="menu-menu-footer-2-container">
                        <ul id="menu-menu-footer-2" class="menu">
                            <li id="menu-item-3338" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3338"><a href="sospensione-rate-index.php" data-name="Suspension of loan installments">Suspension of loan installments</a></li>
                            <li id="menu-item-3339" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3339"><a href="index.php" data-name="Referee for Disputes">Referee for disputes</a></li>
                            <li id="menu-item-3340" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3340"><a href="index.php" data-name="Company Data">Company data</a></li>
                            <li id="menu-item-5243" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5243"><a href="index.php" data-name="PSD2 &#038; Open Banking">PSD2 &#038; Open Banking</a></li>
                        </ul>
                    </div>
                    <div style="clear:both;"></div>
                </section>
            </div>
            <div class="fusion-column fusion-column-last col-lg-3 col-md-3 col-sm-3">
                <!-- <section id="social_links-widget-2" class="fusion-footer-widget-column widget social_links">
                    <h4 class="widget-title">FOLLOW US</h4>
                    <div class="fusion-social-networks">

                        <div class="fusion-social-networks-wrapper">

                            <a class="fusion-social-network-icon fusion-tooltip fusion-rss fusion-icon-rss" href="index.php" data-placement="top" data-title="Rss" data-toggle="tooltip" data-original-title="" title="Rss" aria-label="Rss" rel="nofollow noopener noreferrer" target="_self" style="font-size:16px;color:#bebdbd;"></a>


                            <a class="fusion-social-network-icon fusion-tooltip fusion-linkedin fusion-icon-linkedin" href="https://www.linkedin.com/company/mediocredito-centrale/" data-placement="top" data-title="LinkedIn" data-toggle="tooltip" data-original-title="" title="LinkedIn" aria-label="LinkedIn" rel="nofollow noopener noreferrer" target="_self" style="font-size:16px;color:#bebdbd;"></a>


                            <a class="fusion-social-network-icon fusion-tooltip fusion-mail fusion-icon-mail" href="mailto:re&#108;az&#105;&#111;n&#105;&#046;e&#115;t&#101;rn&#101;&#064;&#109;cc.i&#116;" data-placement="top" data-title="Mail" data-toggle="tooltip" data-original-title="" title="Mail" aria-label="Mail" rel="nofollow noopener noreferrer" target="_self" style="font-size:16px;color:#bebdbd;"></a>



                        </div>
                    </div>

                    <div style="clear:both;"></div>
                </section> -->
            </div>

            <div class="fusion-clearfix"></div>
        </div> <!-- fusion-columns -->
    </div> <!-- fusion-row -->
</footer> <!-- fusion-footer-widget-area -->


<footer id="footer" class="fusion-footer-copyright-area fusion-footer-copyright-center">
    <div class="fusion-row">
        <div class="fusion-copyright-content">

            <div class="fusion-copyright-notice">
                <div>
                    Centennial Bank © 2020 Tax Code 00594040586 VAT number 00915101000 </div>
            </div>
            <div class="fusion-social-links-footer">
            </div>

        </div> <!-- fusion-fusion-copyright-content -->
    </div> <!-- fusion-row -->
</footer> <!-- #footer -->
</div>

    </div> <!-- wrapper -->

    <a class="fusion-one-page-text-link fusion-page-load-link"></a>

    <div class='asp_hidden_data' id="asp_hidden_data" style="display: none !important;">
        <svg style="position:absolute" height="0" width="0">
            <filter id="aspblur">
                <feGaussianBlur in="SourceGraphic" stdDeviation="4" />
            </filter>
        </svg>
        <svg style="position:absolute" height="0" width="0">
            <filter id="no_aspblur"></filter>
        </svg>
    </div>

    <link rel='stylesheet' id='js_composer_front-css' href='wp-content/plugins/js_composer/assets/css/js_composer.min40df.css?ver=5.6' type='text/css' media='all' />
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wpcf7 = {
            "apiSettings": {
                "root": "https:\/\/www.mcc.it\/wp-json\/contact-form-7\/v1",
                "namespace": "contact-form-7\/v1"
            },
            "cached": "1"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/scripts3c21.js?ver=5.1.1'></script>
    <!--[if IE 9]>
<script type='text/javascript' src='https://www.mcc.it/wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-ie9.js?ver=1'></script>
<![endif]-->
    <script type='text/javascript' src='wp-content/plugins/ajax-search-pro/js/nomin/photostacke154.js?ver=A9LIR0'></script>
    <script type='text/javascript' src='wp-content/plugins/ajax-search-pro/js/min/chosen.jquery.mine154.js?ver=A9LIR0'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var ajaxsearchpro = {
            "ajaxurl": "https:\/\/www.mcc.it\/wp-admin\/admin-ajax.php",
            "backend_ajaxurl": "https:\/\/www.mcc.it\/wp-admin\/admin-ajax.php",
            "js_scope": "jQuery"
        };
        var ASP = {
            "ajaxurl": "https:\/\/www.mcc.it\/wp-admin\/admin-ajax.php",
            "backend_ajaxurl": "https:\/\/www.mcc.it\/wp-admin\/admin-ajax.php",
            "js_scope": "jQuery",
            "asp_url": "https:\/\/www.mcc.it\/wp-content\/plugins\/ajax-search-pro\/",
            "upload_url": "https:\/\/www.mcc.it\/wp-content\/uploads\/asp_upload\/",
            "detect_ajax": "0",
            "media_query": "A9LIR0",
            "version": "4978",
            "scrollbar": "1",
            "css_loaded": "1",
            "js_retain_popstate": "0",
            "fix_duplicates": "1"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/plugins/ajax-search-pro/js/min/jquery.ajaxsearchpro-noui-isotope.mine154.js?ver=A9LIR0'></script>
    <script type='text/javascript' src='wp-includes/js/jquery/ui/core.mine899.js?ver=1.11.4'></script>
    <script type='text/javascript' src='wp-includes/js/jquery/ui/datepicker.mine899.js?ver=1.11.4'></script>
    <script type='text/javascript'>
        jQuery(document).ready(function(jQuery) {
            jQuery.datepicker.setDefaults({
                "closeText": "Chiudi",
                "currentText": "Oggi",
                "monthNames": ["gennaio", "febbraio", "marzo", "aprile", "maggio", "giugno", "luglio", "agosto", "settembre", "ottobre", "novembre", "dicembre"],
                "monthNamesShort": ["Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic"],
                "nextText": "Successivo",
                "prevText": "Precedente",
                "dayNames": ["domenica", "luned\u00ec", "marted\u00ec", "mercoled\u00ec", "gioved\u00ec", "venerd\u00ec", "sabato"],
                "dayNamesShort": ["dom", "lun", "mar", "mer", "gio", "ven", "sab"],
                "dayNamesMin": ["D", "L", "M", "M", "G", "V", "S"],
                "dateFormat": "d MM yy",
                "firstDay": 1,
                "isRTL": false
            });
        });
    </script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.hoverintent68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-vertical-menu-widget68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/cssuac93e.js?ver=2.1.28'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/modernizr3d36.js?ver=3.3.1'></script>
    <script type='text/javascript' src='wp-content/plugins/js_composer/assets/lib/bower/isotope/dist/isotope.pkgd.min40df.js?ver=5.6'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/packery001e.js?ver=2.0.0'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/bootstrap.collapseb12b.js?ver=3.1.1'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/bootstrap.modalb12b.js?ver=3.1.1'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/bootstrap.tooltip46df.js?ver=3.3.5'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/bootstrap.popover46df.js?ver=3.3.5'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/bootstrap.transitione485.js?ver=3.3.6'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/bootstrap.tabb12b.js?ver=3.1.1'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.waypointsb95e.js?ver=2.0.3'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.requestAnimationFrame68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.appear68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.carouFredSelf731.js?ver=6.2.1'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.cycle19ce.js?ver=3.0.3'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.easing4e44.js?ver=1.3'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.easyPieCharte7f3.js?ver=2.1.7'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.fitvids4963.js?ver=1.1'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.flexslider605a.js?ver=2.2.2'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var fusionMapsVars = {
            "admin_ajax": "https:\/\/www.mcc.it\/wp-admin\/admin-ajax.php"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.fusion_maps605a.js?ver=2.2.2'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.hoverflow68b3.js?ver=1'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var fusionLightboxVideoVars = {
            "lightbox_video_width": "1280",
            "lightbox_video_height": "720"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.ilightbox950a.js?ver=2.2.3'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.infinitescroll53cf.js?ver=2.1'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.mousewheel7c45.js?ver=3.0.6'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.placeholder5d0a.js?ver=2.0.7'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.touchSwipe7514.js?ver=1.6.6'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/jquery.fade68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/imagesLoaded3a79.js?ver=3.1.8'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-alert68b3.js?ver=1'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var fusionEqualHeightVars = {
            "content_break_point": "800"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-equal-heights68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/fusion-parallax68b3.js?ver=1'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var fusionVideoGeneralVars = {
            "status_vimeo": "0",
            "status_yt": "1"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/fusion-video-general68b3.js?ver=1'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var fusionVideoBgVars = {
            "status_vimeo": "0",
            "status_yt": "1"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/library/fusion-video-bg68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-waypoints68b3.js?ver=1'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var fusionLightboxVars = {
            "status_lightbox": "1",
            "lightbox_gallery": "1",
            "lightbox_skin": "metro-white",
            "lightbox_title": "1",
            "lightbox_arrows": "1",
            "lightbox_slideshow_speed": "5000",
            "lightbox_autoplay": "",
            "lightbox_opacity": "0.9",
            "lightbox_desc": "1",
            "lightbox_social": "1",
            "lightbox_deeplinking": "1",
            "lightbox_path": "vertical",
            "lightbox_post_images": "1",
            "lightbox_animation_speed": "Normal"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-lightbox68b3.js?ver=1'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var fusionCarouselVars = {
            "related_posts_speed": "2500",
            "carousel_speed": "5000"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-carousel68b3.js?ver=1'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var fusionFlexSliderVars = {
            "status_vimeo": "",
            "page_smoothHeight": "false",
            "slideshow_autoplay": "1",
            "slideshow_speed": "7000",
            "pagination_video_slide": "",
            "status_yt": "1",
            "flex_smoothHeight": "false"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-flexslider68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-popover68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-tooltip68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-sharing-box68b3.js?ver=1'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var fusionBlogVars = {
            "infinite_blog_text": "<em>Loading the next set of posts...<\/em>",
            "infinite_finished_msg": "<em>All items displayed.<\/em>",
            "slideshow_autoplay": "1",
            "slideshow_speed": "7000",
            "pagination_video_slide": "",
            "status_yt": "1",
            "lightbox_behavior": "all",
            "blog_pagination_type": "Pagination",
            "flex_smoothHeight": "false"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-blog68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-button68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-general-global68b3.js?ver=1'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var fusionIe1011Vars = {
            "form_bg_color": "#ffffff"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-ie101168b3.js?ver=1'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var avadaHeaderVars = {
            "header_position": "top",
            "header_layout": "v2",
            "header_sticky": "0",
            "header_sticky_type2_layout": "menu_only",
            "side_header_break_point": "1024",
            "header_sticky_mobile": "0",
            "header_sticky_tablet": "0",
            "mobile_menu_design": "flyout",
            "sticky_header_shrinkage": "0",
            "nav_height": "54",
            "nav_highlight_border": "3",
            "nav_highlight_style": "bar",
            "logo_margin_top": "31px",
            "logo_margin_bottom": "31px",
            "layout_mode": "wide",
            "header_padding_top": "0px",
            "header_padding_bottom": "0px",
            "offset_scroll": "full"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-header9f31.js?ver=5.7.2'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var avadaMenuVars = {
            "header_position": "Top",
            "logo_alignment": "Left",
            "header_sticky": "0",
            "side_header_break_point": "1024",
            "mobile_menu_design": "flyout",
            "dropdown_goto": "Go to...",
            "mobile_nav_cart": "Shopping Cart",
            "mobile_submenu_open": "Open Sub Menu",
            "mobile_submenu_close": "Close Sub Menu",
            "submenu_slideout": "1"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-menu9f31.js?ver=5.7.2'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var fusionScrollToAnchorVars = {
            "content_break_point": "800",
            "container_hundred_percent_height_mobile": "0"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/themes/mcc/includes/lib/assets/min/js/general/fusion-scroll-to-anchor68b3.js?ver=1'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/library/bootstrap.scrollspyd617.js?ver=3.3.2'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var avadaCommentVars = {
            "title_style_type": "",
            "title_margin_top": "",
            "title_margin_bottom": ""
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-comments9f31.js?ver=5.7.2'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-general-footer9f31.js?ver=5.7.2'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-quantity9f31.js?ver=5.7.2'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-scrollspy9f31.js?ver=5.7.2'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-select9f31.js?ver=5.7.2'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var avadaSidebarsVars = {
            "header_position": "top",
            "header_layout": "v2",
            "header_sticky": "0",
            "header_sticky_type2_layout": "menu_only",
            "side_header_break_point": "1024",
            "header_sticky_tablet": "0",
            "sticky_header_shrinkage": "0",
            "nav_height": "54",
            "content_break_point": "800"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-sidebars9f31.js?ver=5.7.2'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/library/jquery.sticky-kit9f31.js?ver=5.7.2'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-tabs-widget9f31.js?ver=5.7.2'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-container-scroll9f31.js?ver=5.7.2'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var toTopscreenReaderText = {
            "label": "Go to Top"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/library/jquery.toTop62ea.js?ver=1.2'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var avadaToTopVars = {
            "status_totop_mobile": "0"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-to-top9f31.js?ver=5.7.2'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-drop-down9f31.js?ver=5.7.2'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var avadaRevVars = {
            "avada_rev_styles": "1"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-rev-styles9f31.js?ver=5.7.2'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-contact-form-79f31.js?ver=5.7.2'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var avadaPrivacyVars = {
            "name": "privacy_embeds",
            "days": "30",
            "path": "\/",
            "types": [],
            "defaults": [],
            "button": "0"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-privacy9f31.js?ver=5.7.2'></script>

    <script type='text/javascript' src='wp-content/plugins/mpc-massive/assets/js/mpc-vendor.min9ab0.js?ver=2.4.3.2'></script>
    <script type='text/javascript' src='wp-content/plugins/mpc-massive/assets/js/mpc-scripts.min9ab0.js?ver=2.4.3.2'></script>
    <script type='text/javascript' src='wp-content/themes/mcc/assets/min/js/general/avada-maind87f.js?ver=4.9.9'></script>
    <script type='text/javascript' src='wp-includes/js/wp-embed.mind87f.js?ver=4.9.9'></script>
    <script type='text/javascript' src='wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min40df.js?ver=5.6'></script>
</body>

<!-- Mirrored from www.mcc.it/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 24 Jun 2019 08:39:18 GMT -->

</html>

<!-- Dynamic page generated in 3.248 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2019-06-20 16:55:56 -->

<!-- Compression = gzip -->